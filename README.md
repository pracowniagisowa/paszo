# Food Orders 3.0 - application info #

The Food Orders 3.0 application is a tool designed to support group lunch ordering.

## Key Features ##

An important feature of the application is the support of the "lunch pass". If the user pays a larger amount in advance, the application will settle it systematically. Information about the overpayment appears in the "Order" panel in the "Balance" field.

Additional functionalities include:
* drawing of the person who should order on a given day
* Storing the piggy bank history.

## Frontend ##
* React Framework
* Bootstrap 5

## Backend ##
* .NET Core
* Entity Framework
* Identity

### Login page ###
![Login page](screens/login_page.png)


### App main window ###
![App main window](screens/order_page.png)

### Lunch ordering panel ###
![Lunch ordering panel screen](screens/order_dialog.png)



