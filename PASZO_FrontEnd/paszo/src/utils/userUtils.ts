import { IUserDto } from "../store/features/userSlice";

function getDisplayName(user: IUserDto): string {
  return `${user.name} ${user.secondName}`;
}

function getInitials(user: IUserDto): string {
  if (user.name && user.secondName) {
    const firstNameInitial = user.name.charAt(0);
    const lastNameInitial = user.secondName.charAt(0);
    return `${firstNameInitial}${lastNameInitial}`.toUpperCase();
  }
  return user.userName.slice(0, 2).toUpperCase();
}

export class UserDto implements IUserDto {
  userId: string;
  userName: string;
  isAdmin: boolean;
  name: string;
  secondName: string;
  email: string;
  telephoneNumber: string;
  initials: string;
  displayName: string;

  constructor(obj: IUserDto) {
    this.userId = obj.userId;
    this.userName = obj.userName;
    this.isAdmin = obj.isAdmin;
    this.name = obj.name;
    this.secondName = obj.secondName;
    this.email = obj.email;
    this.telephoneNumber = obj.telephoneNumber;
    this.initials = this.getInitials();
    this.displayName = this.getDisplayName();
  }

  public getDisplayName(): string {
    let displayName = "";

    if (this.name && this.secondName)
      displayName = `${this.name} ${this.secondName}`;
    else displayName = this.userName;

    return displayName;
  }

  public getInitials(): string {
    if (this.name && this.secondName) {
      const firstNameInitial = this.name.charAt(0);
      const lastNameInitial = this.secondName.charAt(0);
      return `${firstNameInitial}${lastNameInitial}`.toUpperCase();
    }
    return this.userName.slice(0, 2).toUpperCase();
  }
}
