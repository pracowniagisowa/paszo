export interface Response {
  Success: boolean;
  Message: string;
  Token: string;
}
