import { configureStore } from "@reduxjs/toolkit";
import { UserSlice } from "../store/features/userSlice";

export const createTestStore = () => {
  const store = configureStore({
    reducer: {
      userData: UserSlice.reducer,
    },
  });
  return store;
};
