import { IUserDto } from "../store/features/userSlice";

export interface IdentifiableWithName {
  id: number;
  name: string;
}

export interface IOrderData {
  restaurant: IRestaurantDto;
  totalPrice: number;
  todayOrders: IOrderDto[];
  todayDrawing: IDrawingHistory | null;
}

export interface IMenuItem {
  id: number;
  boxPrice: number;
  itemName: string;
  itemPrice: number;
  isOrdered: boolean;
}

export interface IRestaurantDto extends IdentifiableWithName {
  id: number;
  name: string;
  url: string;
  telephoneNumber: string;
}

export interface IOrderDto {
  id: number;
  user: IUserDto;
  menuItems: IMenuItem[];
  comment: string;
  userName: string;
  restaurantDto: IRestaurantDto;
  totalPrice: number;
}

export enum IDrawingType {
  Normal,
  LatePenalty,
}

export interface IDrawingHistory {
  drawingHistoryId: number;
  userName: string;
  drawingDate: Date;
  drawingType: IDrawingType;
}
