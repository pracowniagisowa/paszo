import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useAppDispatch } from "./store/store";
import { updateToken } from "./store/features/userSlice";
import useCookie from "react-use-cookie";

interface CookieTokenData {
  cookieToken: string | null;
  loading: boolean;
}

const useCookieToken = () => {
  const [cookieToken] = useCookie("token", "0");
  const [loading, setLoading] = useState<boolean>(true);
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  useEffect(() => {
    const checkToken = async () => {
      if (cookieToken && cookieToken !== "0") {
        dispatch(updateToken({ token: cookieToken }));
        navigate("/home");
      } else {
        navigate("/");
      }
      setLoading(false);
    };
    checkToken();
  }, [cookieToken, navigate]);

  return [cookieToken, loading];
};

export default useCookieToken;
