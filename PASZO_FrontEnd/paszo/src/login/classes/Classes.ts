export class Credentials {
  public Name: string;
  public SecondName: string;
  public UserName: string;
  public PaszoGroupName: string;
  public Password: string;
  public RepeatPassword: string;
  public Email: string;
  public TelephoneNumber: string;

  constructor() {
    this.Name = "";
    this.SecondName = "";
    this.UserName = "";
    this.PaszoGroupName = "";
    this.Password = "";
    this.RepeatPassword = "";
    this.Email = "";
    this.TelephoneNumber = "";
  }
}

export class Errors {
  public Name!: string;
  public UserName!: string;
  public PaszoGroupName!: string;
  public Password!: string;
  public RepeatPassword!: string;
  public Email!: string;
  public TelephoneNumber!: string;
  public SecondName!: string;

  public hasNullOrEmptyProps(): boolean {
    const props = Object.values(this);
    for (const prop of props) {
      if (!prop || prop.trim() === "") {
        return true;
      }
    }
    return false;
  }
}
