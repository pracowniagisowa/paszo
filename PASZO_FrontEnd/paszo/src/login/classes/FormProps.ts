import { ChangeEventHandler, FormEvent } from "react";
import { Credentials, Errors } from "./Classes";

export interface FormProps {
  credentials: Credentials;
  onInputChange: (event: ChangeEventHandler<HTMLInputElement>) => void;
  onSubmit: (event: FormEvent<HTMLFormElement>, submitionType: string) => void;
  onHasAccountChange: () => void;
  errors: Errors;
}
