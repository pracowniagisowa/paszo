import { baseUrl, handleResponse, handleError } from "../utils/apiUtils";
import { Credentials } from "./classes/Classes";

export function register(credentials: Credentials): Promise<any> {
  return fetch(baseUrl + "/account/register", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(credentials),
  })
    .then(handleResponse)
    .catch(handleError);
}

export function signUp(credentials: Credentials): Promise<any> {
  return fetch(baseUrl + "/Account/Login", {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(credentials),
  })
    .then(handleResponse)
    .catch(handleError);
}
