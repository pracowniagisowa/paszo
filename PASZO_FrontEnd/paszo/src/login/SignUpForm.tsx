import React from "react";
import { FormProps } from "./classes/FormProps";
import Input from "../home/components/inputs/Input";

function SignUpForm(props: FormProps) {
    const { credentials, onInputChange, onSubmit, onHasAccountChange, errors } = props;
    return (
        <form onSubmit={(event) => onSubmit(event, "register")} className="login--form">
            <h3>Sign up</h3>
            <Input
                id="UserName"
                label="Username"
                type="text"
                value={credentials.UserName}
                onChange={onInputChange}
                error={errors.UserName} />
            <Input
                id="Name"
                label="Name"
                type="text"
                value={credentials.Name}
                onChange={onInputChange}
                error={errors.Name} />
            <Input
                id="SecondName"
                label="SecondName"
                type="text"
                value={credentials.SecondName}
                onChange={onInputChange}
                error={errors.SecondName} />
            <Input
                id="Email"
                label="Email address"
                type="email"
                value={credentials.Email}
                onChange={onInputChange}
                error={errors.Email} />
            <Input
                id="Password"
                label="Password"
                type="password"
                value={credentials.Password}
                onChange={onInputChange}
                error={errors.Password} />
            <Input
                id="RepeatPassword"
                label="Confirm password"
                type="password"
                value={credentials.RepeatPassword}
                onChange={onInputChange}
                error={errors.RepeatPassword} />
            <input type="submit" value="Submittt" className="btn btn-primary main--button" />
            <br />
            <p className="login--form--link" onClick={onHasAccountChange}>Have an account? Sign In</p>
        </form>
    )
}

export default SignUpForm;