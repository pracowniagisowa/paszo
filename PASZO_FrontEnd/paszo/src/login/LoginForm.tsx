import React from "react";
import { FormProps } from "./classes/FormProps";
import Input from "../home/components/inputs/Input";

const LoginForm = (props: FormProps) =>
    <form className="login--form" onSubmit={(event) => props.onSubmit(event, "login")}>
        <h3>Sign in</h3>
        <Input
            id="UserName"
            label="Username"
            type="text"
            value={props.credentials.UserName}
            onChange={props.onInputChange}
            error={props.errors.UserName} />
        <Input
            id="Password"
            label="Password"
            type="password"
            value={props.credentials.Password}
            onChange={props.onInputChange}
            error={props.errors.Password} />

        <button type="submit" value="Submit" className="btn btn-primary main--button">Submit</button>
        <br />
        <p className="login--form--link" onClick={props.onHasAccountChange}>Don't have account? Sign Up</p>
    </form>


export default LoginForm;