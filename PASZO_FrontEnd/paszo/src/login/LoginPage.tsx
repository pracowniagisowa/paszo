import React, { useState } from "react";
import "./LoginPage.scss"
import spoons from "../imgs/favicon.png"
import background from "../imgs/dinner_background_right_transp_80.png"
import LoginForm from "./LoginForm";
import SignUpForm from "./SignUpForm";
import { Credentials, Errors } from "./classes/Classes";
import { register, signUp } from "./LoginService";
import { toast } from "react-toastify";
import { useNavigate } from 'react-router-dom'
import { useAppDispatch } from "../store/store";
import { updateToken } from "../store/features/userSlice";
import useCookie from 'react-use-cookie';


function LoginPage() {

    const [hasAccount, setHasAccount] = useState<boolean>(true);
    const [credentials, setCredentials] = useState<Credentials>(new Credentials());
    const [errors, setErrors] = useState<Errors>(new Errors());

    const navigate = useNavigate()
    const dispatch = useAppDispatch();
    const [token, setToken] = useCookie("token", "0");

    const handeSwitchHasAccount = () => {
        setErrors(new Errors());
        setHasAccount(!hasAccount);
    }
    const isLoginFormValid = (): boolean => {
        let _errors = new Errors()
        if (!credentials.UserName) _errors.UserName = "Username is required";
        if (!credentials.Password) _errors.Password = "Password is required";
        setErrors(_errors);
        return _errors.hasNullOrEmptyProps();
    }

    const isInputValid = (inputId: string, value: any) => {
        let _errors = new Errors()
        switch (inputId) {
            case "UserName":
                _errors.UserName = value ? "" : "Username is required";
                break;
            case "Password":
                _errors.Password = value ? "" : "Password is required";
                break;
            case "RepeatPassword":
                _errors.RepeatPassword = value === credentials.Password ? "" : "Passwords are different";
                break;
            default:
                break;
        }

        setErrors(_errors);
        return _errors.hasNullOrEmptyProps();
    }

    const handleInputChange = (event: any) => {
        const { id, value } = event.target;
        setCredentials({ ...credentials, [id]: value } as Credentials);

        isInputValid(id, value);
    };

    const handleSubmit = (event: any, submitionType: string) => {
        event.preventDefault();

        if (!isLoginFormValid()) return;

        if (submitionType === "register") {
            register(credentials)
                .then((resp) => {
                    if (resp.success) {
                        toast.success("User " + credentials.UserName + "created. Please sign in");
                        navigate("/");
                    } else {
                        toast.error("Could not add new user")
                    }
                });
        } else {
            signUp(credentials)
                .then((resp) => {
                    if (resp.success) {
                        setToken(resp.token);
                        dispatch(updateToken({ token: resp.token }));
                        navigate("/home");
                    }
                    else toast.error("login gone wrong");
                })
        }
    }


    return (
        <>
            <div className="login--container login--max-width-container">
                <div className="login--left-container">
                </div>
                <div className="login login--max-width">
                    <div className="login--header">
                        <img src={spoons} width={50} height={50} alt="" />
                    </div>
                    {hasAccount
                        ? <LoginForm
                            credentials={credentials}
                            onInputChange={handleInputChange}
                            onSubmit={handleSubmit}
                            onHasAccountChange={handeSwitchHasAccount}
                            errors={errors} />
                        : <SignUpForm
                            credentials={credentials}
                            onInputChange={handleInputChange}
                            onSubmit={handleSubmit}
                            onHasAccountChange={handeSwitchHasAccount}
                            errors={errors} />}
                    <div className="login--footer"></div>
                </div>

            </div>
        </>


    );


}
export default LoginPage