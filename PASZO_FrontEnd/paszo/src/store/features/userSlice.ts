import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface IUserDto {
  userId: string;
  userName: string;
  isAdmin: boolean;
  name: string;
  secondName: string;
  email: string;
  telephoneNumber: string;
}

export interface IUserData {
  token: string;
  user: IUserDto;
  balance: number;
}

export const initialState: IUserData = {
  token: "",
  user: {
    userId: "",
    userName: "",
    isAdmin: false,
    name: "",
    secondName: "",
    email: "",
    telephoneNumber: "",
  },
  balance: 0,
};

export const UserSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    updateToken: (state, action: PayloadAction<{ token: string }>) => {
      return {
        ...state,
        token: action.payload.token,
      };
    },
    updateUser: (state, action: PayloadAction<{ user: IUserDto }>) => {
      return {
        ...state,
        user: action.payload.user,
      };
    },
    updateBalance: (state, action: PayloadAction<{ balance: number }>) => {
      return {
        ...state,
        balance: action.payload.balance,
      };
    },
  },
});

export default UserSlice.reducer;
export const { updateToken, updateUser, updateBalance } = UserSlice.actions;
