import { useNavigate } from "react-router-dom";
import { useAppDispatch } from "../store/store";
import { updateToken } from "../store/features/userSlice";
import useCookie from "react-use-cookie";

const useLogin = () => {
  const [cookie, setCookie] = useCookie("token", "0");
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const logout = () => {
    navigate("/");

    dispatch(updateToken({ token: "0" }));
    setCookie("0");
  };

  return logout;
};

export default useLogin;
