import React, { useState, useEffect } from "react";
import "./HomePage.scss"
import { getUserByToken } from "./HomeService";
import { useAppDispatch, useAppSelector } from "../store/store";
import Navbar from "./Navbar";
import OrderPage from "./Order/OrderPage";
import AdminPage from "./Admin/AdminPage";
import UserPage from "./User/UserPage";
import { updateBalance, updateUser } from "../store/features/userSlice";
import { getUserChange } from "./Order/OrderService";
import useCookie from 'react-use-cookie';
import { updateToken } from "../store/features/userSlice";
import useCookieToken from "../useCookieToken";
import Header from "./Header";
import Footer from "./Footer"

export enum SubPage {
    Order,
    Admin,
    User
}

function HomePage() {
    const [cookieToken, loading] = useCookieToken();

    const [subPage, setSubPage] = useState<SubPage>(SubPage.Order);
    const dispatch = useAppDispatch();
    const token = useAppSelector((state) => state.userData.token);


    useEffect(() => {
        if (!loading) {
            getUserByToken(token)
                .then(json => {
                    dispatch(updateUser({ user: json }));
                    getUserChange(json.userId, token)
                        .then(json => {
                            dispatch(updateBalance({ balance: json }))
                        })
                })
        }
    }, [loading]);

    const handleSubPageChange = (subPage: SubPage) => {
        setSubPage(subPage);
    }

    const renderSubPage = () => {
        switch (subPage) {
            case SubPage.Order:
                return <OrderPage />;
            case SubPage.Admin:
                return <AdminPage />;
            case SubPage.User:
                return <UserPage />;
            default:
                break;
        }
    }

    return <div className="home-page">
        <Header onClick={handleSubPageChange} />
        {!loading && renderSubPage()}
        <Footer />
    </div>




}

export default HomePage;