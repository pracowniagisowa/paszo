import "./HomePage.scss"
import { SubPage } from "./HomePage";
import { useAppSelector } from "../store/store";
import spoons from "../imgs/favicon.png";
import { toast } from "react-toastify";
import { Initials } from "./components/Initials";
import { UserDto } from "../utils/userUtils";

export interface NavbarPros {
    onClick: (subPageName: SubPage) => void;
}

const Navbar = (props: NavbarPros) => {
    const { onClick } = { ...props }
    const user = new UserDto(useAppSelector(store => store.userData.user));


    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand">
                <img className="d-inline-block align-top navbar--spoons" src={spoons} width="30" height="30" />
                PASZO
            </a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item active">
                        <button className="nav-link" onClick={() => onClick(SubPage.Order)}>Dzisiejsze zamówienia </button>
                    </li>
                    {user.isAdmin && <li className="nav-item">
                        <button className="nav-link" onClick={() => onClick(SubPage.Admin)}>Panel administratora</button>
                    </li>}
                    <li className="nav-item">
                        <button className="nav-link" onClick={() => onClick(SubPage.User)}>Panel użytkownika</button>
                    </li>
                </ul>
            </div>
            <Initials initials={user.initials} onClick={() => toast.success("clicked")} />
        </nav>
    )
}

export default Navbar;