import { baseUrl, handleResponse, handleError } from "../utils/apiUtils";

export function getUserByToken(token: string) {
  return fetch(baseUrl + "/Account/GetUserByToken", {
    method: "GET",
    headers: new Headers({
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    }),
  })
    .then(handleResponse)
    .catch(handleError);
}
