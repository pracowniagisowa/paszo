import React, { useState } from "react";
import "./HomePage.scss"
import { SubPage } from "./HomePage";
import { useAppSelector } from "../store/store";
import spoons from "../imgs/favicon.png";
import hamburger from "../imgs/hamburger2.png"
import { toast } from "react-toastify";
import { Initials } from "./components/Initials";
import { UserDto } from "../utils/userUtils";
import useLogin from "../hooks/useLogin";

interface HeaderProps {
    onClick: (subPageName: SubPage) => void;
}

const Header = (props: HeaderProps) => {
    const { onClick } = { ...props }
    const user = new UserDto(useAppSelector(store => store.userData.user));

    const [userDropdown, setUserDropdown] = useState<boolean>()
    const [hamburgerDropdown, setHamburgerDropdown] = useState<boolean>()
    const logout = useLogin();


    const handleUserDropdown = () => {
        setUserDropdown(!userDropdown);
        setHamburgerDropdown(false);
    };

    const handleLogout = () => {
        setUserDropdown(false);
        logout();
    }

    const handleHamburgerDropdown = () => {
        setHamburgerDropdown(!hamburgerDropdown);
        setUserDropdown(false);
    };


    return <section className="header-section max-width-container">
        <div className="global-nav max-width">
            <nav className="global-header">
                <div className="dropdown-container global-header--hamburger--dropdown--container">
                    <button onClick={handleHamburgerDropdown} className="global-header--hamburger">
                        <img src={hamburger} width="35" height="35" />
                    </button>
                    {hamburgerDropdown && <div className="global-header--hamburger--dropdown--items dropdown-items">
                        <Links
                            user={user}
                            onClick={onClick}
                            itemClassName="global-header--hamburger--dropdown--item dropdown-item" />
                    </div>}
                </div>

                <Logo />
                <div className="global-header--links">
                    <Links
                        user={user}
                        onClick={onClick}
                        itemClassName="global-header--link" />

                </div>

                <div className="global-header--avatar--dropdown dropdown-container">
                    <button className="global-header--avatar--button" onClick={handleUserDropdown}>
                        <Initials initials={user.initials} diameter={"3em"} />
                    </button>
                    {userDropdown && <div className="global-header--avatar--dropdown-items dropdown-items">
                        <a href="#" className="global-header--avatar--dropdown-item dropdown-item">User settings</a>
                        <a onClick={handleLogout} className="global-header--avatar--dropdown-item dropdown-item">Logout</a>
                    </div>}

                </div>


            </nav>
        </div>

    </section>

}

const Logo = () => <div className="global-header--logo">
    <img src={spoons} width="60" height="60" alt="spoons" />
</div>

interface LinksProps {
    user: UserDto,
    onClick: (subPageName: SubPage) => void,
    itemClassName: string;
}

const Links = (props: LinksProps) => <>
    <a className={props.itemClassName} onClick={() => props.onClick(SubPage.Order)}>Today orders</a>
    {props.user.isAdmin && <a className={props.itemClassName} onClick={() => props.onClick(SubPage.Admin)}>Admin panel</a>}
    <a className={props.itemClassName} onClick={() => props.onClick(SubPage.User)}>User panel</a>
</>

export default Header;