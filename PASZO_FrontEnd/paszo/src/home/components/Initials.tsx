import { useMemo } from 'react';

export interface InitialsProps {
    initials: string,
    diameter?: string,
    onClick?: () => void
}

export const Initials = ({ initials, diameter = "50px", onClick = () => { } }: InitialsProps) => {
    const getColor = useMemo(() => {
        const s = 50; // Nasycenie w zakresie pastelowym
        const l = 80; // Jasność w zakresie pastelowym

        return () => {
            var hash = 0;
            for (var i = 0; i < initials.length; i++) {
                hash = initials.charCodeAt(i) + ((hash << 5) - hash);
            }

            var h = hash % 151; // Ograniczenie h do zakresu 0-244

            return 'hsl(' + h + ', ' + s + '%, ' + l + '%)';
        };
    }, [initials]);

    const color = getColor();

    return (<span className="avatar rounded-circle" style={{ width: diameter, height: diameter, background: color }}>{initials}</span>);
}


