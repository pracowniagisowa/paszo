import React from "react";
import "./Inputs.scss"

interface TextInputProps {
    id: string;
    type: string;
    label: string;
    value: string | number;
    onChange: (event: any) => void;
    inlineLabel?: boolean;
    error?: string;
    disabled?: boolean;
    className?: string;
}

const Input = ({ id, type, label, value, onChange, inlineLabel, error, disabled, className }: TextInputProps) => {

    let classname = "d-flex flex-column"
    if (inlineLabel)
        classname = "d-flex flex-row align-items-center"


    return (
        <div className={`input--container ${className}`}>
            <div className={classname}>
                <input value={value} type={type} id={id} onChange={onChange} step="0.01" disabled={disabled} required />
                <label htmlFor={id} >{label}</label>

            </div>
            {error && <p className="text-danger input--warning">{error}</p>}
        </div>
    )
}

export default Input;
