import React from "react";
import { IdentifiableWithName } from "../../../utils/dtos";

export interface SelectProps {
    id: string;
    label: string;
    values: IdentifiableWithName[];
    onChange: (event: any) => void;
    inlineLabel?: boolean;
    error?: string;
    className?: string;
}

const Select = ({ id, label, values, onChange, inlineLabel, className }: SelectProps) => {

    let classname = "d-flex flex-column"
    if (inlineLabel)
        classname = "d-flex flex-row align-items-center"

    return (
        <div className={`input ${className}`}>
            <div className={classname}>
                <label id={id} htmlFor={id} className="input--label">{label}</label>
                <select id={id} className="form-select" aria-label="Default select example" onChange={onChange}>
                    {values.map((v, index) => <option key={index} value={v.id}>{v.name}</option>)}
                </select>
            </div>
        </div>
    );
}

export default Select;