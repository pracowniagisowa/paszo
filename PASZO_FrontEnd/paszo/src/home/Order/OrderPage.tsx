import React, { useState, useEffect } from "react";
import "./OrderPage.scss";
import { useAppSelector } from "../../store/store";
import { getTodayOrder } from "./OrderService";
import { IOrderData } from "../../utils/dtos";
import OrderTable from "./OrderTable";
import OrderDialog from "./OrderDialog";

const OrderPage = () => {

    const [todayOrder, setTodayOrder] = useState<IOrderData[]>()
    const token = useAppSelector((state) => state.userData.token);

    useEffect(() => {
        handleTodayOrder()
    }, [token]);

    const handleTodayOrder = () => {
        getTodayOrder(token).then(json => setTodayOrder(json))
    }

    return (
        <>
            <section className="order-page--heading--section max-width-container">
                <div className="order-page--heading max-width">
                    <h3 className="">Orders:</h3>
                    <OrderDialog refreshOrderTable={handleTodayOrder} />
                </div>
            </section>
            <section className="order-page--orders--section max-width-container">
                <div className="order-page--orders max-width">
                    {todayOrder?.map((restaurantOrder, index) => <OrderTable key={index} orderData={restaurantOrder} />)}
                </div>
            </section>
        </>

    );
}

export default OrderPage;