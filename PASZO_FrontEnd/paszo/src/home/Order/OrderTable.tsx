import React, { useEffect, useState } from "react";
import { IMenuItem, IOrderData, IOrderDto } from "../../utils/dtos";
import { Initials } from "../components/Initials";
import { UserDto } from "../../utils/userUtils";
import { ITransaction } from "./classes";

export interface OrderTableProps {
    key: number,
    orderData: IOrderData
}

const OrderTable = (props: OrderTableProps) => {
    const [activeRowIndex, setActiveRowIndex] = useState<number>(-1);

    const handleActiveRow = (rowIndex: number) => {
        if (activeRowIndex !== rowIndex)
            setActiveRowIndex(rowIndex);
        else
            setActiveRowIndex(-1);
    }

    return (
        <>
            <div className="order-page--restaurant-info">
                <h4 className="font-weight-bold">{props.orderData.restaurant.name}</h4>
                <h5 className="font-weight-bold">Total amount: {props.orderData.totalPrice} zł</h5>
            </div>
            <div>
                <table className="order-page--table">
                    <thead>
                        <tr>
                            <th scope="col" className="col-1">No.</th>
                            <th scope="col" className="col-3">User</th>
                            <th scope="col" className="col-3">Order</th>
                            <th scope="col" className="col-4">Comment</th>
                            <th scope="col" className="col-4">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.orderData.todayOrders.map((order, index) =>
                            <TableRow
                                key={index}
                                index={index}
                                order={order}
                                activeRowIndex={activeRowIndex}
                                setRowActive={handleActiveRow} />)}

                    </tbody>
                </table>
            </div>
        </>
    )
}

interface TableRowProps {
    index: number,
    order: IOrderDto,
    activeRowIndex: number,
    setRowActive: (rowId: number) => void
}

const TableRow = ({ index, order, activeRowIndex, setRowActive }: TableRowProps) => {
    const user = new UserDto(order.user);
    const [rowClass, setRowClass] = useState<string>("");

    useEffect(() => {
        if (index === activeRowIndex)
            setRowClass("active-row")
        else
            setRowClass("");
    }, [activeRowIndex])

    return (
        <tr className={rowClass} onClick={() => setRowActive(index)}>
            <td data-title="No." className="order-page--table--index-data">{index + 1}</td>
            <td data-title="User">
                <div className="order-page--table--name">
                    <Initials initials={user.initials} diameter={"35px"} />
                    <div className="order-page--table--display-name">{user.displayName}</div>
                </div>

            </td>
            <td data-title="Order"><MenuItemsTable menuItems={order.menuItems} /></td>
            <td data-title="Comment">{order.comment}</td>
            <td data-title="Price">{order.totalPrice} zł</td>
        </tr>
    )
};

const MenuItemsTable = ({ menuItems }: { menuItems: IMenuItem[] }) => <>
    <ul>
        {menuItems.map((i, index) => <li key={index}>{i.itemName}</li>)}
    </ul>
</>

export default OrderTable;


