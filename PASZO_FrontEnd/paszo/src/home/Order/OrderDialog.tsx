import React, { useState, useEffect } from 'react';
import { Modal, Button } from "react-bootstrap";
import { ITransaction } from './classes';
import Input from '../components/inputs/Input';
import { IMenuItem, IRestaurantDto } from '../../utils/dtos';
import { getRestaurants, transactionAdd } from './OrderService';
import { useAppSelector } from '../../store/store';
import Select from '../components/inputs/Select';

export interface OrderDialogProps {
    refreshOrderTable: () => void;
}

const OrderDialog = ({ refreshOrderTable }: OrderDialogProps) => {

    const initMenuItem: IMenuItem = {
        id: 0,
        boxPrice: 0,
        itemName: "",
        itemPrice: 0,
        isOrdered: false,
    }

    const token = useAppSelector((state) => state.userData.token);
    const user = useAppSelector((state) => state.userData.user);
    const balance = useAppSelector((state) => state.userData.balance);

    const [show, setShow] = useState<boolean>();
    const [transaction, setTransaction] = useState<ITransaction>({
        Ammount: 0,
        Id: 0,
        Comment: "",
        UserName: "",
        RestaurantId: 0,
        Date: new Date(),
        TransactionAddedBy: "",
        MenuItems: [initMenuItem]
    });
    const [restaurants, setRestaurants] = useState<IRestaurantDto[]>([])
    const [sum, setSum] = useState<number>(0);
    const [toPay, setToPay] = useState<number>(0);



    const handleShow = (show: boolean) =>
        setShow(show);

    const handleTransaction = (event: any) => {
        const { id, value } = event.target;

        setTransaction({ ...transaction, [id]: value });
    }

    const handleMenuItems = (index: number, updatedMenuItem: IMenuItem) => {
        setTransaction(prevTransaction => {
            const updatedMenuItems = [...prevTransaction.MenuItems];
            updatedMenuItems[index] = updatedMenuItem;
            return {
                ...prevTransaction,
                MenuItems: updatedMenuItems
            };
        });
    };

    const handleAppendNewMenuItem = () => {
        let newMenuItems = [...transaction.MenuItems, initMenuItem];
        setTransaction({ ...transaction, MenuItems: newMenuItems });
    }

    const handleDeleteMenuItem = (index: number) => {
        const newMenuItems = transaction.MenuItems.filter((item, i) => i !== index);
        setTransaction({ ...transaction, MenuItems: newMenuItems });
    }

    const calculateSum = () => {
        let totalPrice: number = 0;

        for (const menuItem of transaction.MenuItems) {
            totalPrice += menuItem.itemPrice + menuItem.boxPrice;
        }
        setSum(totalPrice);
    }

    const calculateToPay = () => {
        let toPay = (sum - balance);
        if (toPay < 0) {
            toPay = 0;
        }
        setToPay(toPay);
    }

    const submitTransaction = () => {
        transactionAdd(user.userId, transaction, token)
            .then(() => {
                refreshOrderTable();
                handleShow(false);
            });
    }

    useEffect(() => {
        getRestaurants(token)
            .then(json => setRestaurants(json));
    }, []);
    useEffect(() => {
        calculateSum();
    }, [transaction]);
    useEffect(() => {
        calculateToPay();
    }, [sum, balance])

    return (<>
        <Button className="order-page--order-button main--button" onClick={() => handleShow(true)}>Create order</Button>
        <Modal show={show}
            size="lg">
            <Modal.Header closeButton>
                <Modal.Title>Order</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {transaction.MenuItems.map((item, index) => <MenuItemForm key={index} index={index} menuItem={item} onChange={handleMenuItems} onDelete={handleDeleteMenuItem} />)}
                <div className="d-flex justify-content-end">
                    <Button variant="secondary" size="sm" onClick={handleAppendNewMenuItem}>Add menu item</Button>
                </div>
                <div className="order-page--dialog--order-params">
                    <Input id="Sum"
                        label="Sum"
                        type="number"
                        value={sum}
                        onChange={() => { }}
                        disabled={true}
                        inlineLabel={true}
                        className="order-page--dialog--order-params--sum" />
                    <Input id="toPay"
                        label="To Pay"
                        type="number"
                        value={toPay}
                        onChange={() => { }}
                        disabled={true}
                        inlineLabel={true}
                        className="order-page--dialog--order-params--to-pay" />
                    <Input id="Balance"
                        label="Balance"
                        type="number"
                        value={balance}
                        onChange={() => { }}
                        disabled={true}
                        inlineLabel={true}
                        className="order-page--dialog--order-params--balance" />

                    <Select
                        id="RestaurantId"
                        label="Restaurant"
                        values={restaurants}
                        onChange={handleTransaction}
                        inlineLabel={true}
                        className="order-page--dialog--order-params--reataurant-dropdown" />
                    <Input
                        id="Comment"
                        label="Comment"
                        type="text"
                        value={transaction.Comment}
                        onChange={handleTransaction}
                        inlineLabel={true}
                        className="order-page--dialog--order-params--comment" />

                </div>

                <div className="row">
                    <div className="col-6">

                    </div>
                </div>
                <div className="row">
                    <div className="col-6">

                    </div>
                    <div className="col-6">

                    </div>
                </div>

                <div className="row">
                    <div className="col"></div>
                    <div className="col"></div>
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => { handleShow(false) }}>Close</Button>
                <Button variant="primary" className="main--button" onClick={() => { submitTransaction() }}>Save changes</Button>
            </Modal.Footer>

        </Modal>
    </>)
}

interface MenuItemFormProps {
    index: number,
    menuItem: IMenuItem,
    onChange: (index: number, updatedMenuItem: IMenuItem) => void
    onDelete: (index: number) => void;
}

const MenuItemForm = ({ index, menuItem, onChange, onDelete }: MenuItemFormProps) => {
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { id, value, type } = event.target;
        let val: string | number = value
        if (type === "number") {
            val = parseFloat(value)
        }

        const updatedMenuItem = { ...menuItem, [id]: val };
        onChange(index, updatedMenuItem);
    };

    return (
        <div className="order-page--dialog--menu-item">
            <div className="order-page--dialog--menu-item--index">
                {index + 1 + "."}
            </div>

            <Input
                id="itemName"
                label="Order Name"
                type="text"
                value={menuItem.itemName}
                onChange={handleChange}
                inlineLabel={true}
                className="order-page--dialog--menu-item--order-name" />


            <Input
                id="itemPrice"
                label="Price"
                type="number"
                value={menuItem.itemPrice}
                onChange={handleChange}
                inlineLabel={true}
                className="order-page--dialog--menu-item--price" />

            <Input
                id="boxPrice"
                label="Box price"
                type="number"
                value={menuItem.boxPrice}
                onChange={handleChange}
                inlineLabel={true}
                className="order-page--dialog--menu-item--box-price" />

            <div className="order-page--dialog--menu-item--delete-icon">
                <i className="bi bi-trash3" onClick={() => onDelete(index)}></i>
            </div>

        </div>
    )


}

export default OrderDialog;