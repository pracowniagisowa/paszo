import React from 'react';
import { render, screen } from '@testing-library/react';
import { getTodayOrder, getRestaurants } from "./OrderService"
import * as orderService from "./OrderService";
import userEvent from '@testing-library/user-event';
import OrderPage from './OrderPage';
import redux from 'react-redux';
import { Provider as ReduxProvider, useSelector } from "react-redux"
import { IUserData } from '../../store/features/userSlice';
import { createTestStore } from '../../utils/testUtils';


const initialState: IUserData = {
    token: "token",
    user: {
        userId: "",
        userName: "testUser",
        isAdmin: false,
        name: "Tester",
        secondName: "User",
        email: "",
        telephoneNumber: "",
    },
    balance: 15,
};
const todayOrder = [
    {
        "restaurant": {
            "id": 1,
            "name": "Restaurant A",
            "url": "",
            "telephoneNumber": ""
        },
        "totalPrice": 50.0,
        "todayOrders": [
            {
                "id": 65,
                "user": {
                    "userId": 2,
                    "userName": "jnowak",
                    "isAdmin": false,
                    "name": "Jan",
                    "secondName": "Nowak",
                    "retiredDate": null
                },
                "menuItems": [
                    {
                        "id": 67,
                        "boxPrice": 1.0,
                        "itemName": "Zupa",
                        "itemPrice": 8.0,
                        "isOrdered": false
                    },
                    {
                        "id": 68,
                        "boxPrice": 1.0,
                        "itemName": "Danie dnia",
                        "itemPrice": 15.0,
                        "isOrdered": false
                    }
                ],
                "comment": "",
                "userName": "jnowak",
                "restaurantDto": {
                    "id": 1,
                    "name": "Restaurant A",
                    "url": "",
                    "telephoneNumber": ""
                }
            },
            {
                "id": 66,
                "user": {
                    "userId": 3,
                    "userName": "zkowalski",
                    "isAdmin": false,
                    "name": "Zbyszek",
                    "secondName": "Kowalski",
                    "retiredDate": null
                },
                "menuItems": [
                    {
                        "id": 69,
                        "boxPrice": 1.0,
                        "itemName": "Zupa",
                        "itemPrice": 8.0,
                        "isOrdered": false
                    },
                    {
                        "id": 70,
                        "boxPrice": 1.0,
                        "itemName": "Danie dnia 2",
                        "itemPrice": 15.0,
                        "isOrdered": false
                    }
                ],
                "comment": "",
                "userName": "zkowalski",
                "restaurantDto": {
                    "id": 1,
                    "name": "Restaurant A",
                    "url": "",
                    "telephoneNumber": ""
                }
            }
        ],
        "todayDrawing": null
    },
    {
        "restaurant": {
            "id": 2,
            "name": "Restaurant B",
            "url": "",
            "telephoneNumber": ""
        },
        "totalPrice": 25.0,
        "todayOrders": [
            {
                "id": 67,
                "user": {
                    "userId": 1,
                    "userName": "admin",
                    "isAdmin": false,
                    "name": "",
                    "secondName": null,
                    "retiredDate": null
                },
                "menuItems": [
                    {
                        "id": 71,
                        "boxPrice": 1.0,
                        "itemName": "Zupa",
                        "itemPrice": 8.0,
                        "isOrdered": false
                    },
                    {
                        "id": 72,
                        "boxPrice": 1.0,
                        "itemName": "Danie dnia",
                        "itemPrice": 15.0,
                        "isOrdered": false
                    }
                ],
                "comment": "",
                "userName": "admin",
                "restaurantDto": {
                    "id": 2,
                    "name": "Restaurant B",
                    "url": "",
                    "telephoneNumber": ""
                }
            }
        ],
        "todayDrawing": null
    }
]
const restaurants = [
    {
        "id": 1,
        "name": "Restaurant A",
        "url": "",
        "telephoneNumber": ""
    },
    {
        "id": 2,
        "name": "Restaurant B",
        "url": "",
        "telephoneNumber": ""
    }
]

global.fetch = jest.fn();

jest.mock("./OrderService", () => ({
    getTodayOrder: jest.fn(),
    getRestaurants: jest.fn()
}));

jest.mock('react-redux', () => ({
    ...jest.requireActual('react-redux'),
    useSelector: jest.fn()
}));

let store: any;


describe("Today order", () => {
    beforeEach(() => {
        store = createTestStore();
        jest.spyOn(orderService, "getTodayOrder").mockImplementation(async () => Promise.resolve(todayOrder));
        jest.spyOn(orderService, "getRestaurants").mockImplementation(async () => Promise.resolve(restaurants));
    });

    it("should open order dialog", async () => {
        const user = userEvent.setup();
        render(
            <ReduxProvider store={store}>
                <OrderPage />
            </ReduxProvider>)

        const createOrderBtn = await screen.findByRole('button', {
            name: /create order/i
        })
        await user.click(createOrderBtn);

        const addMenuItemrBtn = await screen.findByRole('button', {
            name: /add menu item/i
        })
        expect(addMenuItemrBtn).toBeInTheDocument();

    })

    it("should select row on click", () => {
        const user = userEvent.setup();
        render(
            <ReduxProvider store={store}>
                <OrderPage />
            </ReduxProvider>)

        screen.logTestingPlaygroundURL();

    })
})



