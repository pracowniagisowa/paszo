import { IMenuItem } from "../../utils/dtos";

export interface ITransaction {
  Id: number;
  Ammount: number;
  Comment: string;
  UserName: string;
  MenuItems: IMenuItem[];
  RestaurantId: number;
  Date: Date;
  TransactionAddedBy: string;
}
