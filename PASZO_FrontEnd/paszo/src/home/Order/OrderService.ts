import { baseUrl, handleResponse, handleError } from "../../utils/apiUtils";
import { ITransaction } from "./classes";

export function getTodayOrder(token: string): Promise<any> {
  return fetch(baseUrl + "/Order/GetToday", {
    method: "GET",
    headers: new Headers({
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    }),
  })
    .then(handleResponse)
    .catch(handleError);
}

export function getRestaurants(token: string) {
  return fetch(baseUrl + "/Restaurant/GetRestaurants", {
    method: "GET",
    headers: new Headers({
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    }),
  })
    .then(handleResponse)
    .catch(handleError);
}

export function getUserChange(id: string, token: string) {
  return fetch(baseUrl + "/Account/GetUserChange" + "?userId=" + id, {
    method: "GET",
    headers: new Headers({
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    }),
  })
    .then(handleResponse)
    .catch(handleError);
}

export function transactionAdd(
  userId: string,
  transaction: ITransaction,
  token: string
) {
  return fetch(baseUrl + "/Transaction/Add" + "?userId=" + userId, {
    method: "POST",
    headers: new Headers({
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    }),
    body: JSON.stringify(transaction),
  })
    .then(handleResponse)
    .catch(handleError);
}
