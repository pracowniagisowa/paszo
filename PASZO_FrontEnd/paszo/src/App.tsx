import React, { useEffect } from 'react';
import "./Reset.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-icons/font/bootstrap-icons.css";
import "./App.scss";
import { Route, Routes, useNavigate } from 'react-router-dom';
import LoginPage from './login/LoginPage';
import HomePage from './home/HomePage';
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import useCookieToken from './useCookieToken';

function App() {

  useCookieToken();

  return (
    <div>
      <ToastContainer autoClose={3000} hideProgressBar />
      <Routes>
        <Route path="/" element={<LoginPage />} />
        <Route path="/home" element={<HomePage />} />
      </Routes>
    </div>
  );
}

export default App;
