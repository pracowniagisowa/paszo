using Microsoft.EntityFrameworkCore;
using PASZO_Core.Controllers;
using PASZO_Core.DatabaseModels;
using System;
using Xunit;

namespace PASZO_Core_Tests
{
    // https://www.youtube.com/watch?v=ddrR440JtiA

    public class PaszoUsersTest : IDisposable
    {
        protected readonly PaszoDbContext _context;

        public PaszoUsersTest()
        {
            var options = new DbContextOptionsBuilder<PaszoDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            _context = new PaszoDbContext(options);

            _context.Database.EnsureCreated();

            var paszoUsers = new[]
            {
                new PaszoUser{Name = "Maciek", UserName = "mfertala"},
                new PaszoUser{Name = "Zbyszek", UserName = "zwisniewski"}
            };

            _context.PaszoUsers.AddRange(paszoUsers);
            _context.SaveChanges();

        }

        [Fact]
        //public void CountCustumer()
        //{
        //    var controller = new UsersController(_context);

        //    var paszoUsersCount = controller.GetPaszoUsers().Count;

        //    Assert.True(paszoUsersCount == 2);
        //}
        

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}
