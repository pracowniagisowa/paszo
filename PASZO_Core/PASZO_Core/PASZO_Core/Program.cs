﻿using Microsoft.AspNetCore;
using NLog.Web;

namespace PASZO_Core
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
			try {
				logger.Debug("init main");
				BuildWebHost(args).Build().Run();
			} catch (Exception ex) {
				logger.Error(ex, "Stopped program because of exception");
				throw;
			} finally {
				NLog.LogManager.Shutdown();
			}
		}

		public static IWebHostBuilder BuildWebHost(string[] args) =>
		WebHost.CreateDefaultBuilder(args)
			.UseStartup<Startup>()
			.ConfigureLogging(logging =>
			{
				logging.ClearProviders();
				logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
			})
			.UseNLog();

	}
}
