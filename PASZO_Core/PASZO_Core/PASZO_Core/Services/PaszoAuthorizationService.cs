﻿using Microsoft.AspNetCore.Identity;
using PASZO_Core.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PASZO_Core.Services
{
	public class PaszoAuthorizationService : IPaszoAuthorizationService
	{
		private readonly UserManager<IdentityPaszoUser> _userManager;

		public PaszoAuthorizationService(UserManager<IdentityPaszoUser> userManager)
		{
			_userManager = userManager;

		}

		public async Task<bool> AuthorizeOwnerOrAdminAsync(ClaimsPrincipal user, string currentUserName)
		{
			var login = user.FindFirst(ClaimTypes.Name)?.Value;
			IdentityPaszoUser appUser = _userManager.Users.FirstOrDefault(u => u.UserName == login);
			var userRoles = await _userManager.GetRolesAsync(appUser);

			if (userRoles.Contains("Admin"))			
				return true;
			
			if (currentUserName == login)			
				return true;			

			return false;

		}
	}
}
