﻿using PASZO_Core.DatabaseModels;
using PASZO_Core.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Services
{
	public class TransactionService
	{
		public static List<TransactionDto> GetTransactionHistory(PaszoDbContext paszoDbContext, int monthsNumber)
		{
			List<TransactionDto> transactionDtos = new List<TransactionDto>();
			List<Tuple<int, int>> months = new List<Tuple<int, int>>();
			var currentMonth = DateTime.UtcNow;

			while (monthsNumber > 0) {
				months.Add(Tuple.Create(currentMonth.Year, currentMonth.Month));
				currentMonth = currentMonth.AddMonths(-1);
				monthsNumber--;
			}

			var transactions = paszoDbContext.Transactions
				.Where(x => months.Contains(Tuple.Create(x.Date.Year, x.Date.Month)))
				.OrderByDescending(x => x.Date);
			foreach (var transaction in transactions) {
				TransactionDto transactionDto = new TransactionDto();
				transactionDto.LoadAndReturn(transaction);
				transactionDtos.Add(transactionDto);
			}

			return transactionDtos;
		}
	}
}
