﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PASZO_Core.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Services
{
	public class CashBoxService
	{
		private ILogger<DrawingService> _logger;
		private static IConfiguration _configuration;

		public CashBoxService(IConfiguration configuration, ILogger<DrawingService> logger)
		{
			_configuration = configuration;
			_logger = logger;
		}


		public void SaveActualCashBoxState()
		{
			var actualDbCBS = GetActualCashBoxSatate();
			decimal databaseTotalChange;
			using (var paszoDbContext = new PaszoDbContext(_configuration)) {
				databaseTotalChange = OrderService.GetTotalChange(paszoDbContext);
			}
            var actualCBS = new CashBoxState(databaseTotalChange + actualDbCBS.ExtraCash)
            {
                ExtraCash = actualDbCBS.ExtraCash,
                DateTime = DateTime.UtcNow,
                CashDatabaseState = databaseTotalChange
            };

            using (var paszoDbContext = new PaszoDbContext(_configuration)) {
				paszoDbContext.CashBoxStates.Add(actualCBS);
				paszoDbContext.SaveChanges();
			}

		}

		public CashBoxState GetActualCashBoxSatate()
		{
			CashBoxState actualCashBoxState = null;
			using (var paszoDbContext = new PaszoDbContext(_configuration)) {
			var cashBoxState = paszoDbContext.CashBoxStates.ToList();

				actualCashBoxState = cashBoxState
					.OrderByDescending(x => x.DateTime)
					.FirstOrDefault(new CashBoxState(0));
			}

			return actualCashBoxState;
		}

		public bool IsActualCashBoxStateSavedToday()
		{
			bool isActualCashBoxStateSavedToday = false;
			using (var paszoDbContext = new PaszoDbContext(_configuration)) {
				isActualCashBoxStateSavedToday = paszoDbContext.CashBoxStates.Any(x => x.DateTime.Date == DateTime.UtcNow.Date);
			}

			return isActualCashBoxStateSavedToday;
		}
	}
}
