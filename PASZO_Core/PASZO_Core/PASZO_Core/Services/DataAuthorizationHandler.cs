﻿using Microsoft.AspNetCore.Authorization;
using PASZO_Core.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using static PASZO_Core.Services.DataAuthorizationHandler;

namespace PASZO_Core.Services
{
	public class DataAuthorizationHandler : AuthorizationHandler<SameAuthorOrAdminRequirement, Transaction>
	{
		protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, 
														SameAuthorOrAdminRequirement requirement, 
														Transaction transaction)
		{
			// TODO: or isAdmin!!!!!
			if (context.User.Identity?.Name == transaction.PaszoUser.UserName )
			{
				context.Succeed(requirement);
			}


			return Task.CompletedTask;
		}

		

		public class SameAuthorOrAdminRequirement : IAuthorizationRequirement
		{

		}
	}
}
