﻿using PASZO_Core.DatabaseModels;
using PASZO_Core.Models;

namespace PASZO_Core.Services
{
    public class OrderService
	{
		public static decimal GetTotalChange(PaszoDbContext paszoDbContext)
		{
			var transactions = paszoDbContext.Transactions
                                    .Select(a => a.Ammount)
                                    .ToList();

    		var totalChange = transactions.Sum();
			return totalChange;
		}

		public List<Order> EnumTodayOrders(PaszoDbContext paszoDbContext)
		{
			var today = DateTime.UtcNow.Date;

			var todayOrder = from o in paszoDbContext.Transactions
							 where o.Date.Date == today && o.MenuItems.Count > 0
							 select (new Order
							 {
								 Id = o.Id,
								 User = o.PaszoUser,
								 MenuItems = o.MenuItems,
								 Comment = o.Comment
							 });

			var todayOrdersList = todayOrder.ToList();
			return todayOrdersList;
		}

		public List<TodayOrdersAndDrawing> EnumTodayOrdersByRestaurants(PaszoDbContext paszoDbContext)
		{
			var today = DateTime.UtcNow.Date;
			Dictionary<Restaurant, List<Order>> todayOrdersAndDrawingsByRestaurant = new Dictionary<Restaurant, List<Order>>();
			List<TodayOrdersAndDrawing> ordersByRestaurants = new List<TodayOrdersAndDrawing>();
			var todayOrders = from o in paszoDbContext.Transactions
							  where o.Date.Date == today && o.MenuItems.Count > 0
							  select (new Order
							  {
								  Id = o.Id,
								  User = o.PaszoUser,
								  MenuItems = o.MenuItems,
								  Comment = o.Comment,
								  Restaurant = o.Restaurant != null ? o.Restaurant : new Restaurant() { Name = "nieokreślona"}
							 });


			foreach (var order in todayOrders) {

				if (todayOrdersAndDrawingsByRestaurant.ContainsKey(order.Restaurant)) {
					todayOrdersAndDrawingsByRestaurant[order.Restaurant].Add(order);
				} else {
					todayOrdersAndDrawingsByRestaurant.Add(order.Restaurant, new List<Order> { order });
				}
			}

			foreach (var item in todayOrdersAndDrawingsByRestaurant) {
				Restaurant restaurant = item.Key;
                TodayOrdersAndDrawing ordersAndDrawing = new TodayOrdersAndDrawing
                {
                    TotalPrice = item.Value
						.Select(o => o.MenuItems
										.Select(i => i.ItemPrice + i.BoxPrice)
										.Sum())
						.Sum(),
                    Restaurant = restaurant,
                    TodayOrders = item.Value,
                    // TodayDrawing = restaurant.GetTodayDrawingHistory(paszoDbContext)
                };

                ordersByRestaurants.Add(ordersAndDrawing);
			}

			return ordersByRestaurants;
		} 

		public bool IsAnyTodayOrder(PaszoDbContext paszoDbContext)
		{
			var todayOrders = EnumTodayOrders(paszoDbContext);
			return todayOrders.Count > 0;
		}

		public bool IsAnyTodayTransaction(PaszoDbContext paszoDbContext)
		{
			var today = DateTime.UtcNow.Date;
			var isAny = paszoDbContext.Transactions.Any(x => x.Date.Date == today);
			return isAny;
		}


		public List<AggregatedOrder> GetAgregatedOrder(PaszoDbContext paszoDbContext)
		{
			// List<Order> todayOrdersList = EnumTodayOrders(paszoDbContext);
			// var simplifiedOrders = todayOrdersList.Select(o => new
			// {
			// 	// Name = o.MenuItem.ItemName,
			// 	// Comment = o.Comment,
			// 	// Price = o.MenuItem.ItemPrice + o.MenuItem.BoxPrice
			// });

			// var countedOrders = simplifiedOrders
			// 	.GroupBy(x => new { x.Name, x.Comment, x.Price });

			// var grouped = countedOrders.Select(s => new AggregatedOrder {
			// 		OrderCount = s.Count(),
			// 		Name = s.Key.Name,
			// 		Comment = s.Key.Comment,
			// 		Price = s.Key.Price });
			// 	//.ToList();

			return new List<AggregatedOrder>(); //grouped.ToList();
		}

		public List<OrderToSent> PrepareOrdersToSent()
		{
			return new List<OrderToSent>();
		}


	}
}
