﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PASZO_Core.Services
{
	public interface IPaszoAuthorizationService
	{
		Task<bool> AuthorizeOwnerOrAdminAsync(ClaimsPrincipal user, string currentUserId);
	}
}
