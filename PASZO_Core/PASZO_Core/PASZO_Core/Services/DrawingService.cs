﻿using PASZO_Core.DatabaseModels;
using PASZO_Core.Models;

namespace PASZO_Core.Services
{
    public class DrawingService
	{
		private List<(Restaurant, List<DrawingTicket<Order>>)> _drawingTicketsByRestaurants;
		private List<DrawingTicket<Order>> _drawingTickets;
		private static IConfiguration _configuration;
		private readonly OrderService _orderService;
		private List<PaszoUser> _paszoUsersWithImmunity;
		private List<string> _paszoUsersWithYesterdayImmunity;
		private ILogger<DrawingService> _logger;

		static Random rand = new Random();

		public DrawingService(IConfiguration configuration, ILogger<DrawingService> logger)
		{
			_configuration = configuration;
			_orderService = new OrderService();
			_logger = logger;
			_drawingTicketsByRestaurants = new List<(Restaurant, List<DrawingTicket<Order>>)>();
		}

		public bool DrawUserFromOrders()
		{
			_paszoUsersWithImmunity = EnumUsersWithImmunity();
			_paszoUsersWithYesterdayImmunity = EnumUsersWithYesterdayImmunity();

			PopulateTicketsAndCalculateWeights();

			using (var paszoDbContext = new PaszoDbContext(_configuration)) {
				var penaltyDrawings = paszoDbContext.PenaltyDrawings
					.Where(x => x.IsPenaltyValid == true)
					.ToList();
				var penaltyDrawingUserIds = penaltyDrawings.Select(x => x.PaszoUserId).ToList();

				bool continueForNextRestourant = false;

				foreach (var drawingTicketsForRestaurant in _drawingTicketsByRestaurants) {
					continueForNextRestourant = false;
					foreach (var orderToCheck in drawingTicketsForRestaurant.Item2) {
						if (penaltyDrawingUserIds.Contains(orderToCheck.Feature.User.UserId)) {
							var penaltyDrawingToCancel = penaltyDrawings.FirstOrDefault(x => x.PaszoUserId == orderToCheck.Feature.User.UserId);
							penaltyDrawingToCancel.CancelPenalty();

							var draw = new DrawingHistory
							{
								UserName = orderToCheck.Feature.User.UserName,
								DrawingDate = DateTime.UtcNow,
								DrawingType = DrawingType.LatePenalty
							};

							var currentRestaurant = paszoDbContext.Restaurants.FirstOrDefault(x => x.Id == drawingTicketsForRestaurant.Item1.Id);
							currentRestaurant.DrawingHistory.Add(draw);
							_logger.LogInformation($"Dla '{currentRestaurant.Name}' wylosowano {draw.UserName} - {draw.DrawingDate}");
							paszoDbContext.SaveChanges();

							continueForNextRestourant = true;
							continue;
						}
					}
					if (continueForNextRestourant) {
						continue;
					}

					var order = Draw<Order>(drawingTicketsForRestaurant.Item2);
					if (order == null) {
						continue;
					}
					using (var dbContext = new PaszoDbContext(_configuration)) {
						var draw = new DrawingHistory
						{
							UserName = order.User.UserName,
							DrawingDate = DateTime.UtcNow,
							DrawingType = DrawingType.Normal
						};

						var currentRestaurant = dbContext.Restaurants.FirstOrDefault(x => x.Id == drawingTicketsForRestaurant.Item1.Id);
						currentRestaurant.DrawingHistory.Add(draw);

						var test = dbContext.DrawingHistory.ToList();
						_logger.LogInformation($"Dla '{currentRestaurant.Name}' wylosowano {draw.UserName} - {draw.DrawingDate}");

						dbContext.SaveChanges();

					}
				}
			}

			return true;
		}

		public DateTime GetLastDrawProjectedTime(TimeSpan defaultDrawingHour)
		{
			DateTime lastDrawingProjectedTime = DateTime.MinValue;

			using (var dbContext = new PaszoDbContext(_configuration)) {
				var lastDrawingHistory = dbContext.DrawingHistory
					.OrderByDescending(x => x.DrawingDate)
					.FirstOrDefault();
				
				DateTime lastDrawing = lastDrawingHistory != null ? lastDrawingHistory.DrawingDate : DateTime.MinValue;

				if (lastDrawing == DateTime.MinValue) {
					return lastDrawingProjectedTime;
				}

				if (lastDrawing.Date < DateTime.UtcNow.Date) {
					lastDrawingProjectedTime = new DateTime(
						lastDrawing.Date.Year,
						lastDrawing.Date.Month,
						lastDrawing.Date.Day,
						defaultDrawingHour.Hours,
						defaultDrawingHour.Minutes,
						defaultDrawingHour.Seconds);
				} else {
					lastDrawingProjectedTime = lastDrawing;
				}
			}
			return lastDrawingProjectedTime;
		}

		private T Draw<T>(List<DrawingTicket<T>> tickets)
		{
			double r = rand.NextDouble() * tickets.Sum(a => a.Weight);
			double min = 0;
			double max = 0;
			DrawingTicket<T> winner = null;
			foreach (var ticket in tickets) {
				max += ticket.Weight;
				//-----------
				if (min <= r && r < max) {
					winner = ticket;
					break;
				}
				//-----------
				min = max;
			}
			if (winner == null)
				return default(T);
			return winner.Feature;
		}


		private List<TodayOrdersAndDrawing> GetTodayOrders()
		{
			List<TodayOrdersAndDrawing> todayOrders = new List<TodayOrdersAndDrawing>();

			using (var dbContext = new PaszoDbContext(_configuration)) {
				var test = dbContext.PaszoUsers.FirstOrDefault();
				todayOrders = _orderService.EnumTodayOrdersByRestaurants(dbContext);
			}

			return todayOrders;
		}

		private void PopulateTicketsAndCalculateWeights()
		{
			_drawingTicketsByRestaurants = new List<(Restaurant, List<DrawingTicket<Order>>)>();
			var todayOrdersByRestauaurants = GetTodayOrders();

			foreach (var restaurant in todayOrdersByRestauaurants) {
				List<DrawingTicket<Order>> drawingTickets = new List<DrawingTicket<Order>>();
				foreach (var order in restaurant.TodayOrders) {
					DrawingTicket<Order> ticket = new DrawingTicket<Order>();
					ticket.Feature = order;
					ticket.Weight = GetOrderTicketWeight(order, true);
					drawingTickets.Add(ticket);
				}

				if (!AreTicketsDrawable(drawingTickets)) {
					drawingTickets = new List<DrawingTicket<Order>>();
					foreach (var order in restaurant.TodayOrders) {
						DrawingTicket<Order> ticket = new DrawingTicket<Order>();
						ticket.Feature = order;
						ticket.Weight = GetOrderTicketWeight(order, false);
						drawingTickets.Add(ticket);
					}
				}

				var restaurantTuple = (restaurant.Restaurant, drawingTickets);
				_drawingTicketsByRestaurants.Add(restaurantTuple);
			}
		}

		private bool AreTicketsDrawable<T>(List<DrawingTicket<T>> drawingTickets)
		{
			bool areTicketsDrawable = drawingTickets.Any(x => x.Weight > 0);
			return areTicketsDrawable;
		}

		private int GetOrderTicketWeight(Order order, bool useYesterdayImmunity)
		{
			
			if (order == null || order.MenuItems.Count == 0) {
				return 0;
			}
			if (_paszoUsersWithImmunity
				.Select(x => x.UserName)
				.Contains(order.User.UserName)) {
				return 0;
			}
			if (useYesterdayImmunity && _paszoUsersWithYesterdayImmunity.Contains(order.User.UserName)) {
				return 0;
			}
			if (IsStringFromListInMenuItem(EnumMenuItemNamesThatChangeDrawingProbability(AffectType.Increase), order)) {
				return 3;
			}

			if (IsStringFromListInMenuItem(EnumMenuItemNamesThatChangeDrawingProbability(AffectType.Reduce), order)) {
				return 1;
			}			
			
			return 2;
		}

		private bool IsStringFromListInMenuItem(List<string> strings, Order order)
		{
			// if (order.MenuItem.ItemName != null) {
			// 	if (strings.Any(x => order.MenuItem.ItemName.Contains(x))) {
			// 		return true;
			// 	}
			// }

			// if (order.Comment != null) {
			// 	if (strings.Any(x => order.Comment.Contains(x))) {
			// 		return true;
			// 	}
			// }

			return false;
		}

		private List<PaszoUser> EnumUsersWithImmunity()
		{
			List<PaszoUser> usersWithImmunity = new List<PaszoUser>();
			using (var dbContext = new PaszoDbContext(_configuration)) {
				usersWithImmunity = dbContext.DrawingImmunities.Select(x => x.PaszoUser).ToList();
			}
			return usersWithImmunity;
		}

		private List<string> EnumUsersWithYesterdayImmunity()
		{
			List<string> usersWithYesterdayImmunity = new List<string>();
			using (var dbContext = new PaszoDbContext(_configuration)) {
				DateTime yesterday = DateTime.UtcNow.Date.AddDays(-1);

				usersWithYesterdayImmunity = dbContext
					.DrawingHistory.Where(x => x.DrawingDate.Date == yesterday)
					.Select(u => u.UserName)
					.ToList();
			}
			return usersWithYesterdayImmunity;
		}

		public List<string> EnumMenuItemNamesThatChangeDrawingProbability(AffectType affectType)
		{
			using (var dbContext = new PaszoDbContext(_configuration)) {
				var menuItemNames = dbContext.MenuItemNameAffectDrawings
					.Where(m => m.AffectType == affectType)
					.Select(x => x.NamePart)
					.ToList();

				return menuItemNames;
			}
			//return new List<string>();
		}


	}
}
