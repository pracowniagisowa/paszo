﻿using PASZO_Core.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Services
{
	public class SettingsService
	{
		private readonly PaszoDbContext _paszoDbContext;


		public SettingsService(PaszoDbContext paszoDbContext)
		{
			_paszoDbContext = paszoDbContext;
		}

		#region BoolValue

		public bool GetBoolValue(string key)
		{
			var setting = _paszoDbContext.PaszoSettings.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null)
			{
				return setting.BoolValue;
			}
			return false;
		}

		public void SetBoolValue(string key, bool value)
		{
			var setting = _paszoDbContext.PaszoSettings.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null)
			{
				setting.BoolValue = value;
			}
			else
			{
				setting = new SettingsDb
				{
					SettingKey = key,
					BoolValue = value
				};
				_paszoDbContext.PaszoSettings.Add(setting);                
			}
			_paszoDbContext.SaveChanges();
		}

		#endregion

		#region IntValue

		public void SetIntValue(string key, int value)
		{
			var setting = _paszoDbContext.PaszoSettings.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				setting.IntValue = value;
			} else {
				setting = new SettingsDb
				{
					SettingKey = key,
					IntValue = value
				};
				_paszoDbContext.PaszoSettings.Add(setting);
			}
			_paszoDbContext.SaveChanges();
		}

		public int GetIntValue(string key)
		{
			var setting = _paszoDbContext.PaszoSettings.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				return setting.IntValue;
			}
			return 0;
		}

		#endregion

		#region StringValue

		public string GetStringValue(string key)
		{
			var setting = _paszoDbContext.PaszoSettings.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null)
			{
				return setting.StringValue;
			}
			return String.Empty;
		}



		#endregion

		#region FloatValue
		public void SetFloatValue(string key, float value)
		{
			var setting = _paszoDbContext.PaszoSettings.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				setting.FloatValue = value;
			} else {
				setting = new SettingsDb
				{
					SettingKey = key,
					FloatValue = value
				};
				_paszoDbContext.PaszoSettings.Add(setting);
			}
			_paszoDbContext.SaveChanges();
		}

		public float GetFloatValue(string key)
		{
			var setting = _paszoDbContext.PaszoSettings.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				return setting.FloatValue;
			}
			return 0;
		}

		#endregion

		#region DoubleValue
		public void SetDoubleValue(string key, double value)
		{
			var setting = _paszoDbContext.PaszoSettings.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				setting.DoubleValue = value;
			} else {
				setting = new SettingsDb
				{
					SettingKey = key,
					DoubleValue = value
				};
				_paszoDbContext.PaszoSettings.Add(setting);
			}
			_paszoDbContext.SaveChanges();
		}

		public double GetDoubleValue(string key)
		{
			var setting = _paszoDbContext.PaszoSettings.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				return setting.DoubleValue;
			}
			return 0;
		}

		#endregion

		#region DecimalValue
		public void SetDecimalValue(string key, decimal value)
		{
			var setting = _paszoDbContext.PaszoSettings.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				setting.DecimalValue = value;
			} else {
				setting = new SettingsDb
				{
					SettingKey = key,
					DecimalValue = value
				};
				_paszoDbContext.PaszoSettings.Add(setting);
			}
			_paszoDbContext.SaveChanges();
		}

		public decimal GetDecimalValue(string key)
		{
			var setting = _paszoDbContext.PaszoSettings.FirstOrDefault(s => s.SettingKey == key);
			if (setting != null) {
				return setting.DecimalValue;
			}
			return 0;
		}

		#endregion

	}
}
