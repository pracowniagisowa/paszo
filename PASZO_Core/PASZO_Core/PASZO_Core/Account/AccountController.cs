﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

using PASZO_Core.DatabaseModels;
using PASZO_Core.DTOs;
using PASZO_Core.Models;
using PASZO_Core.Services;

namespace PASZO_Core.Controllers
{
    [ApiController]
	[Route("[controller]/[action]")]
	[Authorize(AuthenticationSchemes = "Bearer")]
	public class AccountController : Controller
	{
		private readonly SignInManager<IdentityPaszoUser> _signInManager;
		private readonly IPaszoAuthorizationService _authorizationService;
		private readonly UserManager<IdentityPaszoUser> _userManager;
		private readonly IConfiguration _configuration;
		private readonly PaszoDbContext _paszoDbContext;
		private readonly SettingsService _paszoSettingsService;

		public AccountController(
			UserManager<IdentityPaszoUser> userManager,
			SignInManager<IdentityPaszoUser> signInManager,
			IConfiguration configuration,
			PaszoDbContext paszoDbContext,
			IPaszoAuthorizationService authorizationService
			)
		{
			_authorizationService = authorizationService;
			_userManager = userManager;
			_signInManager = signInManager;
			_configuration = configuration;
			_paszoDbContext = paszoDbContext;
			_paszoSettingsService = new SettingsService(_paszoDbContext); 
		}

		[AllowAnonymous]
		[HttpPut]
		public async Task<ActionResult> Login(Credentials credentials)
		{
			if (credentials.UserName == null || credentials.Password == null) {
				return BadRequest("Missing login or password");
			}
			var result = await _signInManager.PasswordSignInAsync(credentials.UserName, credentials.Password, false, false);

			if (result.Succeeded)
			{
				var appUser = _userManager.Users.SingleOrDefault(r => r.UserName == credentials.UserName);
				var tokenObject = await GenerateJwtToken(appUser);
				return Ok(new Response{Success = true, Token = tokenObject.ToString()});
			}
			else
			{
				return BadRequest(new Response {Success = false, Message = "Login failed"});
			}
		}

		[AllowAnonymous]
		[HttpPost]
		public async Task<IActionResult> Register(Credentials credentials)
		{
			// if (!_paszoSettingsService.GetBoolValue("IsAddingUserAviable"))			
			// 	return BadRequest("Dodawanie nowych użytkowników jest wyłączone. Aby mieć dostęp do dodawania nowych użytkoników sontaktuj się z administratorem");			

			PaszoUserGroup groupDb = null;
			if (credentials.PaszoGroupName != null)
				groupDb = _paszoDbContext.PaszoGroups.FirstOrDefault(x => x.GroupName == credentials.PaszoGroupName);
			else			
				groupDb = _paszoDbContext.PaszoGroups.FirstOrDefault(x => x.IsDefault);

			var user = new IdentityPaszoUser
			{
				UserName = credentials.UserName,
				PhoneNumber = credentials.TelephoneNumber,
				Email = credentials.Email,
				PaszoUser = new PaszoUser
				{
					Name = credentials.Name,
					SecondName = credentials.SecondName,
					UserName = credentials.UserName,
					UserGroup = groupDb
				}
			};
			
			var result = await _userManager.CreateAsync(user, credentials.Password);

			if (result.Succeeded) {
				await _userManager.AddToRoleAsync(user, "User");
				return Ok(new Response { Success = true, Message = "User created successfully" });
			} else {
				return Ok(new Response { Success = false, Message = result.Errors.First().Description });
			}
		}

		[AllowAnonymous]
		[HttpGet]
		public ActionResult IsRegisterAllowed()
		{
			return Ok(_paszoSettingsService.GetBoolValue("IsAddingUserAviable"));
		}

		[HttpPost]
		public async Task<ActionResult> SetPassword(string userId, [FromBody] NewCredentials newCredentials)
		{
			IdentityPaszoUser paszoUser = await _userManager.FindByIdAsync(userId);
			if (paszoUser == null)
			{
				return BadRequest("Nie ma takiego użytkownika w bazie danych");
			}

			if (await _authorizationService.AuthorizeOwnerOrAdminAsync(User, paszoUser.UserName))
			{
				var resoult = await _userManager.ChangePasswordAsync(paszoUser, newCredentials.Password, newCredentials.NewPassword );
				if (resoult.Succeeded)
				{
					return Ok("Sukces. Hasło zostało zmienione");

				}
				else
				{
					return Ok("Uwaga. Hasło nie zostało zmienione");
				}
			} 

			return new UnauthorizedResult();
		}


		[HttpPost]
		public async Task<ActionResult> ResetPassword([FromBody] NewCredentials newCredentials)
		{
			IdentityPaszoUser paszoUser = _userManager.Users.FirstOrDefault(u => u.UserName == newCredentials.UserName);
			if (paszoUser == null)
			{
				return BadRequest("Nie ma takiego użytkownika w bazie danych");
			}

			if (await _authorizationService.AuthorizeOwnerOrAdminAsync(User, paszoUser.UserName))
			{
				var resoult = await _userManager.RemovePasswordAsync(paszoUser);
				var resoultSet = await _userManager.AddPasswordAsync(paszoUser, newCredentials.NewPassword);
				if (resoultSet.Succeeded)
				{
					return Ok("Hasło zostało zresetowane");
				}
				return Ok(resoultSet);
			}
			return Unauthorized();

		}

		[HttpPost]
		public async Task<ActionResult> EditUserData(string userId, [FromBody] Credentials credentials)
		{
			IdentityPaszoUser identityPaszoUser = await _userManager.FindByIdAsync(userId);//Users.FirstOrDefault(u => u.UserName == userName);
			if (credentials == null || identityPaszoUser == null)
			{
				return BadRequest();
			}

			if (await _authorizationService.AuthorizeOwnerOrAdminAsync(User, identityPaszoUser.UserName))
			{
				identityPaszoUser.PaszoUser.Name = credentials.Name;
				identityPaszoUser.Email = credentials.Email;
				identityPaszoUser.PhoneNumber = credentials.TelephoneNumber;

				_paszoDbContext.SaveChanges();
				return Ok("user data edited");
			}
			else
			{
				return Unauthorized();
			}
		}

		[HttpGet]
		public async Task<ActionResult> GetUserByLogin(string login)
		{
			IdentityPaszoUser identityPaszoUser = _userManager.Users.FirstOrDefault(u => u.UserName == login);
			if (identityPaszoUser == null)
			{
				return BadRequest();
			}
			if (await _authorizationService.AuthorizeOwnerOrAdminAsync(User, login))
			{

				return Ok(
					new UserDto {
						UserId = identityPaszoUser.Id,
						UserName = identityPaszoUser.UserName,
						Name = identityPaszoUser.PaszoUser.Name,
						Email = identityPaszoUser.Email,
						TelephoneNumber = identityPaszoUser.PhoneNumber,
						IsAdmin = await _userManager.IsInRoleAsync(identityPaszoUser, "Admin")
					});
			}
			else
			{
				return Unauthorized();
			}

		}

		[HttpGet]
		public async Task<ActionResult> GetUserByToken()
		{
			var request = HttpContext.Request;
			var login = User.FindFirst(ClaimTypes.Name)?.Value;
			IdentityPaszoUser identityPaszoUser = _userManager.Users.FirstOrDefault(u => u.UserName == login);

			if (identityPaszoUser == null)
			{
				return BadRequest();
			}

			return Ok(
					new UserDto
					{
						UserId = identityPaszoUser.Id,
						UserName = identityPaszoUser.UserName,
						Name = identityPaszoUser.PaszoUser.Name,
						SecondName = identityPaszoUser.PaszoUser.SecondName,
						Email = identityPaszoUser.Email,
						TelephoneNumber = identityPaszoUser.PhoneNumber,
						IsAdmin = await _userManager.IsInRoleAsync(identityPaszoUser, "Admin")
					});

		}


		[HttpGet]
		public async Task<ActionResult> GetUserChange(string userId)
		{
			IdentityPaszoUser identityPaszoUser = await _userManager.FindByIdAsync(userId);//.Users.FirstOrDefault(u => u.UserName == currentUserName);
			if (identityPaszoUser == null)
			{
				return BadRequest();
			}
			if (await _authorizationService.AuthorizeOwnerOrAdminAsync(User, identityPaszoUser.UserName))
			{
				decimal sum = identityPaszoUser.PaszoUser.Transactions.Sum(a => a.Ammount);
				return Json(sum);
			}
			else
			{
				return Unauthorized();
			}
		}

		[HttpGet]
		public async Task<ActionResult> GetUserHistory(string userId)
		{
			IdentityPaszoUser identityPaszoUser = await _userManager.FindByIdAsync(userId);
			if (identityPaszoUser == null)
			{
				return BadRequest();
			}
			if (await _authorizationService.AuthorizeOwnerOrAdminAsync(User, identityPaszoUser.UserName))
			{
				var transactionHistory = identityPaszoUser.PaszoUser.Transactions
					.OrderByDescending(t => t.Date)
					.Select(o => new TransactionHistoryDto
					{
						Id = o.Id,
						MenuItems = o.MenuItems,
						Ammount = o.Ammount,
						Date = string.Format("{0}-{1}-{2}", o.Date.Year.ToString(), o.Date.Month.ToString(), o.Date.Day.ToString()),
						Comment = o.Comment
						//TODO: 
						//RestourantName = o.RestourantName
					});

				return Json(transactionHistory);
			}
			else
			{
				return Unauthorized();
			}
		}

		[AllowAnonymous]
		[HttpGet]
		public async Task<ActionResult> GetListOfAdmins()
		{
			IList<IdentityPaszoUser> identityPaszoAdmins = await _userManager.GetUsersInRoleAsync("Admin");
			List<string> paszoAdminsLogins = identityPaszoAdmins
				.Select(l => l.UserName)
				.Where(i => i != _configuration.GetSection("PowerUserSettings:Login").Value)
				.ToList();
			return Json(paszoAdminsLogins);
		}

		[Authorize (Roles = "Admin")]
		[HttpDelete]
		public async Task<ActionResult> RetireUser(string userId)
		{
			//var adminTest = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
			//IdentityPaszoUser appUser = _userManager.Users.FirstOrDefault(u => u.UserName == adminTest);
			//var userRoles = await _userManager.GetRolesAsync(appUser);

			IdentityPaszoUser user = await _userManager.FindByIdAsync(userId);
			int paszoUserId = user.PaszoUser.UserId;

			if (user != null)
			{
				var result = await _userManager.DeleteAsync(user);
				if (result.Succeeded)
				{
					PaszoUser paszoUserFromDb = _paszoDbContext.PaszoUsers.FirstOrDefault(p => p.UserId == paszoUserId);
					paszoUserFromDb.RetiredDate = DateTime.UtcNow;
					_paszoDbContext.SaveChanges();
					return Json("Użytkownik usunięty");
				}
				else
				{
					return Json(result.Errors);
				}
			}
			return NotFound("Nie znaleziono takiego użytkownika");            
		}

		private async Task<object> GenerateJwtToken(IdentityPaszoUser user)
		{
			var claims = new List<Claim>
			{
				new Claim(ClaimTypes.Name, user.UserName),
				new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
				new Claim(ClaimTypes.NameIdentifier, user.Id),
				//new Claim(ClaimTypes.Role,"The Role Of the logged in user, you can get from your DB")
			};
			// Get User roles and add them to claims
			var roles = await _userManager.GetRolesAsync(user);
			AddRolesToClaims(claims, roles);

			var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("JWTSettings:SecretKey").Value));
			var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

			var iss = _configuration.GetSection("JWTSettings:Issuer").Value;

			var token = new JwtSecurityToken(
				issuer: _configuration.GetSection("JWTSettings:Issuer").Value,
				audience: _configuration.GetSection("JWTSettings:Audience").Value,
				claims: claims, 
				signingCredentials: creds
				);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}

		private void AddRolesToClaims(List<Claim> claims, IEnumerable<string> roles)
		{
			foreach (var role in roles)
			{
				var roleClaim = new Claim(ClaimTypes.Role, role);
				claims.Add(roleClaim);
			}
		}

	}
}