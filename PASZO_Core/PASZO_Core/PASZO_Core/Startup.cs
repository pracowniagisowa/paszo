﻿using System.Text;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

using PASZO_Core.DatabaseModels;
using PASZO_Core.HostedServices;
using PASZO_Core.Services;
using static PASZO_Core.Services.DataAuthorizationHandler;

namespace PASZO_Core
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // Database context
            services.AddDbContext<PaszoDbContext>();

            // Identity configuration
            services.AddIdentity<IdentityPaszoUser, IdentityRole>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 1;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
            })
            .AddEntityFrameworkStores<PaszoDbContext>()
            .AddDefaultTokenProviders();

            // Authentication configuration
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.GetSection("JWTSettings:SecretKey").Value)),
                    ValidateIssuer = true,
                    ValidIssuer = Configuration.GetSection("JWTSettings:Issuer").Value,
                    ValidateAudience = true,
                    ValidAudience = Configuration.GetSection("JWTSettings:Audience").Value,
                    ValidateLifetime = false,
                };
            });

			

            // Authorization policies
            services.AddAuthorization(options =>
            {
                options.AddPolicy("OwningDataPolicy", policy =>
                    policy.Requirements.Add(new SameAuthorOrAdminRequirement()));
            });

			services.AddControllers();

			services.AddCors(options =>
            {
                options.AddPolicy("PaszoCorsPolicy", builder => {
					builder.WithOrigins("http://localhost:3000")
					.AllowAnyHeader()
					.AllowAnyMethod()
					.AllowCredentials();
						});
            });

            // Dependency injection
            services.AddSingleton<TimeScheduler>();
            services.AddSingleton<DrawingService>();
            services.AddSingleton<CashBoxService>();
            services.AddSingleton<IHostedService, TimeTicker>();
            services.AddSingleton<IAuthorizationHandler, DataAuthorizationHandler>();
            services.AddTransient<IPaszoAuthorizationService, PaszoAuthorizationService>();

            // Swagger
            services.AddSwaggerGen();
        }

        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            PaszoDbContext dbContext,
            IServiceProvider serviceProvider,
            RoleManager<IdentityRole> roleManager)
        {
			// CORS
            app.UseCors("PaszoCorsPolicy");

            // Swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Paszo");
            });

            // Routing
            app.UseRouting();
			// Use authentication
			app.UseAuthentication();
			app.UseAuthorization();

            // Endpoints
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            dbContext.Database.Migrate();
            CreateDefaultGroup(dbContext);
            CreateDefaultRestaurant(dbContext);
            CreateRoles(serviceProvider, roleManager, dbContext).Wait();
        }

        private void CreateDefaultGroup(PaszoDbContext dbContext){
            if (dbContext.PaszoGroups.Any(x => x.IsDefault))
                return;
            var defaultGroupName = Configuration.GetSection("PaszoSettings:DefaultGroup").Value;
            var defaultGroup = new PaszoUserGroup{
                    GroupName = defaultGroupName,
                    IsDefault = true
                };
            dbContext.PaszoGroups.Add(defaultGroup);
            dbContext.SaveChanges();
        }

        private void CreateDefaultRestaurant(PaszoDbContext dbContext){
            if (dbContext.Restaurants.Any(x => x.IsDefault))
                return;
            var defaultRestaurantName = Configuration.GetSection("PaszoSettings:DefaultRestaurant").Value;
            var defaultRestaurant = new Restaurant{
                Name = defaultRestaurantName,
                IsDefault = true,
                };
            dbContext.Restaurants.Add(defaultRestaurant);
            dbContext.SaveChanges();
        }

        private async Task CreateRoles(IServiceProvider serviceProvider, RoleManager<IdentityRole> roleManager, PaszoDbContext dbContext)
        {
            var UserManager = serviceProvider.GetRequiredService<UserManager<IdentityPaszoUser>>();
            var roleNames = Configuration.GetSection("UserRoles").Get<List<string>>();
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                var roleExist = await roleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    roleResult = await roleManager.CreateAsync(new IdentityRole(roleName));
                }
            }

            string powerUserUserName = Configuration.GetSection("PowerUserSettings:Login").Value;
            string powerUserPassword = Configuration.GetSection("PowerUserSettings:Password").Value;

            var user = await UserManager.FindByNameAsync(powerUserUserName);
            var defaultGroup = dbContext.PaszoGroups.FirstOrDefault(x => x.IsDefault);

            if (user == null)
            {
                var powerUser = new IdentityPaszoUser
                {
                    UserName = powerUserUserName,
                    PaszoUser = new PaszoUser
                    {
                        UserName = powerUserUserName,
                        UserGroup = defaultGroup
                    }
                };

                var createPowerUser = await UserManager.CreateAsync(powerUser, powerUserPassword);
                if (createPowerUser.Succeeded)
                {
                    await UserManager.AddToRoleAsync(powerUser, "Admin");
                }
            }
        }
    }
}
