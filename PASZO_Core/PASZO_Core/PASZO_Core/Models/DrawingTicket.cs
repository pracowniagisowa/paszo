﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Models
{
	public class DrawingTicket<T>
	{
		public T Feature { get; set; }
		public double Weight { get; set; }

	}
}
