﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Models
{
	public class AggregatedOrder
	{
		public int OrderCount { get; set; }
		public string Name { get; set; }
		public string Comment { get; set; }
		public decimal Price { get; set; }
	}
}
