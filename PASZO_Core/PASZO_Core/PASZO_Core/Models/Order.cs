﻿using PASZO_Core.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Models
{
	public class Order
	{
		public decimal _totalSum;
		public int Id { get; set; }
		public PaszoUser User { get; set; }
		public List<MenuItem> MenuItems { get; set; }
		public string Comment { get; set; }
		public Restaurant Restaurant { get; set; }

		public decimal TotalSum {
			get {return _totalSum; }
			set {_totalSum = MenuItems.Select(i => i.BoxPrice + i.ItemPrice).Sum();}
		}

	}
}
