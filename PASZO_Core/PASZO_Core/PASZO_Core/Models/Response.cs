using System;

namespace PASZO_Core.Models
{
    public class Response
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string Token { get; set; }
    }
}
