﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Models
{
	public class NewCredentials
	{
		public string UserName { get; set; }
		public string Password { get; set; }
		public string NewPassword { get; set; }
		
	}
}
