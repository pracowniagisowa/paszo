﻿using PASZO_Core.DatabaseModels;
using PASZO_Core.Dtos;
using PASZO_Core.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Models
{
	public class TodayOrdersAndDrawing
	{
		private decimal _totalSum;
		public Restaurant Restaurant { get; set; }
		public decimal TotalPrice { get; set; }
		public List<Order> TodayOrders { get; set; }
		public DrawingHistory TodayDrawing { get; set; }
		public decimal TotalSum { 
			get { return _totalSum; }
			set { _totalSum = TodayOrders.Select(o => o.TotalSum).Sum(); }
		}
	}
}
