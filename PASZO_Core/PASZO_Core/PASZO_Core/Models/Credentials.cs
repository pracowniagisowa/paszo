﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Models
{
	public class Credentials
	{
		public string UserName { get; set; }
		public string Name { get; set; }
		public string SecondName { get; set; }
		public string PaszoGroupName { get; set; }
		public string Password { get; set; }
		public string RepeatPassword { get; set; }
		public string Email { get; set; }
		public string TelephoneNumber { get; set; }
	}
}
