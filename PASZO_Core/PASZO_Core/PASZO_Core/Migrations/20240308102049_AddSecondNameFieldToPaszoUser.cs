﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PASZO_Core.Migrations
{
    public partial class AddSecondNameFieldToPaszoUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SecondName",
                table: "PaszoUsers",
                type: "TEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SecondName",
                table: "PaszoUsers");
        }
    }
}
