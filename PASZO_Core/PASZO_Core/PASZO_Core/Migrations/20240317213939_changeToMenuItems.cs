﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PASZO_Core.Migrations
{
    public partial class changeToMenuItems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_MenuItems_MenuItemId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_MenuItemId",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "MenuItemId",
                table: "Transactions");

            migrationBuilder.AddColumn<int>(
                name: "TransactionId",
                table: "MenuItems",
                type: "INTEGER",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MenuItems_TransactionId",
                table: "MenuItems",
                column: "TransactionId");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItems_Transactions_TransactionId",
                table: "MenuItems",
                column: "TransactionId",
                principalTable: "Transactions",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuItems_Transactions_TransactionId",
                table: "MenuItems");

            migrationBuilder.DropIndex(
                name: "IX_MenuItems_TransactionId",
                table: "MenuItems");

            migrationBuilder.DropColumn(
                name: "TransactionId",
                table: "MenuItems");

            migrationBuilder.AddColumn<int>(
                name: "MenuItemId",
                table: "Transactions",
                type: "INTEGER",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_MenuItemId",
                table: "Transactions",
                column: "MenuItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_MenuItems_MenuItemId",
                table: "Transactions",
                column: "MenuItemId",
                principalTable: "MenuItems",
                principalColumn: "Id");
        }
    }
}
