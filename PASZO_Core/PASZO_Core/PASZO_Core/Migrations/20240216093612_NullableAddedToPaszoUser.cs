﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PASZO_Core.Migrations
{
    public partial class NullableAddedToPaszoUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaszoUsers_PaszoGroups_UserGroupGroupId",
                table: "PaszoUsers");

            migrationBuilder.AlterColumn<int>(
                name: "UserGroupGroupId",
                table: "PaszoUsers",
                type: "INTEGER",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "Password",
                table: "PaszoUsers",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "PaszoUsers",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AddForeignKey(
                name: "FK_PaszoUsers_PaszoGroups_UserGroupGroupId",
                table: "PaszoUsers",
                column: "UserGroupGroupId",
                principalTable: "PaszoGroups",
                principalColumn: "GroupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaszoUsers_PaszoGroups_UserGroupGroupId",
                table: "PaszoUsers");

            migrationBuilder.AlterColumn<int>(
                name: "UserGroupGroupId",
                table: "PaszoUsers",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Password",
                table: "PaszoUsers",
                type: "TEXT",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "PaszoUsers",
                type: "TEXT",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PaszoUsers_PaszoGroups_UserGroupGroupId",
                table: "PaszoUsers",
                column: "UserGroupGroupId",
                principalTable: "PaszoGroups",
                principalColumn: "GroupId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
