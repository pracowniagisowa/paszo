﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PASZO_Core.DatabaseModels;
using PASZO_Core.Models;
using PASZO_Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.HostedServices
{
	public class TimeScheduler
	{
		private readonly DrawingService _drawingService;
		private readonly CashBoxService _cashBoxService;
		private IConfiguration _configuration;
		private OrderService _orderService;

		private TimeSpan _drawingTime;
		private DateTime _lastDrawDay = DateTime.MinValue;
		private ILogger<TimeScheduler> _logger;

		private static TimeSpan _cashBoxSaveTime = new TimeSpan(16, 0, 0);

		public TimeScheduler(DrawingService drawingService, CashBoxService cashBoxService, IConfiguration configuration, ILogger<TimeScheduler> logger)
		{
			_orderService = new OrderService();
			_drawingService = drawingService;
			_cashBoxService = cashBoxService;
			_configuration = configuration;
			_logger = logger;
		}

		public async Task PeriodActions()
		{
			DateTime now = DateTime.UtcNow;
			TimeSpan nowInDay = new TimeSpan(now.Hour, now.Minute, now.Second);

			#region Drawing

			using (var dbContext = new PaszoDbContext(_configuration)) {
				var settinsService = new SettingsService(dbContext);
				var orderService = new OrderService();
				if (settinsService.GetBoolValue("WithDrawing") && orderService.IsAnyTodayOrder(dbContext)) {
					var settingService = new SettingsService(dbContext);
					_drawingTime = new TimeSpan(0, settingService.GetIntValue("DrawingHourTotalMinutes"), 0);

					DateTime lastDrawProjected = _drawingService.GetLastDrawProjectedTime(_drawingTime);

					if (nowInDay.TotalMinutes > _drawingTime.TotalMinutes && now - lastDrawProjected > new TimeSpan(24, 0, 0)) {

						_drawingService.DrawUserFromOrders();

						_lastDrawDay = new DateTime(now.Year, now.Month, now.Day, _drawingTime.Hours, _drawingTime.Minutes, _drawingTime.Seconds);
					}
				}
			}

			#endregion

			#region CashBox

			using (var dbContext = new PaszoDbContext(_configuration)) {
				if (nowInDay >= _cashBoxSaveTime && _orderService.IsAnyTodayTransaction(dbContext)) {
					if (!_cashBoxService.IsActualCashBoxStateSavedToday()) {
						_cashBoxService.SaveActualCashBoxState();
					}
				}
			}

			#endregion

		}


	}
}
