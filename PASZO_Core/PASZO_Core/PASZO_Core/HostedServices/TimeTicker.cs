﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PASZO_Core.HostedServices
{
	public class TimeTicker : HostedService
	{
		private readonly TimeScheduler _timeScheduler;
		private IConfiguration _configuration;
		private readonly ILogger<TimeTicker> _logger;

		private DateTime _lastLoggingTime = DateTime.MinValue;

		public TimeTicker(TimeScheduler timeScheduler, ILogger<TimeTicker> logger)
			:base(logger)
		{
			_timeScheduler = timeScheduler;
			_logger = logger;
		}

		protected override async Task ExecuteAsync(CancellationToken cancellationToken)
		{
			while (!cancellationToken.IsCancellationRequested) {
				var nowUtc = DateTime.UtcNow;
				if (nowUtc -_lastLoggingTime > new TimeSpan(1, 0, 0)) {
					_logger.LogInformation("TickTock!");
					_lastLoggingTime = nowUtc;

				}

				try {
					await _timeScheduler.PeriodActions();
					await Task.Delay(TimeSpan.FromSeconds(5), cancellationToken);
				} catch (Exception ex) {
					_logger.LogError(ex, "TimeTicker error...");
				}

			}

		}
	}
}
