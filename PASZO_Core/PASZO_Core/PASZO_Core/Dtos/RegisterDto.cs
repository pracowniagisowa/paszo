﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Dtos
{
	public class RegisterDto
	{
		[Required]
		public string Email { get; set; }

		[Required]
		[StringLength(100, ErrorMessage = "PASSWORD_MIN_LENGTH", MinimumLength = 1)]
		public string Password { get; set; }
	}
}
