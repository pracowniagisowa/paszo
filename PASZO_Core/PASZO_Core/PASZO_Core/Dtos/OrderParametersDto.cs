﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO_Core.DTOs
{
	public class OrderParametersDto
	{
		public string TodayRestaurant { get; set; }
		public string Status { get; set; }
		public int OrderTime { get; set; }
		public int DeliveryTime { get; set; }
		public string Url { get; set; }
		public DateTime Date { get; set; }
		public int TelNr { get; set; }
		public RestaurantDto Restaurant { get; set; }
	}
}