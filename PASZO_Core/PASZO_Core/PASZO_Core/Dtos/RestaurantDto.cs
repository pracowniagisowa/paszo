﻿using PASZO_Core.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO_Core.DTOs
{
	public class RestaurantDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Url { get; set; }
		public string TelephoneNumber { get; set; }

		public RestaurantDto LoadAndReturn(Restaurant restaurant)
		{
			if (restaurant == null) {
				return null;;
			}

			Id = restaurant.Id;
			Name = restaurant.Name;
			Url = restaurant.Url;
			TelephoneNumber = restaurant.TelephoneNumber;

			return this;
		}
	}
}