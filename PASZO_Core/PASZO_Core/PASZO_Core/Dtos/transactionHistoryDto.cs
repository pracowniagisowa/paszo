﻿using PASZO.Models;
using PASZO_Core.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO_Core.DTOs
{
	public class TransactionHistoryDto
	{
		public int Id { get; set; }
		public List<MenuItem> MenuItems { get; set; }
		public decimal Ammount { get; set; }
		public string Date { get; set; }
		public string Comment { get; set; }
		public string RestourantName { get; set; }
	}
}