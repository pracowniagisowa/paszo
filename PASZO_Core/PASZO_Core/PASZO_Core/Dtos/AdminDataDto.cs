﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO.Models
{
	public class AdminDataDto
	{
		public string IdentityUserId { get; set; }
		public string UserName { get; set; }
		public string Name { get; set; }
		public decimal UserChange { get; set; }
		public decimal AdminPay { get; set; }
		public bool IsAdmin { get; set; }

		public AdminDataDto()
		{

		}

		public AdminDataDto(string userName, decimal userChange, bool isAdmin)
		{
			UserName = userName;
			UserChange = userChange;
			AdminPay = 0;
			IsAdmin = isAdmin;
		}
	}
}