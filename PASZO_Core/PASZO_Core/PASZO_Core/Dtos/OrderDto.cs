﻿using PASZO_Core.DatabaseModels;
using PASZO_Core.DTOs;
using PASZO_Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO_Core.Dtos
{
	public class OrderDto
	{
		public int Id { get; set; }
		public PaszoUserDto User { get; set; }
		public List<MenuItem> MenuItems { get; set; }
		public string Comment { get; set; }
		public string UserName { get; set; }
		public RestaurantDto RestaurantDto { get; set; }
		public decimal TotalPrice { get; set; } = 0;

		public OrderDto()
		{

		}

		public OrderDto(int id, PaszoUserDto user, List<MenuItem> menuItems, string comment)
		{
			this.Id = id;
			this.User = user;
			this.MenuItems = menuItems;
			this.Comment = comment;
		}

		public List<OrderDto> GetList(List<Order> orders)
		{
			List<OrderDto> orderDtos = new List<OrderDto>();
			foreach (var order in orders) {
				var dto = GetOrderDto(order);
				orderDtos.Add(dto);
			}
			return orderDtos;
		}

		private OrderDto GetOrderDto(Order order)
		{
            OrderDto orderDto = new OrderDto
            {
                Id = order.Id,
                User = new PaszoUserDto().LoadAndReturn(order.User),
                MenuItems = order.MenuItems,
                Comment = order.Comment,
                RestaurantDto = new RestaurantDto().LoadAndReturn(order.Restaurant),
				TotalPrice = order.MenuItems.Sum(x => x.ItemPrice + x.BoxPrice)
            };
            if (order.User != null) {
				orderDto.UserName = order.User.UserName;
			}

			return orderDto;
		}
	}
}