﻿using PASZO_Core.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Dtos
{
	public class PaszoUserDto
	{
		public int UserId { get; set; }
		public string UserName { get; set; }
		public bool IsAdmin { get; set; }
		public string Name { get; set; }
		public string SecondName { get; set; }
		public DateTime? RetiredDate { get; set; }

		public PaszoUserDto LoadAndReturn (PaszoUser paszoUser) {
			UserId = paszoUser.UserId;
			UserName = paszoUser.UserName;
			IsAdmin = paszoUser.IsAdmin;
			Name = paszoUser.Name;
			SecondName = paszoUser.SecondName;
			RetiredDate = paszoUser.RetiredDate;

			return this;
		}
	}
}
