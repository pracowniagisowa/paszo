﻿using PASZO.Models;
using PASZO_Core.DatabaseModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace PASZO_Core.Dtos
{
	public class TransactionDto
	{
		public int Id { get; set; }
		public decimal Ammount { get; set; }
		public string Comment { get; set; }
		public string UserName { get; set; }
		public List<MenuItem> MenuItems { get; set; }
		public int RestaurantId { get; set; }
		public DateTime Date { get; set; }
		public string TransactionAddedBy { get; set; }

		public TransactionDto(){}
		public TransactionDto(Transaction transaction)
		{
			Id = transaction.Id;
			Comment = transaction.Comment;
			UserName = transaction.PaszoUser.UserName;
			MenuItems = transaction.MenuItems;
			Date = transaction.Date;
			
			TransactionAddedBy = transaction.TransactionAddedBy;
		}

		public TransactionDto LoadAndReturn(Transaction transaction)
		{
			Id = transaction.Id;
			Comment = transaction.Comment;
			UserName = transaction.PaszoUser.UserName;
			MenuItems = transaction.MenuItems;
			Date = transaction.Date;
			TransactionAddedBy = transaction.TransactionAddedBy;

			return this;
		}
	}
}