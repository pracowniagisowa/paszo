﻿using PASZO_Core.DatabaseModels;
using PASZO_Core.DTOs;
using PASZO_Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Dtos
{
	public class TodayOrdersAndDrawingDto
	{
		public RestaurantDto Restaurant { get; set; }
		public decimal TotalPrice { get; set; }
		public List<OrderDto> TodayOrders { get; set; }
		public DrawingHistory TodayDrawing { get; set; }

		public TodayOrdersAndDrawingDto LoadAndReturn(TodayOrdersAndDrawing todayOrdersAndDrawing)
		{
			RestaurantDto restaurantDto = new RestaurantDto();
			Restaurant = restaurantDto.LoadAndReturn(todayOrdersAndDrawing.Restaurant);
			TotalPrice = todayOrdersAndDrawing.TotalPrice;
			OrderDto orderDto = new OrderDto();
			TodayOrders = orderDto.GetList(todayOrdersAndDrawing.TodayOrders);
			TodayDrawing = todayOrdersAndDrawing.TodayDrawing;

			return this;
		}
	}
}
