﻿using PASZO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Dtos
{
	public class AdminParametersDto
	{
		public CashBoxStateDto ActualCashBoxState { get; set; }
		public List<AdminDataDto> AdminDataList { get; set; }

		public AdminParametersDto()
		{
			AdminDataList = new List<AdminDataDto>();
			ActualCashBoxState = new CashBoxStateDto();

		}
	}
}
