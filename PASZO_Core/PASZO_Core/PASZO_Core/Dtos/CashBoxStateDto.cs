﻿using PASZO_Core.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Dtos
{
	public class CashBoxStateDto
	{
		public decimal CashState { get; set; }
		public decimal CashDatabaseState { get; set; }
		public decimal ExtraCash { get; set; }
		public decimal CashChange { get; set; }
		public string Comment { get; set; }
		public string UserName { get; set; }
		public string Name { get; set; }
		public DateTime DateTime { get; set; }
		public string Time { get { return $"{DateTime.ToShortDateString()} {DateTime.ToShortTimeString()}"; } }

		public void LoadFromEntity(CashBoxState cashBoxState)
		{
			if (cashBoxState == null) {
				return;
			}
			CashState = cashBoxState.CashState;
			CashDatabaseState = cashBoxState.CashDatabaseState;
			ExtraCash = cashBoxState.ExtraCash;
			Comment = cashBoxState.Comment;
			DateTime = cashBoxState.DateTime;
			if (cashBoxState.PaszoUser != null) {
				UserName = cashBoxState.PaszoUser.UserName;
				Name = cashBoxState.PaszoUser.Name;

			} else {
				UserName = "wpis automatyczny";
			}
		}
	}
}
