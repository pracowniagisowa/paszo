﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO_Core.DTOs
{
	public class UserDto
	{
		public string UserId { get; set; }
		public string UserName { get; set; }
		public bool IsAdmin { get; set; }
		public string Name { get; set; }
		public string SecondName { get; set; }
		public string Email { get; set; }
		public string TelephoneNumber { get; set; }
	}
}