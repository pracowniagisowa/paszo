﻿	using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PASZO_Core.DatabaseModels;

namespace PASZO_Core.Controllers
{
	[Route("[controller]/[action]")]
	public class UsersController : Controller
	{
		private readonly SignInManager<IdentityPaszoUser> _signInManager;
		private readonly IAuthorizationService _authorizationService;
		private readonly UserManager<IdentityPaszoUser> _userManager;
		private readonly IConfiguration _configuration;
		private readonly PaszoDbContext _paszoDbContext;
		

		public UsersController(
			UserManager<IdentityPaszoUser> userManager,
			SignInManager<IdentityPaszoUser> signInManager,
			PaszoDbContext paszoDbContext,
			IConfiguration configuration,
			IAuthorizationService authorizationService
			)
		{
			_authorizationService = authorizationService;
			_userManager = userManager;
			_paszoDbContext = paszoDbContext;
		}

		[HttpGet]
		public List<PaszoUser> GetPaszoUsers()
		{
			// var test = HttpContext.User;
			// var user =  _userManager.GetUserAsync(HttpContext.User).Result;
			// var role =_userManager.GetRolesAsync(user).Result;
			var paszoUsers = _paszoDbContext.PaszoUsers.ToList();
			return paszoUsers;
		}
	}
}		
