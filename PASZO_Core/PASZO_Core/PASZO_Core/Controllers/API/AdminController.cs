﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NLog;
using NLog.Targets;
using PASZO.Models;
using PASZO_Core.DatabaseModels;
using PASZO_Core.Dtos;
using PASZO_Core.DTOs;
using PASZO_Core.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Controllers
{
	[Authorize(AuthenticationSchemes = "Bearer")]
	[Route("[controller]/[action]")]
	public class AdminController : Controller
	{
		//private readonly SignInManager<IdentityUser> _signInManager;
		private readonly UserManager<IdentityPaszoUser> _userManager;
		private readonly IConfiguration _configuration;
		private readonly PaszoDbContext _paszoDbContext;
		private readonly SettingsService _paszoSettingsService;
		private readonly DrawingService _drawingService;


		public AdminController(
			PaszoDbContext paszoDbContext,
			IConfiguration configuration,
			UserManager<IdentityPaszoUser> userManager,
			DrawingService drawingService
			)
		{
			_paszoDbContext = paszoDbContext;
			_configuration = configuration;
			_userManager = userManager;
			_paszoSettingsService = new SettingsService(_paszoDbContext);
			_drawingService = drawingService;
		}

		#region Admin Parameters

		[Authorize(Roles = "Admin")]
		[HttpGet]
		public async Task<ActionResult> GetAdminParameters()
		{
			AdminParametersDto adminParametersDto = new AdminParametersDto();

			string powerUserUserName = _configuration.GetSection("PowerUserSettings:Login").Value;
			List<AdminDataDto> adminDataList = new List<AdminDataDto>();

			var usersList = _paszoDbContext.Users;

			foreach (var user in usersList) {
				if (user.UserName == powerUserUserName) {
					continue;
				}

				if (user.PaszoUser == null) {
					continue;
				}

				AdminDataDto adminDataDto = new AdminDataDto
				{
					IdentityUserId = user.Id,
					UserName = user.UserName,
					Name = user.PaszoUser.Name,
					UserChange = user.PaszoUser.Transactions.Sum(t => t.Ammount),
					IsAdmin = await _userManager.IsInRoleAsync(user, "Admin")
				};
				adminDataList.Add(adminDataDto);
			}

			adminParametersDto.AdminDataList = adminDataList;

			var cashBoxState = _paszoDbContext.CashBoxStates.OrderByDescending(x => x.DateTime).FirstOrDefault();
			if (cashBoxState != null) {
				adminParametersDto.ActualCashBoxState.LoadFromEntity(cashBoxState);
			}
			
			adminParametersDto.ActualCashBoxState.CashDatabaseState = OrderService.GetTotalChange(_paszoDbContext);
			adminParametersDto.ActualCashBoxState.CashState
				= adminParametersDto.ActualCashBoxState.CashDatabaseState + adminParametersDto.ActualCashBoxState.ExtraCash;

			decimal test = adminParametersDto.ActualCashBoxState.CashState;

			return Json(adminParametersDto);
		}

		[Authorize(Roles = "Admin")]
		[HttpGet]
		public async Task<ActionResult> SwichAdminRole(string userId)
		{
			string adminRole = "Admin";
			var user = await _userManager.FindByIdAsync(userId); // FirstOrDefault(u => u.UserName == userName);
			if (user == null)
			{
				return BadRequest("Użytkownik nie znaleziony w bazie danych");
			}
			//var roles = await _userManager.GetRolesAsync(user);
			if (await _userManager.IsInRoleAsync(user, adminRole))
			{
				var test = await _userManager.RemoveFromRoleAsync(user, adminRole);
				return Ok("Użytkownik usunięty z listy administratorów");
			}
			else
			{
				var test = await _userManager.AddToRoleAsync(user, adminRole);
				return Ok("Użytkownik dodany do listy administratorów");

			}
		}

		#endregion

		#region Settings

		[Authorize(Roles = "Admin")]
		[HttpGet]
		public ActionResult SwichIsAddingUserAviable()
		{
			bool currentIsAddingUserAviableValue = _paszoSettingsService.GetBoolValue("IsAddingUserAviable");
			_paszoSettingsService.SetBoolValue("IsAddingUserAviable", !currentIsAddingUserAviableValue);

			bool isAddingUserAviableValue = _paszoSettingsService.GetBoolValue("IsAddingUserAviable");
			return Ok(isAddingUserAviableValue);
		}


		[Authorize(Roles = "Admin")]
		[HttpGet]
		public async Task<ActionResult> GetAdminDataListAsync()
		{
			string powerUserUserName = _configuration.GetSection("PowerUserSettings:Login").Value;
			List<AdminDataDto> adminDataList = new List<AdminDataDto>();

			var usersList = _paszoDbContext.Users;

			foreach (var user in usersList)
			{
				if (user.UserName == powerUserUserName)
				{
					continue;
				}

				AdminDataDto adminDataDto = new AdminDataDto
				{
					IdentityUserId = user.Id,
					UserName = user.UserName,
					UserChange = user.PaszoUser.Transactions.Sum(t => t.Ammount),
					IsAdmin = await _userManager.IsInRoleAsync(user, "Admin")
				};
				adminDataList.Add(adminDataDto);
			}            

			return Json(adminDataList.OrderBy(a => a.UserName));
		}

		[Authorize(Roles = "Admin")]
		[HttpPost]
		public ActionResult PostPaszoGroup([FromBody] PaszoUserGroup paszoUserGroup)
		{
			if (paszoUserGroup == null || paszoUserGroup.GroupName == null) {
				return BadRequest("Błędna struktura przekazywanego obiektu");
			}

			if (_paszoDbContext.PaszoGroups.FirstOrDefault(g => g.GroupName == paszoUserGroup.GroupName) != null) {
				return BadRequest("Grupa o takiej nazwie znajduje się już w bazie danych");
			}

			_paszoDbContext.PaszoGroups.Add(paszoUserGroup);
			_paszoDbContext.SaveChanges();
			return Ok();

		}

		#endregion

		#region History

		[Authorize(Roles = "Admin")]
		[HttpGet]
		public ActionResult GetTransactionsHistory(int monthsNumber)
		{
			var transactionsHistory = TransactionService.GetTransactionHistory(_paszoDbContext, monthsNumber);
			return Json(transactionsHistory);
		}

		#endregion

		#region Drawing

		[Authorize(Roles = "Admin")]
		[HttpPost]
		public ActionResult PostUserToImmunity(string userName)
		{
			var user = _userManager.Users.FirstOrDefault(u => u.UserName == userName);
			if (user == null || user.PaszoUser == null) {
				return BadRequest("Brak takiego użytkownika w bazie danych");
			}

			var usersWithImmunity = _paszoDbContext.DrawingImmunities.ToList();
			if (usersWithImmunity.Any(x => x.PaszoUser.UserName == userName)) {
				return BadRequest("Ten użytkownik ma już immunitet");
			}

			_paszoDbContext.DrawingImmunities.Add(new DrawingImmunity { PaszoUser = user.PaszoUser });
			_paszoDbContext.SaveChanges();
			return Ok();
		}

		[Authorize(Roles = "Admin")]
		[HttpGet]
		public ActionResult GetUsersWithImmunity()
		{
			var usersWithImmunity = _paszoDbContext.DrawingImmunities
				.Select(x => new { x.PaszoUser.UserName})
				.ToList();
			return Json(usersWithImmunity);
		}

		[Authorize(Roles = "Admin")]
		[HttpDelete]
		public ActionResult DeleteUserWithImmunity(string userName)
		{
			var user = _userManager.Users.FirstOrDefault(x => x.UserName == userName); // FirstOrDefault(u => u.UserName == userName);
			if (user == null) {
				return BadRequest("Użytkownik nie znaleziony w bazie danych");
			}

			var userWithImmunity = _paszoDbContext.DrawingImmunities.FirstOrDefault(x => x.PaszoUser.UserName == userName);
			if (userWithImmunity == null) {
				return BadRequest("Użytkownik nie znaleziony na liście immunitetów");
			}

			_paszoDbContext.DrawingImmunities.Remove(userWithImmunity);
			_paszoDbContext.SaveChanges();

			return Ok();
		}


		[Authorize(Roles = "Admin")]
		[HttpPost]
		public ActionResult PostMenuItemNameAffectDrawing([FromBody] MenuItemNameAffectDrawing menuItemNameAffectDrawing)
		{
			if (menuItemNameAffectDrawing == null || menuItemNameAffectDrawing.NamePart == null || menuItemNameAffectDrawing.AffectType == 0) {
				return BadRequest("Niepełne dane. NamePart oraz AffectType nie może być puste");
			}

			_paszoDbContext.MenuItemNameAffectDrawings.Add(menuItemNameAffectDrawing);
			_paszoDbContext.SaveChanges();
			return Ok();
		}

		[Authorize(Roles ="Admin")]
		[HttpGet]
		public ActionResult GetMenuItemNamesThatReduceDrawingProbability()
		{			
			//var strings = _drawingService.EnumMenuItemNamesThatChangeDrawingProbability(AffectType.Reduce);

			var affectingStrings = _paszoDbContext.MenuItemNameAffectDrawings
				.Where(x => x.AffectType == AffectType.Reduce)
				.ToList();

			return Json(affectingStrings);
		}

		[Authorize(Roles = "Admin")]
		[HttpGet]
		public ActionResult GetMenuItemNamesThatIncreaseDrawingProbability()
		{
			//var strings = _drawingService.EnumMenuItemNamesThatChangeDrawingProbability(AffectType.Increase);
			var affectingStrings = _paszoDbContext.MenuItemNameAffectDrawings
				.Where(x => x.AffectType == AffectType.Increase)
				.ToList();

			return Json(affectingStrings);
		}

		[Authorize(Roles ="Admin")]
		[HttpDelete]
		public ActionResult DeleteMenuItemNamesThatChangeDrawingProbability(int id)
		{
			var menuItemName = _paszoDbContext.MenuItemNameAffectDrawings.FirstOrDefault(x => x.Id == id);
			if (menuItemName == null) {
				return BadRequest("Brak MenuItemName o wsakzanym Id");
			}

			_paszoDbContext.MenuItemNameAffectDrawings.Remove(menuItemName);
			_paszoDbContext.SaveChanges();
			return Ok();
		}
		
		[Authorize(Roles = "Admin")]
		[HttpGet]
		public ActionResult GetDrawingTimeTotalMinutesUtc()
		{
			return Ok(_paszoSettingsService.GetIntValue("DrawingHourTotalMinutes"));
		}

		[Authorize(Roles = "Admin")]
		[HttpPost]
		public ActionResult PostDrawingTimeTotalMinutesUtc(int drawingTotalMinutes)
		{

			_paszoSettingsService.SetIntValue("DrawingHourTotalMinutes", drawingTotalMinutes);
			return Ok();
		}


		#endregion

		#region Logs


		[Authorize(Roles ="Admin")]
		[HttpGet]
		public async Task<ActionResult> GetLogFile(string namePart)
		{
			var test = (FileTarget)LogManager.Configuration.AllTargets.FirstOrDefault(x => x.Name.Contains(namePart));
			if (test == null) {
				return NotFound();
			}

			var logEventInfo = new LogEventInfo();
			var filepath = test.FileName.Render(logEventInfo);
			//return Json(filename);

			var memory = new MemoryStream();
			using (var stream = new FileStream(filepath, FileMode.Open)) {
				await stream.CopyToAsync(memory);
			}
			memory.Position = 0;
			return File(memory, "text/plain", Path.GetFileName(filepath));
		}

		#endregion

		#region Diagnostics

		[Authorize(Roles = "Admin")]
		[HttpGet]
		public ActionResult GetDataBasePath()
		{
			var connection = _paszoDbContext.Database.GetDbConnection();
			var path = connection.ConnectionString;
			return Json(path.ToString());
		}

		[Authorize(Roles = "Admin")]
		[HttpPost]
		public ActionResult SetExtraCash(decimal extraCash)
		{
			_paszoSettingsService.SetDecimalValue("ExtraCash", extraCash);
			return Ok();
		}

		#endregion

		#region Depreciated

		[Authorize(Roles = "Admin")]
		[HttpGet]
		public ActionResult GetTotalChange()
		{
			decimal change = _paszoDbContext.Transactions
				.Select(a => a.Ammount)
				.DefaultIfEmpty(0)
				.Sum();

			return Ok(change);
		}

		#endregion

	}
}
