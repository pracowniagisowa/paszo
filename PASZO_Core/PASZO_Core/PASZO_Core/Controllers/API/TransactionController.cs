﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

using PASZO_Core.DatabaseModels;
using PASZO_Core.Dtos;
using PASZO_Core.Models;
using PASZO_Core.Services;

namespace PASZO_Core.Controllers
{
	[ApiController]
	[Route("[controller]/[action]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
	public class TransactionController : Controller
	{
		private readonly SignInManager<IdentityPaszoUser> _signInManager;
		private readonly IPaszoAuthorizationService _authorizationService;
		private readonly UserManager<IdentityPaszoUser> _userManager;
		private readonly IConfiguration _configuration;
		private readonly PaszoDbContext _paszoDbContext;
		private SettingsService _settinsService;


		public TransactionController(
			UserManager<IdentityPaszoUser> userManager,
			SignInManager<IdentityPaszoUser> signInManager,
			IConfiguration configuration,
			PaszoDbContext paszoDbContext,
			IPaszoAuthorizationService authorizationService
			)
		{
			_authorizationService = authorizationService;
			_userManager = userManager;
			_signInManager = signInManager;
			_configuration = configuration;
			_paszoDbContext = paszoDbContext;
			_settinsService = new SettingsService(_paszoDbContext);
		}

		[HttpPost]
		public async Task<ActionResult> Add(string userId, [FromBody] TransactionDto transaction)
		{
			var user = await _userManager.FindByIdAsync(userId);
			if (user == null || transaction == null)
				return BadRequest();
			
			// if (transaction.MenuItems != null && (transaction.MenuItems.Any(i => i.ItemPrice < 0) || transaction.MenuItem.BoxPrice < 0))
			// 	return BadRequest("Cena nie może być ujemna");

			Restaurant restaurant = null;

			if (transaction.MenuItems.Count != 0) {
				restaurant = _paszoDbContext.Restaurants.FirstOrDefault(x => x.Id == transaction.RestaurantId);
				restaurant ??= _paszoDbContext.Restaurants.FirstOrDefault(x => x.IsDefault);
			}

			if (await _authorizationService.AuthorizeOwnerOrAdminAsync(User, user.UserName)) {

                Transaction newTransaction = new Transaction {
					Ammount = transaction.Ammount,
                    MenuItems = transaction.MenuItems,
                    Restaurant = restaurant,
                    Comment = transaction.Comment,
					TransactionAddedBy = user.UserName,
                };

				if (_settinsService.GetBoolValue("WithDrawing") && IsCloseToDrawingTime()) {
					_paszoDbContext.PenaltyDrawings.Add(new PenaltyDrawing(user.PaszoUser.UserId, user.PaszoUser.UserName));
				}				

				user.PaszoUser.Transactions.Add(newTransaction);
				_paszoDbContext.SaveChanges();

				return Ok(new Response{Success = true});
			} else {
				return Unauthorized(new Response {Success = false});
			}
		}

		private bool IsCloseToDrawingTime()
		{
			int drawingTimeUTC = _settinsService.GetIntValue("DrawingHourTotalMinutes");
			DateTime now = DateTime.UtcNow;
			TimeSpan nowInDayUTC = new TimeSpan(now.Hour, now.Minute, now.Second);

			return nowInDayUTC.TotalSeconds + 5 > drawingTimeUTC * 60;
		}

		[HttpPut]
		public async Task<ActionResult> Edit(int id, [FromBody] TransactionDto newTransaction)
		{
			if (newTransaction == null) {
				return BadRequest();
			}

			// if (newTransaction.MenuItem.ItemPrice < 0 || newTransaction.MenuItem.BoxPrice < 0) {
			// 	return BadRequest("Cena nie może być ujemna");
			// }

			Transaction transactionFromDb = _paszoDbContext.Transactions.FirstOrDefault(t => t.Id == id);
			if (transactionFromDb != null) {
				if (await _authorizationService.AuthorizeOwnerOrAdminAsync(User, transactionFromDb.PaszoUser.UserName)) {
					transactionFromDb.MenuItems = newTransaction.MenuItems;
					transactionFromDb.Comment = newTransaction.Comment;
					transactionFromDb.SetAmmount();
					_paszoDbContext.SaveChanges();

					return new OkResult();
				}

				return new UnauthorizedResult();
			}
			else {
				return new BadRequestResult();
			}
		}

		[HttpGet]
		public async Task<ActionResult> Get(int id)
		{
			Transaction transaction = _paszoDbContext.Transactions.FirstOrDefault(t => t.Id == id);
			if (transaction == null){
				return BadRequest("Brak takiej transakcji w bazie danych");
			}

			if (await _authorizationService.AuthorizeOwnerOrAdminAsync(User, transaction.PaszoUser.UserName)){
				return Json(
					new TransactionDto {
						Id = transaction.Id,
						Date = transaction.Date,
						MenuItems = transaction.MenuItems,
						Comment = transaction.Comment
					});
			}
			return new UnauthorizedResult();
		}

		[HttpDelete]
		public async Task<ActionResult> Del(int id)
		{
			Transaction transaction = _paszoDbContext.Transactions.FirstOrDefault(t => t.Id == id);
			if (transaction == null){
				return BadRequest("Brak takiej transakcji w bazie danych");
			}

			if (await _authorizationService.AuthorizeOwnerOrAdminAsync(User, transaction.PaszoUser.UserName)){
				_paszoDbContext.Transactions.Remove(transaction);
				_paszoDbContext.SaveChanges();
				return Ok();
			}
			return new UnauthorizedResult();
		}
	}
}
