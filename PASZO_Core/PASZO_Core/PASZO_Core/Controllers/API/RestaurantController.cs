﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PASZO_Core.DatabaseModels;
using PASZO_Core.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.Controllers.API
{
	[Authorize(AuthenticationSchemes = "Bearer")]
	[Route("[controller]/[action]")]
	public class RestaurantController : Controller
	{
		private readonly PaszoDbContext _paszoDbContext;


		public RestaurantController(PaszoDbContext paszoDbContext)
		{
			_paszoDbContext = paszoDbContext;
		}


		[Authorize(Roles = "Admin")]
		[HttpPost]
		public ActionResult PostRestaurant([FromBody] Restaurant restaurant)
		{
			if (restaurant == null && restaurant.Name == null) {
				return BadRequest("Błędna struktura przekazywanego obiektu");
			}
			if (_paszoDbContext.Restaurants.FirstOrDefault(r => r.Name == restaurant.Name) != null) {
				return BadRequest("Restauracja o takiej nazwie znajduje się już w bazie danych");
			}

			_paszoDbContext.Restaurants.Add(restaurant);
			_paszoDbContext.SaveChanges();
			return Ok("Restauracja dodana do bazy danych");
		}

		[Authorize(Roles = "User, Admin")]
		[HttpGet]
		public ActionResult GetRestaurants()
		{
			var restaurants = _paszoDbContext.Restaurants
				.Where(x => x.Deleted == false)
				.Select(r => new RestaurantDto()
				{
					Id = r.Id,
					Name = r.Name,
					Url = r.Url,
					TelephoneNumber = r.TelephoneNumber
				})
				.ToList();

			return Json(restaurants);
		}

		[Authorize(Roles = "Admin")]
		[HttpDelete]
		public ActionResult DelRestaurant(int id)
		{
			Restaurant restaurant = _paszoDbContext.Restaurants.FirstOrDefault(r => r.Id == id);
			if (restaurant == null) {
				return BadRequest("Błędny Id restauracji");
			}
			restaurant.Deleted = true;
			//_paszoDbContext.Restaurants.Remove(restaurant);
			_paszoDbContext.SaveChanges();

			return Ok();
		}

		//TODO: pprawić na [Authorize]
		[Authorize]
		[HttpPost]
		public ActionResult PostMenuItemToRestourant(string restaurantName, [FromBody] MenuItem menuItem)
		{
			Restaurant restaurant = _paszoDbContext.Restaurants.FirstOrDefault(r => r.Name == restaurantName);
			if (restaurant == null || menuItem == null) {
				return BadRequest();
			}

			restaurant.MenuItemsList.Add(menuItem);
			_paszoDbContext.SaveChanges();
			return Ok();
		}

		[Authorize]
		[HttpGet]
		public ActionResult GetMenuItemsForRestaurant(string restaurantName)
		{
			Restaurant restaurant = _paszoDbContext.Restaurants.FirstOrDefault(r => r.Name == restaurantName);
			if (restaurant == null) {
				return BadRequest();
			}

			var menuItems = restaurant.MenuItemsList;
			return Json(menuItems);
		}
	}
}
