﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PASZO_Core.DatabaseModels;
using PASZO_Core.Dtos;
using PASZO_Core.DTOs;
using PASZO_Core.Models;
using PASZO_Core.Services;

namespace PASZO_Core.Controllers
{
    [ApiController]
	[Route("[controller]/[action]")]
	[Authorize(AuthenticationSchemes = "Bearer")]

	public class OrderController : Controller
	{
		private readonly SignInManager<IdentityPaszoUser> _signInManager;
		private readonly IPaszoAuthorizationService _authorizationService;
		private readonly UserManager<IdentityPaszoUser> _userManager;
		private readonly IConfiguration _configuration;
		private readonly PaszoDbContext _paszoDbContext;
		private readonly OrderService _orderService;


		public OrderController(
			UserManager<IdentityPaszoUser> userManager,
			SignInManager<IdentityPaszoUser> signInManager,
			IConfiguration configuration,
			PaszoDbContext paszoDbContext,
			IPaszoAuthorizationService authorizationService
			)
		{
			_authorizationService = authorizationService;
			_userManager = userManager;
			_signInManager = signInManager;
			_configuration = configuration;
			_paszoDbContext = paszoDbContext;
			_orderService = new OrderService();
		}

		[HttpGet]
		public ActionResult GetToday()
		{
			List<Order> todayOrdersList = _orderService.EnumTodayOrders(_paszoDbContext);
			DrawingHistory todayDrawing = _paszoDbContext.DrawingHistory.FirstOrDefault(x => x.DrawingDate.Date == DateTime.UtcNow.Date);

			var ordersByRestaurants = _orderService.EnumTodayOrdersByRestaurants(_paszoDbContext);
			List<TodayOrdersAndDrawingDto> ordersByRestaurantsDto = new List<TodayOrdersAndDrawingDto>();

			foreach (var item in ordersByRestaurants) {
				TodayOrdersAndDrawingDto todayOrdersAndDrawingDto = new TodayOrdersAndDrawingDto();
				todayOrdersAndDrawingDto.LoadAndReturn(item);
				ordersByRestaurantsDto.Add(todayOrdersAndDrawingDto);
			}

			return Ok(ordersByRestaurantsDto);
		}

		[HttpGet]
		public ActionResult GetAggregated()
		{
			DrawingHistory todayDrawing = _paszoDbContext.DrawingHistory.FirstOrDefault(x => x.DrawingDate.Date == DateTime.UtcNow.Date);

			List<AggregatedOrder> aggregatedOrders = _orderService.GetAgregatedOrder(_paszoDbContext);

			var aggregatedOrdersAndTodayDrawiang = new
			{
				TodayDrawing = todayDrawing,
				AggregatedOrders = aggregatedOrders
			};

			return Json(aggregatedOrdersAndTodayDrawiang);
		}


		[HttpGet]
		public string getTodaySum()
		{
			var today = DateTime.UtcNow.Date;

			var todayOrders = _paszoDbContext.Transactions.Where(t => t.Date.Date == today && t.MenuItems.Count != 0).ToList();
			var todaySum = todayOrders.Sum(s => s.Ammount);
			var todaySumReversed = todaySum * -1;

			return todaySumReversed.ToString("0.00");
		}

		#region TodayOrder

		//[Authorize(Roles = "Admin, User")]
		[HttpGet]
		public ActionResult GetTodayOrderParameters()
		{
			var today = DateTime.UtcNow.Date;
			OrderParameters todayOrderParams = _paszoDbContext.OrderParameters.FirstOrDefault(o => o.Date == today);

			if (todayOrderParams == null) {
				todayOrderParams = new OrderParameters();
				SetDefaultsToOrder(todayOrderParams);
				_paszoDbContext.OrderParameters.Add(todayOrderParams);
				_paszoDbContext.SaveChanges();
			}

			//Restaurant todayRestaurant = _paszoDbContext.Restaurants
			//	.FirstOrDefault(x => x.Deleted == false && x.Name == todayOrderParams.TodayRestaurant);

			RestaurantDto todayRestaurantDto = new RestaurantDto();

			todayRestaurantDto.LoadAndReturn(todayOrderParams.Restaurant);

			OrderParametersDto orderParametersDto = new OrderParametersDto
			{
				TodayRestaurant = todayOrderParams.TodayRestaurant,
				OrderTime = todayOrderParams.OrderTimeMinutes,
				DeliveryTime = todayOrderParams.DeliveryTimeMinutes,
				Url = todayOrderParams.Url,
				Date = todayOrderParams.Date,
				Status = todayOrderParams.Status,
				Restaurant = todayRestaurantDto
				

			};
			return Json(orderParametersDto);
		}


		[HttpPost]
		public ActionResult PostTodayOrderParameters(OrderParametersDto inputOrderParameters)
		{
			if (inputOrderParameters == null) {
				return BadRequest();
			}

			var today = DateTime.UtcNow.Date;

			OrderParameters todayOrderParameters = _paszoDbContext.OrderParameters.FirstOrDefault(o => o.Date == today);

			if (todayOrderParameters == null) {
				OrderParameters newOrderParams = new OrderParameters();
				UpdateTodayOdrerParametersObject(newOrderParams, inputOrderParameters);
				_paszoDbContext.OrderParameters.Add(newOrderParams);
				_paszoDbContext.SaveChanges();
			} else {

				UpdateTodayOdrerParametersObject(todayOrderParameters, inputOrderParameters);
				_paszoDbContext.SaveChanges();
			}

			if (inputOrderParameters.Status == "Zamówione") {
				var todayTransactions = _paszoDbContext.Transactions.Where(t => t.Date.Date == today && t.MenuItems.Count != 0);
				foreach (var transaction in todayTransactions) {
					foreach (var menuItem in transaction.MenuItems) {
						menuItem.IsOrdered = true;
					}					
				}
				_paszoDbContext.SaveChanges();
			}

			return Ok();

		}

		private void UpdateTodayOdrerParametersObject(OrderParameters data, OrderParametersDto inputData)
		{
			data.Status = inputData.Status;
			data.OrderTimeMinutes = inputData.OrderTime;
			//data.TodayRestaurant = inputData.TodayRestaurant;
			data.DeliveryTimeMinutes = inputData.DeliveryTime;
			if (inputData.Restaurant != null) {
				Restaurant todayRestaurant = _paszoDbContext.Restaurants.FirstOrDefault(x => x.Id == inputData.Restaurant.Id);
				if (todayRestaurant != null) {
					data.Restaurant = todayRestaurant;

				}
			}

			//data.Url = _paszoDbContext.Restaurants.FirstOrDefault(r => r.Name == data.TodayRestaurant).Url;
		}

		private void SetDefaultsToOrder(OrderParameters orderParameters)
		{
			//var today = DateTime.Now.Date;
			//orderParameters.TodayRestaurant = _configuration.GetSection("PaszoSettings:DefaultRestaurant").Value;

			Restaurant todayRestaurant = _paszoDbContext.Restaurants.FirstOrDefault(r => r.Name == _configuration.GetSection("PaszoSettings:DefaultRestaurant").Value);
			if (todayRestaurant != null) {
				
				//orderParameters.Url = todayRestaurant.Url;
				orderParameters.Restaurant = new Restaurant() { Id = todayRestaurant.Id};
				//_paszoDbContext.Restaurants.Attach(orderParameters.Restaurant);
			}

			orderParameters.OrderTimeMinutes = TimeStringToTimeAmmount(_configuration.GetSection("PaszoSettings:DefaultOrderTime").Value);
			orderParameters.DeliveryTimeMinutes = TimeStringToTimeAmmount(_configuration.GetSection("PaszoSettings:DefaultDeliveryTime").Value);

		}

		private int TimeStringToTimeAmmount(string timeString)
		{
			string[] timeArray = timeString.Split(":");

			return Convert.ToInt32(timeArray[0]) * 60 + Convert.ToInt32(timeArray[1]);  
		}

		#endregion

		#region CashBoxStatus

		[HttpPost]
		public ActionResult PostCashBoxState([FromBody] CashBoxStateDto cashBox)
		{
			var login = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
			var paszoUser = _paszoDbContext.PaszoUsers.FirstOrDefault(x => x.UserName == login && x.RetiredDate == null);
			if (paszoUser == null) 
			{
				return BadRequest();
			}

			var actualCashBoxState = GetDataActualCashBoxState();
			var totalChangeFromDatabase = OrderService.GetTotalChange(_paszoDbContext);

			var newCashBox = new CashBoxState(totalChangeFromDatabase + cashBox.ExtraCash);
			newCashBox.CashDatabaseState = totalChangeFromDatabase;
			newCashBox.ExtraCash = cashBox.ExtraCash;
			newCashBox.Comment = cashBox.Comment;
			newCashBox.PaszoUser = paszoUser;

			if (!IsCashBoxStateIdentical(actualCashBoxState, newCashBox)) {
				_paszoDbContext.CashBoxStates.Add(newCashBox);
				_paszoDbContext.SaveChanges();
			}

			actualCashBoxState = GetDataActualCashBoxState();

			CashBoxStateDto dto = new CashBoxStateDto();
			dto.LoadFromEntity(actualCashBoxState);

			return Json(dto);
		}

		[HttpGet]
		public ActionResult GetActualCashBoxState()
		{
			var actualCashBoxState = GetDataActualCashBoxState();

			CashBoxStateDto dto = new CashBoxStateDto();
			dto.LoadFromEntity(actualCashBoxState);

			dto.CashState = OrderService.GetTotalChange(_paszoDbContext) + dto.ExtraCash;

			return Json(dto);
		}

		[HttpGet]
		public ActionResult GetCashBoxStateHistory()
		{
			List<CashBoxStateDto> dtos = new List<CashBoxStateDto>(); 
			var cashBoxStates = _paszoDbContext.CashBoxStates
				.OrderBy(x => x.DateTime)
				.ToList();

			//int count = 0;
			decimal cashStateBuffor = 0;
			foreach (var cashBoxState in cashBoxStates) {
				CashBoxStateDto dto = new CashBoxStateDto();
				dto.LoadFromEntity(cashBoxState);
				decimal change = cashBoxState.CashState - cashStateBuffor;
				dto.CashChange = change;//(float)(Math.Round(change, 2, MidpointRounding.AwayFromZero)); 
				dtos.Add(dto);
				cashStateBuffor = cashBoxState.CashState;
			}

			var dtosOrdered = dtos.OrderByDescending(x => x.DateTime);

			return Json(dtosOrdered);
		}

		private CashBoxState GetDataActualCashBoxState()
		{
			return _paszoDbContext.CashBoxStates
				.OrderByDescending(x => x.DateTime)
				.FirstOrDefault();
		}

		private bool IsCashBoxStateIdentical(CashBoxState oldState, CashBoxState newState)
		{
			if (oldState == null) {
				return false;
			}
			if (newState == null) {
				return false;
			}

		 	var isCashStateIdentical = oldState.CashState == newState.CashState;
			var isCommentIdentical = oldState.Comment == newState.Comment;

			//if (oldState.PaszoUser != newState.PaszoUser) {
			//	return true;
			//}
			//if (newState.DateTime - oldState.DateTime > new TimeSpan(0, 1, 0)) {
			//	return true;
			//}

			return isCashStateIdentical && isCommentIdentical;

		}

		#endregion


	}
}
