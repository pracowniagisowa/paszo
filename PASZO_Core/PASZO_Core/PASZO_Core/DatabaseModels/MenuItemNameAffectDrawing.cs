﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.DatabaseModels
{
	public class MenuItemNameAffectDrawing
	{
		[Key]
		public int Id { get; set; }
		public string NamePart { get; set; }
		public AffectType AffectType { get; set; }

	}

	public enum AffectType
	{
		NoAffect = 0,
		Reduce = 1,
		Increase = 2,
		

	}
}
