﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.DatabaseModels
{
	public class DrawingHistory
	{
		//[Key]
		public int DrawingHistoryId { get; set; }
		//[ForeignKey("UserId")]
		public string UserName { get; set; }
		public DateTime DrawingDate { get; set; }
		public DrawingType DrawingType { get; set; }
	}

	public enum DrawingType
	{
		Normal,
		LatePenalty

	}
}
