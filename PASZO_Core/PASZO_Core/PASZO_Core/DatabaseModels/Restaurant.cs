﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PASZO_Core.DatabaseModels
{
	public class Restaurant
	{
		[Key]
		public int Id { get; set; }
		public string Name { get; set; }		
		public virtual List<MenuItem> MenuItemsList { get; set; }
		public virtual List<DrawingHistory> DrawingHistory { get; set; }
		public decimal AddidtionalCost { get; set; } = 0;
		public string Url { get; set; } = String.Empty;
		public string TelephoneNumber { get; set; } = String.Empty;
		public bool IsDefault { get; set; }
		public bool Deleted { get; set; }

		public Restaurant()
		{
			MenuItemsList = new List<MenuItem>();
			DrawingHistory = new List<DrawingHistory>();
		}

		public DrawingHistory GetTodayDrawingHistory(PaszoDbContext paszoDbContext)
		{
			var today = DateTime.UtcNow.Date;
			var dbRestaurant = paszoDbContext.Restaurants.FirstOrDefault(x => x.Id == Id);
			var todayDrawing = dbRestaurant.DrawingHistory.FirstOrDefault(x => x.DrawingDate.Date == today);
			
			return todayDrawing;
		}
	}
}