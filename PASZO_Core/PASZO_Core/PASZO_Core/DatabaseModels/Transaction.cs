﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PASZO_Core.DatabaseModels
{
	public class Transaction
	{
		private List<MenuItem> _menuItems;
		public int Id { get; set; }
		public decimal Ammount { get; set; }

		[StringLength(255)]
		public string Comment { get; set; }
		public DateTime Date {get; set;}

		public virtual Restaurant? Restaurant { get; set; }
		public string TransactionAddedBy { get; set; }
		public virtual PaszoUser PaszoUser { get; set; }
		public virtual List<MenuItem> MenuItems
		{
			get => _menuItems; 
			set {
				_menuItems = value;
				if (_menuItems != null)
					SetAmmount();
			}
		}

		public Transaction(){
			Date = DateTime.UtcNow;
		}

		public void SetAmmount(){
			Ammount = MenuItems.Select(i => (i.ItemPrice + i.BoxPrice) * -1).Sum();
		}
	}
}