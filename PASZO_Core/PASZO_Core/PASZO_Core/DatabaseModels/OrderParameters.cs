﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PASZO_Core.DatabaseModels
{
	public class OrderParameters
	{
		
		//private string _todayRestaurant;

		public string TodayRestaurant { get; set; }


		[Key]
		public int Id { get; set; }
		[StringLength(70)]
		public string Status { get; set; }
		public DateTime OrderTime { get; set; }
		public int OrderTimeMinutes { get; set; }
		public DateTime DeliveryTime { get; set; }
		public int DeliveryTimeMinutes { get; set; }
		public string Url { get; set; }
		public DateTime Date { get; set; }
		public virtual Restaurant Restaurant { get; set; }

		public OrderParameters()
		{
			Date = DateTime.UtcNow.Date;
		}

		public void ClearData()
		{
			TodayRestaurant = null;
			Status = null;
			OrderTimeMinutes = 0;
			DeliveryTimeMinutes = 0;
		}
	}
}