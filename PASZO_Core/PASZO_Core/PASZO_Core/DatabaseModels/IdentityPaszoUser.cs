﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PASZO_Core.DatabaseModels
{
	public class IdentityPaszoUser : IdentityUser
	{
		public virtual PaszoUser PaszoUser { get; set; }
	}
}
