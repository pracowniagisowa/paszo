﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PASZO_Core.DatabaseModels
{
	public class PaszoUser
	{
		[Key]
		public int UserId { get; set; }
		[Required]
		[StringLength(70)]
		public string UserName { get; set; }
		public bool IsAdmin { get; set; }
		public string Name { get; set; }
		public string SecondName { get; set; }
		public string Password { get; set; }
		public DateTime? RetiredDate { get; set; }
		public virtual ICollection<Transaction> Transactions { get; set; }
		public virtual PaszoUserGroup UserGroup { get; set; }

		public PaszoUser()
		{
			Transactions = new List<Transaction>();
			UserName = "";
			Password = "";
			Name = "";
			SecondName = "";
		}

	}
}