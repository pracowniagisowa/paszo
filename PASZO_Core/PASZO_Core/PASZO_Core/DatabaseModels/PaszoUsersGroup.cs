﻿using System.ComponentModel.DataAnnotations;

namespace PASZO_Core.DatabaseModels
{
	public class PaszoUserGroup
	{
		[Key]
		public int GroupId { get; set; }
		public string GroupName { get; set; }
		public string LocationAdress { get; set; }
		public bool IsDefault { get; set; }

		public PaszoUserGroup(){
			LocationAdress = String.Empty;
		}
	}
}
