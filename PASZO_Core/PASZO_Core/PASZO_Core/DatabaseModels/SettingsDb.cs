﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.DatabaseModels
{
	public class SettingsDb
	{
		public int Id { get; set; }
		public string SettingKey { get; set; }
		public string StringValue { get; set; }
		public int IntValue { get; set; }
		public bool BoolValue { get; set; }
		public float FloatValue { get; set; }
		public double DoubleValue { get; set; }
		public decimal DecimalValue { get; set; }
	}
}
