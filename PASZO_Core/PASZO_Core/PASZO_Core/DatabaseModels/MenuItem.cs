﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO_Core.DatabaseModels
{
	public class MenuItem
	{

		public int Id { get; set; }
		
		public decimal BoxPrice { get; set; }

		public string ItemName { get; set; }

		public decimal ItemPrice { get; set; }

		public bool IsOrdered { get; set; }

	}
}