﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.DatabaseModels
{
	public class PenaltyDrawing
	{
		public int Id { get; set; }
		public int PaszoUserId { get; set; }
		public string UserName { get; set; }
		public DateTime InsertDateTime { get; set; }
		public bool IsPenaltyValid { get; set; }
		public DateTime CancelDateTime { get; set; }

		public PenaltyDrawing(int paszoUserId, string userName)
		{
			PaszoUserId = paszoUserId;
			UserName = userName;
			InsertDateTime = DateTime.UtcNow;
			IsPenaltyValid = true;
		}

		public void CancelPenalty()
		{
			IsPenaltyValid = false;
			CancelDateTime = DateTime.UtcNow;
		}
	}
}
