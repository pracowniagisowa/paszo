﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.DatabaseModels
{
	public class OrderToSent
	{
		public int Id { get; set; }
		public string OrderName { get; set; }
	}
}
