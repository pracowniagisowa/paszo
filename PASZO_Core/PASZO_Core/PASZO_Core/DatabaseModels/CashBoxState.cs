﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PASZO_Core.DatabaseModels
{
	public class CashBoxState
	{
		[Key]
		public int CashBoxStateId { get; set; }
		public decimal CashState { get; set; }
		public decimal CashDatabaseState { get; set; }
		public decimal ExtraCash { get; set; }
		public DateTime DateTime { get; set; }
		public string Comment { get; set; }
		public virtual PaszoUser PaszoUser { get; set; }

		public CashBoxState(decimal cashState)
		{
			CashState = cashState;
			DateTime = DateTime.UtcNow;
		}
	}
}
