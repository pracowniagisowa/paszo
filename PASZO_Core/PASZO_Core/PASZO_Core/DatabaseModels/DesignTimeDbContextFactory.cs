﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace PASZO_Core.DatabaseModels
{
	public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<PaszoDbContext>
	{

		public PaszoDbContext CreateDbContext(string[] args)
		{
			IConfigurationRoot configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json")
				.Build();

			var builder = new DbContextOptionsBuilder<PaszoDbContext>();
			var connectionString = "Data Source=PaszoDataBase.db";
			var connection = @"Server=(localdb)\mssqllocaldb;Database=PaszoDataBaseMF.AspNetCore;Trusted_Connection=True;ConnectRetryCount=0";

			builder.UseSqlite(connection);
			return new PaszoDbContext(configuration);

		}
	}
}
