﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace PASZO_Core.DatabaseModels
{
	public class PaszoDbContext : IdentityDbContext<IdentityPaszoUser>
		{
			private IConfiguration _configuration;

			public PaszoDbContext(IConfiguration configuration)
			{
				_configuration = configuration;
			}

			protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.UseLazyLoadingProxies()
			.UseSqlite(_configuration.GetConnectionString("PaszoSQLServerConnection"));

			protected override void OnModelCreating(ModelBuilder builder)
			{
				base.OnModelCreating(builder);
			}

			public DbSet<MenuItem> MenuItems { get; set; }
			public DbSet<OrderParameters> OrderParameters { get; set; }
			public DbSet<PaszoUser> PaszoUsers { get; set; }
			public DbSet<Restaurant> Restaurants { get; set; }
			public DbSet<Transaction> Transactions { get; set; }
			public DbSet<PaszoUserGroup> PaszoGroups { get; set; }
			public DbSet<SettingsDb> PaszoSettings { get; set; }
			public DbSet<DrawingHistory> DrawingHistory { get; set; }
			public DbSet<DrawingImmunity> DrawingImmunities { get; set; }
			public DbSet<MenuItemNameAffectDrawing> MenuItemNameAffectDrawings { get; set; }
			public DbSet<OrderToSent> OrdersToSent { get; set; }
			public DbSet<CashBoxState> CashBoxStates { get; set; }
			public DbSet<PenaltyDrawing> PenaltyDrawings { get; set; }
		}
}