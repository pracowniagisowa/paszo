(function(){
  angular.module('PaszoApp')
  .controller('ModalController', ModalController);

  ModalController.$inject = ['$scope', '$uibModalInstance', '$rootScope', '$location', '$http', 'HttpGetService', 'editId', 'updateOrders' ]
  function ModalController($scope, $uibModalInstance, $rootScope, $location, $http, HttpGetService, editId, updateOrders){
    var $modal = this;

    // console.log("current Id: " + editId)
    // console.log('zmienna z modala: ' + updateOrders);

    var transactionUrl = $rootScope.configData.host + '/api/paszo/addtransaction?currentUserName=' + $rootScope.login

    $scope.boxPrice = 0;

    // $http.get($rootScope.configData.host + '/api/paszo/change?currentUserName=' + $rootScope.login)
    $http({
      method: "GET",
      url: $rootScope.configData.host + '/api/paszo/change?currentUserName=' + $rootScope.login,
      withCredentials: true
    })
    .then(function(data){
      $scope.saldo = data.data;
      $scope.debt = data.data * -1;

    }, function(error){
      console.log(error)
    });

    if (editId != undefined) {
      // $http.get($rootScope.configData.host + '/api/paszo/gettransaction?id=' + editId)
      $http({
        method: "GET",
        url: $rootScope.configData.host + '/api/paszo/gettransaction?id=' + editId,
        withCredentials: true
      })
      .then(function(data){
        // $scope.currentOrder = data;
        // console.log(data);

        $scope.itemNameEdit = data.data.MenuItem.ItemName;
        $scope.itemPriceEdit = data.data.MenuItem.ItemPrice;
        $scope.boxPriceEdit = data.data.MenuItem.BoxPrice;
        $scope.commentEdit = data.data.Comment;
        // console.log($scope.itemName +"; "+ $scope.itemPrice +"; "+ $scope.boxPrice )

    }, function(error){
      console.log(error)
    });
    }    

    $scope.countAll = function () {
      // console.log("Count All function")
      $scope.totalPrice = $scope.itemPrice + $scope.boxPrice;
      var toPayValue = $scope.totalPrice - $scope.saldo;
      if(toPayValue < 0){
          $scope.toPay = 0;
        } else {
          $scope.toPay = toPayValue;
        }
    };

    $scope.countAllEdit = function () {
      // console.log("Count All Edit function")
      $scope.totalPrice = $scope.itemPriceEdit + $scope.boxPriceEdit;
      var toPayValue = $scope.totalPrice - $scope.saldo;
      if(toPayValue < 0){
          $scope.toPay = 0;
        } else {
          $scope.toPay = toPayValue;
        }
    };


    $modal.orderOk = function(){

      var orderJson = {
        "MenuItem": {
          "ItemName": $scope.itemName,
          "ItemPrice": $scope.itemPrice,
          "BoxPrice": $scope.boxPrice
                    },
        "Comment": $scope.comment
      };

      // console.log("OK:");
      // console.log(transactionUrl);
      // console.log(orderJson);

      // $http.put(transactionUrl, orderJson)
      $http({
        method: "PUT",
        data: orderJson,
        url: transactionUrl,
        withCredentials: true
      })
      .then(function(){
        updateOrders()
      }, function(error){console.log(error);} );

      
      $uibModalInstance.dismiss('cancel');
    };

    $modal.payOk = function(){
      // console.log($scope.income);

      var payJson = {
        "Ammount": $scope.income
      };
    // $http.put(transactionUrl, payJson)
    $http({
        method: "PUT",
        data: payJson,
        url: transactionUrl,
        withCredentials: true
      })
    .then(function(){
        updateOrders()
      }, function(error){console.log(error);} );

    $uibModalInstance.dismiss('cancel');
    };

    $modal.cancelModal = function () {
      $uibModalInstance.dismiss('cancel');
    };

    $modal.saveEdit = function(){
      console.log("edytuje...")
      
      var editUrl = $rootScope.configData.host + '/api/paszo/orderedit?id=' + editId;

      var editJson = {
          "MenuItem": {
            "ItemName": $scope.itemNameEdit,
            "ItemPrice": $scope.itemPriceEdit,
            "BoxPrice": $scope.boxPriceEdit
          },
          "Comment": $scope.commentEdit,
        }
      // console.log(editJson)

      // $http.put(editUrl, editJson)
      $http({
        method: "PUT",
        data: editJson,
        url: editUrl,
        withCredentials: true
      })
      .then(function(){
        updateOrders()
      }, function(error){console.log(error);} );

      $uibModalInstance.dismiss('cancel');
    };
  };
})();
