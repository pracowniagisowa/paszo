(function (){

angular.module('PaszoApp')
.controller('UserPanelController', UserPanelController);

UserPanelController.$inject = ['$scope', '$rootScope', '$http', '$cookies', 'AlertService', '$location']
function UserPanelController($scope, $rootScope, $http, $cookies, AlertService, $location){
    var $userPanel = this;
    console.log("UserPanelController")

    $rootScope.login = $cookies.get('login');

    $userPanel.goToOrder = function(){
        $location.path('/order');
    };

    $userPanel.getChange = function(){
        $http({
            method: "GET",
            url: $rootScope.configData.host + '/api/paszo/change?currentUserName=' + $rootScope.login,
            withCredentials: true
        })
        .then(function(data){
        $scope.saldo = data.data;
        $scope.saldoAbsoute = Math.abs(data.data);

        // console.log($scope.credit);
        }, function(error){
        console.log(error)
        });
    };

    $userPanel.passwordChange = function(){
        // TODO: generalna poprawka funkcji
        var loginJson = {
          "UserName": $rootScope.login,
          "Password": $scope.oldPassword
              };
        // $http.post($rootScope.configData.host + '/api/paszo/checkloginandpass', loginJson)
        $http({
            method: "POST",
            url: $rootScope.configData.host + '/api/paszo/authorize',
            data: loginJson,
            withCredentials: true
        })
      .then(function(data){
        //   console.log(data)
          var checkLogin = data.data.Success
          if (checkLogin == true){
            console.log('haslo prawidłowe')
            if ($scope.newPassword == $scope.newPasswordRepeat) {
                console.log('hasła się zgadzają')
                var setPassJson = {
                    "UserName": $rootScope.login,
                    "Password": $scope.newPassword
                    }
                // $http.post($rootScope.configData.host + '/api/paszo/setpassword', setPassJson)
                $http({
                    method: "POST",
                    url: $rootScope.configData.host + '/api/paszo/setpassword',
                    data: setPassJson,
                    withCredentials: true
                })
                .then(function(){
                    AlertService.openModal('Hasło zostało zmienione')
                })
            } else {
                AlertService.openModal('Uwaga','Nowo podane hasła się nie zgadzają')
            }

          } else {
              AlertService.openModal('Uwaga','hasło nieprawidłowe!')
          }
      });
    };

    $userPanel.getUserHistory = function(){
        $http({
            method: "GET",
            url: $rootScope.configData.host + '/api/paszo/getuserhistory?userLogin=' + $rootScope.login,
            withCredentials: true
          })
          .then(function(data){
              console.log(data.data)
            $scope.userHistory = data.data;
            }, function(error){
            console.log(error)
            });
      };
    
    $userPanel.getUserData = function(){
        $http({
            method: "GET",
            url: $rootScope.configData.host + '/api/paszo/getuserbylogin?login=' + $rootScope.login,
            withCredentials: true
        })
        .then(function(data){
            console.log(data.data);
            var userData = data.data;
            $scope.name = userData.Name;
            $scope.email = userData.Email;
            if (userData.TelephoneNumber != 0) {
                $scope.telephoneNumber = userData.TelephoneNumber;
            }

        }, function(error){
            console.log(error)
        });
      };

    $userPanel.editUserData = function(){
        var userData = {
            "UserName": $rootScope.login,
            "Name": $scope.name,
            "Email": $scope.email,
            "TelephoneNumber": $scope.telephoneNumber
        };
        $http({
            method: "POST",
            url: $rootScope.configData.host + '/api/paszo/edituserdata',//?user=' + userData,
            data: userData,
            withCredentials: true 
        }).then(function(data){
            AlertService.openModal('Dane użytkownika zostały zmienione')
        });
      };

    $userPanel.getChange();
    $userPanel.getUserHistory();
    $userPanel.getUserData();


};
})();