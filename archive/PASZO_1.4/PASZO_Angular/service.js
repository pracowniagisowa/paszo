// service factory - zwraca funkcje
(function(){
   'use strict';

  angular
  .module('PaszoApp')
  .factory('httpService', function($q, $http){
    function httpGet(url){
      var deferred = $q.defer();
      $http.get(url).
        then(function(response){
            deferred.resolve(response.data);
        });
        return deferred.promise;
    }
    function httpPost(){
      console.log('pusty POST');
    }
    return{
      httpGet:httpGet,
      httpPost:httpPost
    }
  });

})();
