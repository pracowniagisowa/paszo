(function () { //IIFE function
'use strict'

angular.module('PaszoApp', ['ngRoute', 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'ngCookies']) //dependencies
.config(function($routeProvider){
  $routeProvider
  .when('/',{
    templateUrl: 'login.html'
  })
  .when('/order', {
    templateUrl: 'order.html'
  })
  .when('/admin',{
    templateUrl: 'admin.html'
  })
  .when('/userPanel',{
    templateUrl: 'userPanel.html'
  })

  .otherwise({
    redirectTo: '/'
  }); 
}, function($httpProvider){
  $httpProvider.defaults.withCredentials = true;

})
.run(function run($rootScope){
  $rootScope.configData = configData;
  // console.log($rootScope.configData)
});

})();
