(function (){
'use strict'

angular.module('PaszoApp')
.controller('OrderController', OrderController);

OrderController.$inject = ['$q','$scope', '$uibModal', 'HttpGetService', '$rootScope', '$location', '$cookies', '$http', 'InfoService']
function OrderController($q, $scope, $uibModal, HttpGetService, $rootScope, $location, $cookies, $http, InfoService){
  var $order = this;

  $scope.visiblePrices = false;
  $scope.showHidePricesLabel = "Pokaż ceny";

  $order.updateOrdersList = function(){
    $http.get($rootScope.configData.host + '/api/paszo/gettodayorder')
    .then(function(data){
      $scope.todayOrdersList = data.data;
      console.log("updateOrdersList")
      console.log($scope.todayOrdersList);
    }, function(error){
      console.log(error)
    });
  };

  $order.getTodaySum = function(){
    console.log("getTodaySum")
    $http.get($rootScope.configData.host + '/api/paszo/gettodaysum')
    .then(function(data){
      $scope.todaySum = data.data;
    }, function(error){
      console.log(error)
    });
  };
  // console.log('Login from cookie: ' + $cookies.get('login'));
  $rootScope.login = $cookies.get('login');
  // console.log('Login from $rootscope: ' + $rootScope.login);
  if (typeof $rootScope.login == 'undefined'){
    console.log("powrot do logowania");
    $location.path('/')
  };

  $http({
        method: 'GET',
        url: $rootScope.configData.host + '/api/paszo/getuserbylogin?login=' + $rootScope.login,
        withCredentials: true
      })
  .then(function(data){
    $rootScope.isAdmin = data.data.IsAdmin;
    $rootScope.name = data.data.Name;

    if ($rootScope.name == null) {
      $rootScope.displayName = $rootScope.login;
    }
    else{
      $rootScope.displayName = $rootScope.name;
    };
  }, function(error){
      console.log(error)
    });

  $order.getChange = function(){
      // $http.get($rootScope.configData.host + '/api/paszo/change?currentUserName=' + $rootScope.login)
      $http({
        method: "GET",
        url: $rootScope.configData.host + '/api/paszo/change?currentUserName=' + $rootScope.login,
        withCredentials: true
      })
      .then(function(data){
        $scope.saldo = data.data;

        // console.log($scope.credit);
        }, function(error){
        console.log(error)
        });
  };

  $order.infoPanel = function(){
    InfoService.openModal();
  };

  $order.getOrderParameters = function(){
    $http({
        method: 'GET',
        url: $rootScope.configData.host + '/api/paszo/getorderparameters',
        withCredentials: true
      })
      .then(function(data){
        $scope.orderParameters = data.data;
        // console.log($scope.orderParameters)
        if (data.data.Status == 'Zamówione' || data.data.Status == 'Doręczone') {
          $scope.orderTimeVisible = false
        } else {$scope.orderTimeVisible = true}

        var orderDateTimeArray = data.data.OrderTime.split("T");
        // console.log(orderDateTimeArray)
        var orderTimeArray = orderDateTimeArray[1].split(":");
        // console.log(orderTimeArray)
        $scope.orderTimeObject = new Date();
        $scope.orderTimeObject.setHours(orderTimeArray[0]);
        $scope.orderTimeObject.setMinutes(orderTimeArray[1]);
        
        var deliveryDateTimeArray = data.data.DeliveryTime.split("T");
        var deliveryTimeArray = deliveryDateTimeArray[1].split(":");
        if (data.data.DeliveryTime == "2017-01-01T00:00:00") {
          // console.log(data.data.DeliveryTime)
          $scope.deliveryTimeVisibility = false;
        } else {$scope.deliveryTimeVisibility = true}
        $scope.deliveryTimeObject = new Date();
        $scope.deliveryTimeObject.setHours(deliveryTimeArray[0]);
        $scope.deliveryTimeObject.setMinutes(deliveryTimeArray[1]);
                

      }, function(error){
      console.log(error)
    });
  };

  $order.getOrderParameters();
  $order.getChange();
  $order.updateOrdersList();
  $order.getTodaySum();

  $order.animationsEnabled = true;

  $order.openOrder = function (){
    var modalInstance = $uibModal.open({
      animation: $order.animationsEnabled,
      templateUrl: 'orderModal.html',
      controller: 'ModalController',
      controllerAs: '$modal',
      backdrop: 'static',
      resolve:{
        editId: function(){
          return undefined;
        },
        updateOrders: function () {
          return function(){$order.refreshAll()}; 
        }
      }
    });
  };

  $order.openPay = function (){
    var modalInstance = $uibModal.open({
      animation: $order.animationsEnabled,
      templateUrl: 'payModal.html',
      controller: 'ModalController',
      controllerAs: '$modal',
      backdrop: 'static',
      resolve:{
        editId: function(){
          return undefined;
        },
        updateOrders: function () {
          return function(){$order.refreshAll()}; 
        }
      }
    });
  };

  $order.editOrder = function (currentId){
    // console.log("order id: " + currentId)
    var modalInstance = $uibModal.open({
      animation: $order.animationsEnabled,
      templateUrl: 'editModal.html',
      controller: 'ModalController',
      controllerAs: '$modal',
      backdrop: 'static',
      resolve:{
        editId: function(){
          return currentId;
        },
        updateOrders: function () {
          return function(){$order.refreshAll()}; 
        }
      }
    });
  };

  $order.delOrder = function (currentId){
    var url = $rootScope.configData.host + '/api/paszo/deltransaction?id=' + currentId;
    // $http.delete(url)
    $http({
      method: "DELETE",
      url: $rootScope.configData.host + '/api/paszo/deltransaction?id=' + currentId,
      withCredentials: true
    })
    .then(function(){
        $order.refreshAll();
      }, function(error){console.log(error);} );
    

  };

  $order.goToAdmin = function () {
    console.log("go to admin");
      // console.log($rootScope.login);
    $location.path('/admin');
  };

  $order.goToUserPanel = function(){
    $location.path('/userPanel');
  };

  $order.refreshAll = function(){
    $order.updateOrdersList();
    $order.getTodaySum();
    $order.getChange();
    $order.getOrderParameters();
  };

  $order.logout = function(){
    $cookies.put('login', undefined, {'path': '/'});
    $cookies.put('paszoToken', undefined, {'path': '/'});    
    $location.path('/');

  };

  $order.swichVisiblePrices = function(){
    $scope.visiblePrices = !$scope.visiblePrices;
    if ($scope.visiblePrices) {
      $scope.showHidePricesLabel = 'Ukryj ceny'
    }
    else{
       $scope.showHidePricesLabel = 'Pokaż ceny'
    }
  };

  $order.goToLink = function(){

  };

};



})();
