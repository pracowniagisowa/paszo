(function(){
  angular.module('PaszoApp')
  .controller('LoginController', LoginController);

  LoginController.$inject = ['$q', '$scope', '$rootScope', '$location', '$http', 'HttpGetService', '$cookies', '$uibModal', 'AlertService', 'InfoService']
  function LoginController($q, $scope, $rootScope, $location, $http, HttpGetService, $cookies, $uibModal, AlertService, InfoService){
    var $login = this;
    console.log('login controller');


    $scope.loading = false;    
    console.log($cookies.get('login')) 
    
    if ($cookies.get('login') != undefined) {
      $rootScope.login = $cookies.get('login');
      console.log('Login from $rootscope: ' + $rootScope.login);
      $location.path('/order')
    }   

    $login.loginButton = function(){  

      $scope.loading = true;

      var loginJson = {
          "UserName": $scope.username.toLowerCase(),
          "Password": $scope.password
              }
      $http({
        method: 'POST',
        url: $rootScope.configData.host + '/api/paszo/authorize',
        data: loginJson,
        withCredentials: true
      })
      .then(function(data){
        var checkLogin = data.data.Success;
        var uName = $scope.username;
        // console.log(uName);
        $scope.loading = false; 

        if (checkLogin == true) {
          console.log('logowanie przeszlo...');
          $rootScope.login = uName;
          $location.path('/order');
        } else {
          // console.log(data.data.Message);
          AlertService.openModal(data.data.Message);
        };
      }, function(error){
        $scope.loading = false; 
        AlertService.openModal('status: ' + error.status ,error.statusText)
                 
        });      
      };

      $login.animationsEnabled = true;

      $login.newUser = function (){
        var modalInstance = $uibModal.open({
          animation: $login.animationsEnabled,
          templateUrl: 'newUserModal.html',
          controller: 'NewUserModalController',
          controllerAs: '$modal',
          size: 'sm',
          backdrop: 'static' 
        });
      };

      $login.infoPanel = function () {
        InfoService.openModal();
      };

      $login.passwordForgotten = function(){
        $http({
          method: 'GET',
          url: $rootScope.configData.host + '/api/paszo/getlistofadmins'
        })
        .then(function(data){
          AlertService.openModal('Twoje hasło może zrestartować administrator aplikacji', 'Lista administratorów to: ' + data.data);
        })
      }
    };

})();
