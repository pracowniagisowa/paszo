(function(){
  // 'use strict'
  angular.module('PaszoApp')
  .controller('AdminController', AdminController);

  AdminController.$inject = ['$q', '$scope', '$rootScope', '$location', '$http', 'HttpGetService', '$cookies', 'AlertService']
  function AdminController($q, $scope, $rootScope, $location, $http, HttpGetService, $cookies, AlertService){
    var $admin = this;
    $scope.ismeridian = false;
    $scope.hstep = 1;
    $scope.mstep = 5;

    $rootScope.login = $cookies.get('login');
    // console.log('Login from $rootscope: ' + $rootScope.login);
    if (typeof $rootScope.login == 'undefined'){
      console.log("powrot do logowania");
      $location.path('/')
    } 
    else 
    {
      $http({
        method: 'GET',
        url: $rootScope.configData.host + '/api/paszo/getuserbylogin?login=' + $rootScope.login,
        withCredentials: true
      })
      .then(function(data){
        $rootScope.isAdmin = data.data.IsAdmin;
        if ($rootScope.isAdmin == 'false'){
          // console.log('isAdmin == ' + $rootScope.isAdmin)
          $location.path('/order')
        }
      });
    };

    $admin.usersAdmin = function(){
      // console.log("loading = " + $scope.loadingUsersAdmin )
      $http({
        method: 'GET',
        url: $rootScope.configData.host + '/api/paszo/adminlist',
        withCredentials: true
      })
      .then(function(data){
        // console.log("loading = " + $scope.loadingUsersAdmin )
        $scope.adminList = data.data;
        // console.log($scope.adminList);
      }, function(error){
      console.log(error)
    });
  };
  
  $admin.getRestaurantsList = function(){
    $http({
        method: 'GET',
        url: $rootScope.configData.host + '/api/paszo/getrestourantslist',
        withCredentials: true
      })
      .then(function(data){
        $scope.restaurantsList = data.data;
        // console.log($scope.restaurantsList);
      }, function(error){
      console.log(error)
    });
  };

  $admin.getTotalSum = function(){
    $http({
      method: 'GET',
      url: $rootScope.configData.host + '/api/paszo/totalchange',
      withCredentials: true
    })
    .then(function(data){
      $scope.totalSum = data.data;
    }, function(error){
      console.log(error)
    })
  }

  $admin.getOrderParameters = function(){
    $http({
        method: 'GET',
        url: $rootScope.configData.host + '/api/paszo/getorderparameters',
        withCredentials: true
      })
      .then(function(data){
        $scope.orderParameters = data.data;

        var orderDateTimeArray = data.data.OrderTime.split("T");
        // console.log(orderDateTimeArray)
        var orderTimeArray = orderDateTimeArray[1].split(":");
        // console.log(orderTimeArray)
        $scope.orderTimeObject = new Date();
        $scope.orderTimeObject.setHours(orderTimeArray[0]);
        $scope.orderTimeObject.setMinutes(orderTimeArray[1]);
        
        var deliveryDateTimeArray = data.data.DeliveryTime.split("T");
        var deliveryTimeArray = deliveryDateTimeArray[1].split(":");
        if (data.data.DeliveryTime == "00:00:00") {
          // console.log(data.data.DeliveryTime)
          $scope.deliveryTimeVisibility = false;
        } else {$scope.deliveryTimeVisibility = true}
        $scope.deliveryTimeObject = new Date();
        $scope.deliveryTimeObject.setHours(deliveryTimeArray[0]);
        $scope.deliveryTimeObject.setMinutes(deliveryTimeArray[1]);

        

      }, function(error){
      console.log(error)
    });
  }; 

    // if ($rootScope.isAdmin != true || typeof $rootScope.login == 'undefined'){
    //   console.log("powrot do strony order");
    //   $location.path('/order')
    // };

    $admin.goToOrder = function(){
      $location.path('/order');
    };

    $admin.refreshAll = function(){
      $admin.getOrderParameters();
      $admin.usersAdmin();
      $admin.getRestaurantsList();
      $admin.getTotalSum();
    };

    $admin.refreshAll();

    $admin.adminPay = function(user){
      var transactionUrl = $rootScope.configData.host + '/api/paszo/addtransaction?currentUserName=' + user.UserName;
      var payJson = {
        "Ammount": user.AdminPay
      };
      // $http.put(transactionUrl, payJson)
      $http({
        method: 'PUT',
        data: payJson,
        url: transactionUrl,
        withCredentials: true
      })
      .then (function(){
        $admin.refreshAll()
      }, function(error){console.log(error);} );

    };

    $admin.swichAdmin = function (user){
      
      var swichAdminUrl = $rootScope.configData.host + '/api/paszo/swichadmin?login=' + user.UserName;
      // $http.put(swichAdminUrl)
      $http({
        method: 'PUT',
        url: swichAdminUrl,
        withCredentials: true
      })
      .then (function(){
        $admin.refreshAll()
        console.log("swich admin - " + user.UserName)
      }, function(error){console.log(error);} );
    };

    $admin.resetPassword = function(user){
      var tempPass = 'paszo';
      var setPassJson = {
        "UserName": user.UserName,
        "Password": tempPass
      };
      // console.log(setPassJson)
      // $http.post($rootScope.configData.host + '/api/paszo/setpassword', setPassJson)
      $http({
        method: "POST",
        url: $rootScope.configData.host + '/api/paszo/setpassword',
        data: setPassJson,
        withCredentials: true       
      })
      .then (function(){
        var message = 'ustawiono hasło "' + tempPass + '" dla uzytkownika ' + user.UserName;
        AlertService.openModal('', message);
        console.log(message)
      }, function(error){console.log(error);} );
    };

    $admin.retireUser = function(user){
      var answer = confirm("Czy na pewno chcesz usunąć użytkownika?");
      console.log(answer);
      if (answer) {
        $http({
          method: "GET",
          url: $rootScope.configData.host + '/api/paszo/retireduser?userName=' + user.UserName,
          withCredentials: true
        })
        .then(function(){
          $admin.refreshAll();
          AlertService.openModal('Użytkownik z dniem dzisiajszym został usunięty z aplikacji',
          'jednak pozostał w bazie danych. Aby przywrócić użytkownika należy zmienić w bazie danych datę usunięcia na późniejszą lub null')
        });  
      }      
    };

    $admin.postRestaurant = function(){
      $http({
        method: "POST",
        url: $rootScope.configData.host + '/api/paszo/postrestourant',
        data: {
          "Name": $scope.restaurantName,
	        "Url": $scope.restaurantUrl
        },
        withCredentials: true
      })
      .then (function(){
        $admin.getRestaurantsList();
        $scope.restaurantName = undefined;
        $scope.restaurantUrl = undefined;
      }, function(error){
        console.log(error);
        // AlertService.openModal('', message);
      } );
    };

    $admin.delRestourant = function(currentId){
      console.log(currentId)
      $http({
        method: 'DELETE',
        url: $rootScope.configData.host + '/api/paszo/delrestourant?id=' + currentId,
        withCredentials: true
      })
      .then(function(){
        $admin.getRestaurantsList();
      }, function(error){
        console.log(error);
      })

    }

    $admin.postOrderParameters = function () {
      $http({
        method: "POST",
        url: $rootScope.configData.host + '/api/paszo/postorderparameters',
        data: $scope.orderParameters,
        withCredentials: true
      })
      .then (function(){
        console.log($scope.orderParameters);
        $admin.getOrderParameters();
      }, function(error){
        console.log($scope.orderParameters);
        console.log(error);
        // AlertService.openModal('', message);
      } );
    }

    $admin.setRestourantToScope = function(value){
      $scope.orderParameters.TodayRestaurant = value;
      // console.log(value);
    };

    $admin.setStatusToScope = function(value){
      $scope.orderParameters.Status = value;
      // console.log(value);
    };

    $admin.timeObjectToString = function(timeObject){
      if (timeObject.getHours() < 10) {
        // console.log(timeObject.getHours())
        var hoursString = "0" + timeObject.getHours();
      } else {
        var hoursString = timeObject.getHours();
      }
      if (timeObject.getMinutes() < 10) {
        var minutesString = "0" + timeObject.getMinutes();
      } else {
        var minutesString = timeObject.getMinutes();
      }
      var timeMessage = "2016-01-01T" + hoursString + ":" + minutesString + ":00.0000000+01:00";
      // console.log(timeMessage)
      return timeMessage;
    };

    $admin.orderTimeChanged = function(){
      // var orderTimeString = $scope.orderTimeObject.getHours() + ":" + $scope.orderTimeObject.getMinutes() + ":00";
      var orderTimeString = $admin.timeObjectToString($scope.orderTimeObject);      
      $scope.orderParameters.OrderTime = orderTimeString;
      // console.log($scope.orderParameters.OrderTime)
    };

    $admin.deliveryTimeChanged = function(){
      // var deliveryTimeString = $scope.deliveryTimeObject.getHours() + ":" + $scope.deliveryTimeObject.getMinutes() + ":00";
      var deliveryTimeString = $admin.timeObjectToString($scope.deliveryTimeObject);
      
      $scope.orderParameters.DeliveryTime = deliveryTimeString;
      console.log($scope.orderParameters.DeliveryTime)
    };

    $admin.clearOrderParameters = function(){
      $http({
        method: "GET",
        url: $rootScope.configData.host + '/api/paszo/clearorderparameters',
        withCredentials: true
      })
      .then (function(){
        $admin.getOrderParameters();
      }, function(error){console.log(error);} );
    }

  };
})();
