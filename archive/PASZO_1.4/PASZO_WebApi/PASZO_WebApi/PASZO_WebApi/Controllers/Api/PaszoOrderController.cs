﻿using PASZO.Services;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PASZO_Web_Api.Models;
using PASZO_WebApi.DTOs;
using PASZO_WebApi.Models;
using AutoMapper;

namespace PASZO_WebApi.Controllers.Api
{
    public class PaszoOrderController : ApiController
    {
        private PaszoDbContext _context;

        public PaszoOrderController()
        {
            _context = new PaszoDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [HttpGet]
        [Route("api/paszo/getorderparameters")]
        public OrderParametersDto GetOrderPrameters()
        {
            var today = DateTime.Now.Date;
            OrderParameters todayOrderParams = _context.OrdersParameters.FirstOrDefault(o => o.Date == today);
            if (todayOrderParams == null)
            {
                todayOrderParams = new OrderParameters()
                {
                    OrderTime = new DateTime(2017, 1, 1, 0, 0, 0),
                    DeliveryTime = new DateTime(2017, 1, 1, 0, 0, 0)
                };

            }
            OrderParametersDto orderParametersDto = Mapper.Map<OrderParameters, OrderParametersDto>(todayOrderParams);
            return orderParametersDto;
        }

        [AuthorizeAdmin]
        [HttpPost]
        [Route("api/paszo/postorderparameters")]
        public HttpResponseMessage PostOrderPrameters(OrderParameters orderParameters)
        {
            var today = DateTime.Now.Date;
            OrderParametersDto returnOrderParametersDto = new OrderParametersDto();


            OrderParameters todayOrderParameters = _context.OrdersParameters.FirstOrDefault(o => o.Date == today);
            if (todayOrderParameters != null)
            {
                todayOrderParameters.TodayRestaurant = orderParameters.TodayRestaurant;
                todayOrderParameters.Status = orderParameters.Status;
                todayOrderParameters.OrderTime = orderParameters.OrderTime;
                todayOrderParameters.DeliveryTime = orderParameters.DeliveryTime;
                _context.SaveChanges();
                returnOrderParametersDto = Mapper.Map<OrderParameters, OrderParametersDto>(todayOrderParameters);
            }
            else
            {
                OrderParameters newOrderParams = new OrderParameters();
                newOrderParams.TodayRestaurant = orderParameters.TodayRestaurant;
                newOrderParams.Status = orderParameters.Status;
                newOrderParams.OrderTime = orderParameters.OrderTime;
                newOrderParams.DeliveryTime = orderParameters.DeliveryTime;

                _context.OrdersParameters.Add(newOrderParams);
                _context.SaveChanges();
                returnOrderParametersDto = Mapper.Map<OrderParameters, OrderParametersDto>(newOrderParams);
            }

            return ActionContext.Request.CreateResponse(HttpStatusCode.OK, returnOrderParametersDto);
        }
    }
}