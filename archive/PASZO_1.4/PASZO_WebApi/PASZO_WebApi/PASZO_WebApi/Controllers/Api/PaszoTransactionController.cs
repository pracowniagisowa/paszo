﻿using PASZO.Models;
using PASZO.Services;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PASZO.DTOs;
using PASZO_Web_Api.Models;

namespace PASZO_WebApi.Controllers.Api
{
    public class PaszoTransactionController : ApiController
    {
        private PaszoDbContext _context;

        public PaszoTransactionController()
        {
            _context = new PaszoDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [AuthorizeService]
        [HttpPut]
        [Route("api/paszo/addtransaction")]
        public HttpResponseMessage PutUsersTransaction(string currentUserName, Transaction transaction)
        {
            AuthorizeAdminService authorizeAdminService = new AuthorizeAdminService();
            string cookieLogin;
            bool isAdmin;
            bool cookieCheck = authorizeAdminService.Check(Request, out cookieLogin, out isAdmin);

            if (isAdmin || cookieLogin == currentUserName)
            {
                var userInDb = _context.PaszoUsers.FirstOrDefault(u => u.UserName == currentUserName);
                if (userInDb == null)
                    throw new HttpResponseException(HttpStatusCode.NotAcceptable);
                userInDb.Transactions.Add(transaction);
                _context.SaveChanges();
                return ActionContext.Request.CreateResponse(HttpStatusCode.OK,
                    new ActionResponse() { Success = true, Message = "transaction added" });
            }
            else
            {
                return ActionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                    new ActionResponse() { Success = false, Message = "no admin acces" });
            }
        }

        [AuthorizeService]
        [HttpPut]
        [Route("api/paszo/orderedit")]
        public HttpResponseMessage OrderEdit(int id, Transaction transactionJson)
        {
            AuthorizeAdminService authorizeAdminService = new AuthorizeAdminService();
            var transactionInDb = _context.Transactions.SingleOrDefault(t => t.Id == id);
            string cookieLogin;
            bool isAdmin;
            bool cookieCheck = authorizeAdminService.Check(Request, out cookieLogin, out isAdmin);

            if (isAdmin || cookieLogin == transactionInDb.PaszoUser.UserName)
            {
                var bufor = transactionJson;
                transactionInDb.MenuItem = transactionJson.MenuItem;
                transactionInDb.Comment = transactionJson.Comment;
                transactionInDb.SetAmmount();
                _context.SaveChanges();
                return ActionContext.Request.CreateResponse(HttpStatusCode.OK,
                    new ActionResponse() { Success = true, Message = "transaction edited" });
            }
            else
            {
                return ActionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                    new ActionResponse() { Success = false, Message = "no admin acces" });
            }
        }

        [AuthorizeService]
        [HttpGet]
        [Route("api/paszo/gettransaction")]
        public TransactionDto GetTransaction(int id)
        {
            Transaction transaction = _context.Transactions.FirstOrDefault(t => t.Id == id);
            return new TransactionDto
            {
                Id = transaction.Id,
                UserName = transaction.PaszoUser.UserName,
                MenuItem = transaction.MenuItem,
                Comment = transaction.Comment
            };
        }

        [AuthorizeService]
        [HttpDelete]
        [Route("api/paszo/deltransaction")]
        public HttpResponseMessage DelTransaction(int id)
        {
            AuthorizeAdminService authorizeAdminService = new AuthorizeAdminService();
            Transaction currentTransaction = _context.Transactions.FirstOrDefault(t => t.Id == id);
            string cookieLogin;
            bool isAdmin;
            bool cookieCheck = authorizeAdminService.Check(Request, out cookieLogin, out isAdmin);

            if (isAdmin || cookieLogin == currentTransaction.PaszoUser.UserName)
            {
                _context.Transactions.Remove(currentTransaction);
                _context.SaveChanges();
                return ActionContext.Request.CreateResponse(HttpStatusCode.OK,
                    new ActionResponse() { Success = true, Message = "transaction deleted" });
            }
            else
            {
                return ActionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                    new ActionResponse() { Success = false, Message = "no admin acces" });
            }
        }
    }
}