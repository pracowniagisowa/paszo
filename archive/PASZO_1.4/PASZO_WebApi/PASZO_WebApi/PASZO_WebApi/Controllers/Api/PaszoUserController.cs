﻿using PASZO.Models;
using PASZO.Services;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Security;
using PASZO.DTOs;
using PASZO_Web_Api.Models;
using PASZO_WebApi.DTOs;

namespace PASZO_WebApi.Controllers.Api
{
    public class PaszoUserController : ApiController
    {
        private PaszoDbContext _context;

        public PaszoUserController()
        {
            _context = new PaszoDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [HttpPost]
        [Route("api/paszo/adduser")]
        public HttpResponseMessage CreateUser(PaszoUser newUserJson)
        {
            if (!ModelState.IsValid)
                return ActionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                        new ActionResponse() { Success = false, Message = "!ModelState.IsValid" });
            if (_context.PaszoUsers.Any(e => e.UserName == newUserJson.UserName))
                return ActionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                        new ActionResponse() { Success = false, Message = "User exists in database" });

            PaszoUser newUser = newUserJson;
            newUser.Password = SecurePasswordHasher.Hash(newUserJson.Password);

            _context.PaszoUsers.Add(newUser);

            _context.SaveChanges();

            UserDto newUserDto = new UserDto()
            {
                UserId = newUser.UserId,
                UserName = newUser.UserName,
                IsAdmin = newUser.IsAdmin,
                Name = newUser.Name
            };

            return ActionContext.Request.CreateResponse(HttpStatusCode.OK, newUserDto);

        }

        [HttpPost]
        [Route("api/paszo/authorize")]
        public HttpResponseMessage Authorize(PaszoUser user)
        {
            //TODO Przenieść czynności do serwisów i posprzątać kontroler
            string login = user.UserName;
            string passoword = user.Password;

            PaszoUser currentUser = _context.PaszoUsers.FirstOrDefault(u => u.UserName == login.ToLower());
            if (currentUser == null)
            {
                return ActionContext.Request.CreateResponse(HttpStatusCode.OK,
                        new ActionResponse() { Success = false, Message = "Login nie znaleziony w bazie danych. Sprawdź poprawność danych lub dodaj nowego użytkownika za pomocą przycisku w prawym górnym rogu ekranu" });
            }
            if (currentUser.RetiredDate < DateTime.Now)
            {
                return ActionContext.Request.CreateResponse(HttpStatusCode.OK,
                        new ActionResponse() { Success = false, Message = "Użytkownik został usunięty z bazy danych" });
            }

            //if (true )
            //{
            if (passoword != null)
            {
                bool isPassValid = SecurePasswordHasher.Verify(passoword, currentUser.Password);
                if (isPassValid)
                {
                    string paszoToken = SecurePasswordHasher.Hash(login);

                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                        1,
                        login,
                        DateTime.Now,
                        DateTime.Now.AddDays(15),
                        false,
                        paszoToken,
                        "/");

                    string encTicket = FormsAuthentication.Encrypt(authTicket);

                    var cookie = new CookieHeaderValue("paszoToken", encTicket);
                    var cookie2 = new CookieHeaderValue("login", login.ToLower());
                    cookie.Path = "/";
                    cookie2.Path = "/";
                    //cookie.Expires = DateTime.Now.AddDays(15);
                    var response = ActionContext.Request.CreateResponse(HttpStatusCode.OK,
                        new ActionResponse() { Success = true, Message = "Authorize succes" });
                    response.Headers.AddCookies(new CookieHeaderValue[] { cookie, cookie2 });
                    return response;
                }
                else
                {
                    return ActionContext.Request.CreateResponse(HttpStatusCode.OK,
                new ActionResponse() { Success = false, Message = "wrong password" });
                }
            }
            else
            {
                return ActionContext.Request.CreateResponse(HttpStatusCode.OK,
                    new ActionResponse() { Success = false, Message = "wrong password" });
            }
            //}
            //else
            //{
            //    return ActionContext.Request.CreateResponse(HttpStatusCode.OK,
            //            new ActionResponse() { Success = false, Message = "Login nie znaleziony w bazie danych. Sprawdź poprawność danych lub dodaj nowego użytkownika za pomocą przycisku w prawym górnym rogu ekranu" });
            //}
        }

        [AuthorizeService]
        [HttpPost]
        [Route("api/paszo/setpassword")]
        public HttpResponseMessage SetPassword(PaszoUser user)
        {
            AuthorizeAdminService authorizeAdminService = new AuthorizeAdminService();
            bool isAdmin;
            string cookieLogin;

            string login = user.UserName;
            string password = user.Password;

            bool isAuthorized = authorizeAdminService.Check(Request, out cookieLogin, out isAdmin);

            if (isAdmin || login == cookieLogin)
            {
                PaszoUser currentUser = _context.PaszoUsers.FirstOrDefault(u => u.UserName == login.ToLower());
                currentUser.Password = SecurePasswordHasher.Hash(password);
                _context.SaveChanges();
                return ActionContext.Request.CreateResponse(HttpStatusCode.OK,
                    new ActionResponse() { Success = true, Message = "password set" });
            }
            else
            {
                return ActionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                    new ActionResponse() { Success = false, Message = "unauthorized acces" });
            }
        }

        [AuthorizeService]
        [HttpPost]
        [Route("api/paszo/edituserdata")]
        public HttpResponseMessage EditUserData(PaszoUser user)
        {
            AuthorizeAdminService authorizeAdminService = new AuthorizeAdminService();
            bool isAdmin;
            string cookieLogin;

            bool isAuthorized = authorizeAdminService.Check(Request, out cookieLogin, out isAdmin);

            if (isAdmin || user.UserName == cookieLogin)
            {
                PaszoUser currentUser = _context.PaszoUsers.FirstOrDefault(u => u.UserName == user.UserName.ToLower());
                currentUser.Email = user.Email;
                currentUser.TelephoneNumber = user.TelephoneNumber;
                _context.SaveChanges();

                return ActionContext.Request.CreateResponse(HttpStatusCode.OK,
                    new ActionResponse() { Success = true, Message = "user data edited" });
            }
            else
            {
                return ActionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                    new ActionResponse() { Success = false, Message = "unauthorized acces" });
            }
        }

        [AuthorizeService]
        [HttpGet]
        [Route("api/paszo/change")]
        public HttpResponseMessage GetUserChange(string currentUserName)
        {
            AuthorizeAdminService authorizeAdminService = new AuthorizeAdminService();
            string cookieLogin;
            bool isAdmin;
            bool cookieCheck = authorizeAdminService.Check(Request, out cookieLogin, out isAdmin);

            if (isAdmin || cookieLogin == currentUserName)
            {
                float change = 0;
                var userInDb = _context.PaszoUsers.FirstOrDefault(u => u.UserName == currentUserName);
                change = userInDb.Transactions.Sum(a => a.Ammount);

                //return  change;
                return ActionContext.Request.CreateResponse(HttpStatusCode.OK, Math.Round(change, 2));
            }
            else
            {
                return ActionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                    new ActionResponse() { Success = false, Message = "no admin acces" });
            }
        }

        [AuthorizeService]
        [HttpGet]
        [Route("api/paszo/getuserbylogin")]
        public HttpResponseMessage GetUserByLogin(string login)
        {
            AuthorizeAdminService authorizeAdminService = new AuthorizeAdminService();
            string cookieLogin;
            bool isAdmin;

            bool cookieCheck = authorizeAdminService.Check(Request, out cookieLogin, out isAdmin);

            if (isAdmin || cookieLogin == login)
            {
                var currentUser = _context
                    .PaszoUsers.Where(u => u.UserName == login.ToLower())
                    .Select(u => new UserDto() {
                        UserId = u.UserId,
                        UserName = u.UserName,
                        IsAdmin = u.IsAdmin,
                        Name = u.Name,
                        Email = u.Email,
                        TelephoneNumber = u.TelephoneNumber
                    })
                    .FirstOrDefault();
                return ActionContext.Request.CreateResponse(HttpStatusCode.OK, currentUser);
            }
            else
            {
                return ActionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                    new ActionResponse() { Success = false, Message = "no admin acces" });
            }
        }

        [AuthorizeService]
        [HttpGet]
        [Route("api/paszo/getuserhistory")]
        public HttpResponseMessage GetUserHistory(string userLogin)
        {
            AuthorizeAdminService authorizeAdminService = new AuthorizeAdminService();
            string cookieLogin;
            bool isAdmin;
            bool cookieCheck = authorizeAdminService.Check(Request, out cookieLogin, out isAdmin);

            if (isAdmin || cookieLogin == userLogin)
            {
                var userInDb = _context.PaszoUsers.FirstOrDefault(u => u.UserName == userLogin);
                var transactionsHostory = userInDb.Transactions.OrderByDescending(t => t.Date)

                    .Select(o => new TransactionHistoryDto
                    {
                        Id = o.Id,
                        MenuItem = o.MenuItem,
                        Ammount = o.Ammount,
                        Date = string.Format("{0}-{1}-{2}", o.Date.Year.ToString(), o.Date.Month.ToString(), o.Date.Day.ToString()),
                        Comment = o.Comment,
                        RestourantName = o.RestourantName
                    });

                return ActionContext.Request.CreateResponse(HttpStatusCode.OK, transactionsHostory);

            }
            else
            {
                return ActionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                    new ActionResponse() { Success = false, Message = "no admin acces" });
            }
        }

        [AuthorizeAdmin]
        [HttpGet]
        [Route("api/paszo/retireduser")]
        public HttpResponseMessage RetiredUser(string userName)
        {
            PaszoUser user = _context.PaszoUsers.FirstOrDefault(u => u.UserName == userName);
            if (user == null || user.RetiredDate < DateTime.Now)
            {
                return ActionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                    new ActionResponse() { Success = false, Message = String.Format("user {0} not exist or retired", userName) });
            }

            user.RetiredDate = DateTime.Now;
            _context.SaveChanges();

            return ActionContext.Request.CreateResponse(HttpStatusCode.OK,
                new ActionResponse() { Success = true, Message = String.Format("user {0} is retired", userName) });
                        
        }
    }
}