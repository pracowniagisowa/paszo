﻿using PASZO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using PASZO_Web_Api.Models;

namespace PASZO.Controllers.Api
{
    public class PaszoController : ApiController
    {
        private PaszoDbContext _context;

        public PaszoController()
        {
            _context = new PaszoDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [HttpGet]
        [Route("api/paszo/gettodayorder")]
        public List<OrderDto> GetTodayOrderSql()
        {
            //List<Transaction> transactionsList = new List<Transaction>();
            List<OrderDto> ordersList = new List<OrderDto>();

            var today = DateTime.Now.Date;

            var todayOrder = from o in _context.Transactions
                             where o.Date == today && o.MenuItem != null
                             select (new OrderDto
                             {
                                 Id = o.Id,
                                 User = o.PaszoUser.UserName,
                                 MenuItem = o.MenuItem,
                                 Comment = o.Comment
                             });// new OrderDto(o.Id, o.User.UserName, o.MenuItem, o.Comment );

            var todayOrdersList = todayOrder.ToList();
            //foreach (var item in todayOrdersList)
            //{
            //    ordersList.Add(new OrderDto(item.Id, item.User.UserName, item.MenuItem, item.Comment));
            //}
            return todayOrdersList;
        }

        [HttpGet]
        [Route("api/paszo/gettodaysum")]
        public string getTodatSum()
        {
            var today = DateTime.Now.Date;

            var todayOrders= _context.Transactions.Where(t => t.Date == today && t.MenuItem != null).ToList();
            var todaySum = todayOrders.Sum(s => s.Ammount);
            var todaySumReversed = todaySum * -1;

            return todaySumReversed.ToString("0.00");
        }
 
    }
}
