﻿using PASZO.Models;
using PASZO.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PASZO_Web_Api.Models;
using PASZO_WebApi.DTOs;
using AutoMapper;

namespace PASZO_WebApi.Controllers.Api
{
    public class PaszoAdminController : ApiController
    {
        private PaszoDbContext _context;

        public PaszoAdminController()
        {
            _context = new PaszoDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [AuthorizeAdmin]
        [HttpGet]
        [Route("api/paszo/adminlist")]
        public HttpResponseMessage GetAdminList()
        {

            List<AdminDataDto> adminDataList = new List<AdminDataDto>();
            var usersList = _context.PaszoUsers.Where(u => u.RetiredDate == null || u.RetiredDate > DateTime.Now).ToList();
            foreach (var user in usersList)
            {
                string currentUserName = user.UserName;
                var userInDb = _context.PaszoUsers.FirstOrDefault(u => u.UserName == currentUserName);
                float currentChange = userInDb.Transactions.Sum(a => a.Ammount);

                AdminDataDto currentAdminData = new AdminDataDto(currentUserName, currentChange, user.IsAdmin);
                adminDataList.Add(currentAdminData);
            }
            return ActionContext.Request.CreateResponse(HttpStatusCode.OK, adminDataList);
        }

        [AuthorizeAdmin]
        [HttpGet]
        [Route("api/paszo/totalchange")]
        public HttpResponseMessage GetTotalChange()
        {
            float change = _context.Transactions
                .Select(a => a.Ammount)
                .DefaultIfEmpty(0)
                .Sum();

            return ActionContext.Request.CreateResponse(HttpStatusCode.OK, Math.Round(change, 2));
        }

        [AuthorizeAdmin]
        [HttpPut]
        [Route("api/paszo/swichadmin")]
        public HttpResponseMessage SwichAdmin(string login)
        {

            PaszoUser currentUser = _context.PaszoUsers.FirstOrDefault(u => u.UserName == login.ToLower());
            currentUser.IsAdmin = !currentUser.IsAdmin;
            _context.SaveChanges();
            return ActionContext.Request.CreateResponse(HttpStatusCode.OK,
                new ActionResponse() { Success = true, Message = "admin swiched" });
        }

        [HttpGet]
        [Route("api/paszo/getlistofadmins")]
        public List<string> GetListOfAdmins()
        {
            var admins = from u in _context.PaszoUsers
                         where u.IsAdmin == true
                         select u.UserName;
            var listOfAdmins = admins.ToList();
            return listOfAdmins;
        }

        [HttpGet]
        [Route("api/paszo/getrestourantslist")]
        public IEnumerable<RestaurantDto> GetRestaurantsList()
        {
            var restaurants = _context.Restaurants
                .Select(r => new RestaurantDto()
                {
                    Id = r.Id,
                    Name = r.Name,
                    Url = r.Url
                })
                    .ToList();

            return restaurants;
        }

        [AuthorizeAdmin]
        [HttpDelete]
        [Route("api/paszo/delrestourant")]
        public HttpResponseMessage DelRestourant(int id)
        {
            AuthorizeAdminService authorizeAdminService = new AuthorizeAdminService();
            Restaurant currentRestourant = _context.Restaurants.FirstOrDefault(r => r.Id == id);
            string cookieLogin;
            bool isAdmin;
            bool cookieCheck = authorizeAdminService.Check(Request, out cookieLogin, out isAdmin);

            if (currentRestourant != null)
            {
                if (isAdmin)
                {
                    _context.Restaurants.Remove(currentRestourant);
                    _context.SaveChanges();
                    return ActionContext.Request.CreateResponse(HttpStatusCode.OK,
                        new ActionResponse() { Success = true, Message = "restourant deleted" });
                }
                else
                {
                    return ActionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                        new ActionResponse() { Success = false, Message = "no admin acces" });
                }
            }
            else
            {
                return ActionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                        new ActionResponse() { Success = false, Message = "id not found in database" });
            }


        }

        [AuthorizeAdmin]
        [HttpPost]
        [Route("api/paszo/postrestourant")]
        public HttpResponseMessage PostRestaurant(Restaurant newRestourant)
        {
            if (_context.Restaurants.Any(r => r.Name == newRestourant.Name))
                return ActionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                        new ActionResponse() { Success = false, Message = "Restaurant exists in database" });

            var maxId = _context.Restaurants.OrderByDescending(r => r.Id).FirstOrDefault().Id;
            //newRestourant.Id = maxId + 1;
            _context.Restaurants.Add(newRestourant);
            _context.SaveChanges();
            RestaurantDto newRestaurantDto = Mapper.Map<Restaurant, RestaurantDto>(newRestourant);

            return ActionContext.Request.CreateResponse(HttpStatusCode.OK, newRestaurantDto);
        }
    }
}