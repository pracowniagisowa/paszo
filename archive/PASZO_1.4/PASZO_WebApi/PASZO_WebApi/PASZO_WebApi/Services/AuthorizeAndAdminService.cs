﻿using PASZO.Models;
using PASZO_Web_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Security;

namespace PASZO.Services
{
    public class AuthorizeAdminService
    {
        private static Dictionary<string, string> cookiesDict = new Dictionary<string, string>();
        //private static PaszoDbContext _context = new PaszoDbContext();
        private static string cookieToken;


        public bool Check(HttpRequestMessage request, out string tokenLogin, out bool isAdmin)
        {
            isAdmin = false;
            string cookieLogin = "";
            tokenLogin = "";


            var cookiesDict = CookieService.ToDict(request);

            bool isCookieLogin = cookiesDict.TryGetValue("login", out cookieLogin);
            bool isCookieToken = cookiesDict.TryGetValue("paszoToken", out cookieToken);
            if (isCookieLogin && isCookieToken)
            {
                string cookieLoginString = cookieLogin;
                try
                {
                    var authTicket = FormsAuthentication.Decrypt(cookieToken);
                    string cookieTokenData = authTicket.UserData;
                    bool areTokensEqual = SecurePasswordHasher.Verify(cookieLogin, cookieTokenData);
                    if (areTokensEqual)
                    {
                        using (var context = new PaszoDbContext())
                        {
                            if (context.PaszoUsers.FirstOrDefault(u => u.UserName == cookieLoginString) != null)
                            {
                                isAdmin = context.PaszoUsers.FirstOrDefault(u => u.UserName == cookieLoginString).IsAdmin;
                            }
                            else
                            {
                                isAdmin = false;
                            }
                            
                            
                        }

                        //_context.Dispose();
                        tokenLogin = cookieLogin;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        //public CookieLoginAdminInfo
        //{
        //    public string CookieLogin { get; set; }
        //    public bool IsAdmin { get; set; }
        //    public CookieLoginAdminInfo(string login, bool isAdmin)
        //    {
        //        CookieLogin = login;
        //        IsAdmin = isAdmin;
        //    }
        //}
    }
}