﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http.Controllers;

namespace PASZO.Services
{
    public static class CookieService
    {
        public static Dictionary<string, string> ToDict(HttpRequestMessage request)
        {
            Dictionary<string, string> cookiesDict = new Dictionary<string, string>();
            var cookies = request.Headers.GetCookies();
            if (cookies.Count > 0)
            {
                var cookiesList = cookies[0].Cookies;
                foreach (var cookie in cookiesList)
                {
                    cookiesDict[cookie.Name] = cookie.Value;
                }
                return cookiesDict;
            }
            else
            {
                return cookiesDict;
            }



        }
    }
}