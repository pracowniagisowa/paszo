﻿using PASZO.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Security;

namespace PASZO.Services
{
    public class AuthorizeServiceAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            Dictionary<string, string> cookiesDict = CookieService.ToDict(actionContext.Request);

            string cookieLogin;
            string cookieToken;

            bool isCookieLogin = cookiesDict.TryGetValue("login", out cookieLogin);
            bool isCookieToken = cookiesDict.TryGetValue("paszoToken", out cookieToken);

            if (isCookieLogin && isCookieToken)
            {
                string cookieLoginString = cookieLogin;
                try
                {
                    var authTicket = FormsAuthentication.Decrypt(cookieToken);
                    string cookieTokenData = authTicket.UserData;
                    bool areTokensEqual = SecurePasswordHasher.Verify(cookieLogin, cookieTokenData);
                    return areTokensEqual;
                }
                catch (Exception)
                {

                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            actionContext.Response = new HttpResponseMessage();
            actionContext.Response.StatusCode = HttpStatusCode.BadRequest;
            actionContext.Response.ReasonPhrase = "Unauthorized request";
        }


    }
}