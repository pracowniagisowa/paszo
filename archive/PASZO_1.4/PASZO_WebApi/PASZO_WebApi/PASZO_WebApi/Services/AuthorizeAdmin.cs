﻿using PASZO.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Security;

namespace PASZO.Services
{
    public class AuthorizeAdminAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            AuthorizeAdminService authorizeAdminService = new AuthorizeAdminService();
            string cookieLogin;
            bool isAdmin;

            bool cookieCheck = authorizeAdminService.Check(actionContext.Request, out cookieLogin, out isAdmin);

            if (cookieCheck)
            {
                return isAdmin;
            }
            else
            {
                return false;
            }
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            actionContext.Response = new HttpResponseMessage();
            actionContext.Response.StatusCode = HttpStatusCode.BadRequest;
            actionContext.Response.ReasonPhrase = "Unauthorized request";
        }


    }
}