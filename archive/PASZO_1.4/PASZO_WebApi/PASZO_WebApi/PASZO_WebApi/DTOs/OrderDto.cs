﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO.Models
{
    public class OrderDto
    {
        public int Id { get; set; }
        public string User { get; set; }
        public MenuItem MenuItem { get; set; }
        public string Comment { get; set; }

        public OrderDto()
        {

        }

        public OrderDto(int id, string user, MenuItem menuItem, string comment)
        {
            this.Id = id;
            this.User = user;
            this.MenuItem = menuItem;
            this.Comment = comment;
        }
    }
}