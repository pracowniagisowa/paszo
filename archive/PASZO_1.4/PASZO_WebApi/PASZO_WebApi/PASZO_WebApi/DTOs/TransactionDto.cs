﻿using PASZO.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PASZO.DTOs
{
    public class TransactionDto
    {
        public int Id { get; set; }
        public float Ammount { get; set; }
        [StringLength(255)]
        public string Comment { get; set; }
        public string UserName { get; set; }
        public MenuItem MenuItem { get; set; }
        public DateTime Date { get; set; }
    }
}