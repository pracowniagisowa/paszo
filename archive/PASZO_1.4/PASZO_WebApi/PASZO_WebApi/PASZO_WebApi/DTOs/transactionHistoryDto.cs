﻿using PASZO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO_WebApi.DTOs
{
    public class TransactionHistoryDto
    {
        public int Id { get; set; }
        public MenuItem MenuItem { get; set; }
        public float Ammount { get; set; }
        public string Date { get; set; }
        public string Comment { get; set; }
        public string RestourantName { get; set; }
    }
}