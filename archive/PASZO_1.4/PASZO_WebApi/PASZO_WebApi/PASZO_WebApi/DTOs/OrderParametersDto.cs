﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO_WebApi.DTOs
{
    public class OrderParametersDto
    {
        public string TodayRestaurant { get; set; }
        public string Status { get; set; }
        public DateTime OrderTime { get; set; }
        public DateTime DeliveryTime { get; set; }
        public string Url { get; set; }
        public DateTime Date { get; set; }


    }
}