﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO.DTOs
{
    public class UserDto
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public bool IsAdmin { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int TelephoneNumber { get; set; }
    }
}