﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO.Models
{
    public class AdminDataDto
    {
        public string UserName { get; set; }
        public float UserChange { get; set; }
        public float AdminPay { get; set; }
        public bool IsAdmin { get; set; }


        public AdminDataDto(string userName, float userChange, bool isAdmin)
        {
            UserName = userName;
            UserChange = userChange;
            AdminPay = 0;
            IsAdmin = isAdmin;
        }
    }
}