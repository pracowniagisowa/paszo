namespace PASZO_WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RetiredDatenullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PaszoUsers", "RetiredDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PaszoUsers", "RetiredDate", c => c.DateTime(nullable: false));
        }
    }
}
