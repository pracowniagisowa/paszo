// <auto-generated />
namespace PASZO_WebApi.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class addingphoneandmailtousermodel : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addingphoneandmailtousermodel));
        
        string IMigrationMetadata.Id
        {
            get { return "201809260530040_adding phone and mail to user model"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
