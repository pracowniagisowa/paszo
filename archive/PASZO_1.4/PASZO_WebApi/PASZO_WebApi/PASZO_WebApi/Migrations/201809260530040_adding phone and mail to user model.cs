namespace PASZO_WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingphoneandmailtousermodel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaszoUsers", "Email", c => c.String());
            AddColumn("dbo.PaszoUsers", "TelephoneNumber", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaszoUsers", "TelephoneNumber");
            DropColumn("dbo.PaszoUsers", "Email");
        }
    }
}
