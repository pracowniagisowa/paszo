namespace PASZO_WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MenuItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BoxPrice = c.Single(nullable: false),
                        ItemName = c.String(),
                        ItemPrice = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderParameters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TodayRestaurant = c.String(),
                        Status = c.String(maxLength: 70),
                        OrderTime = c.DateTime(nullable: false),
                        DeliveryTime = c.DateTime(nullable: false),
                        Url = c.String(),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PaszoUsers",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 70),
                        IsAdmin = c.Boolean(nullable: false),
                        Name = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ammount = c.Single(nullable: false),
                        RestourantName = c.String(maxLength: 255),
                        Comment = c.String(),
                        Date = c.DateTime(nullable: false),
                        MenuItem_Id = c.Int(),
                        PaszoUser_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuItems", t => t.MenuItem_Id)
                .ForeignKey("dbo.PaszoUsers", t => t.PaszoUser_UserId)
                .Index(t => t.MenuItem_Id)
                .Index(t => t.PaszoUser_UserId);
            
            CreateTable(
                "dbo.Restaurants",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        AddidtionalCost = c.Single(nullable: false),
                        Url = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "PaszoUser_UserId", "dbo.PaszoUsers");
            DropForeignKey("dbo.Transactions", "MenuItem_Id", "dbo.MenuItems");
            DropIndex("dbo.Transactions", new[] { "PaszoUser_UserId" });
            DropIndex("dbo.Transactions", new[] { "MenuItem_Id" });
            DropTable("dbo.Restaurants");
            DropTable("dbo.Transactions");
            DropTable("dbo.PaszoUsers");
            DropTable("dbo.OrderParameters");
            DropTable("dbo.MenuItems");
        }
    }
}
