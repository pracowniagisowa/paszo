namespace PASZO_WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class populatewithadmindata : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO PaszoUsers (UserName, IsAdmin, Name, Password, Email) VALUES ('admin', 1, 'Admin', '$MYHASH$V1$10000$7k78PbKC/XZ0S4iKnfQSEf/rg2/mFANm5h6e3dgg0jT+5bMJ', 'paszoserwis@gmail.com')");
        }
        
        public override void Down()
        {
        }
    }
}
