namespace PASZO_WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingretiredDatetouser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaszoUsers", "RetiredDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaszoUsers", "RetiredDate");
        }
    }
}
