﻿using AutoMapper;
using PASZO_Web_Api.Models;
using PASZO_WebApi.App_Start;
using System.Linq;
using System.Web.Http;
using WebApplication1;

namespace PASZO_WebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var context = new PaszoDbContext();
            context.PaszoUsers.Any();

            Mapper.Initialize(c => c.AddProfile<MappingProfile>());

            GlobalConfiguration.Configure(WebApiConfig.Register);

            //http://stackoverflow.com/questions/19467673/entity-framework-self-referencing-loop-detected

            HttpConfiguration config = GlobalConfiguration.Configuration;

            config.Formatters.JsonFormatter
                        .SerializerSettings
                        .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        }
    }
}
