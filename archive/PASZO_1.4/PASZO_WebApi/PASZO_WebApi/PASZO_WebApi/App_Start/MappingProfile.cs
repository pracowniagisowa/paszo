﻿using AutoMapper;
using PASZO.Models;
using PASZO_WebApi.DTOs;
using PASZO_WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO_WebApi.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<OrderParameters, OrderParametersDto>();
            Mapper.CreateMap<OrderParametersDto, OrderParameters>(); 
            Mapper.CreateMap<Restaurant, RestaurantDto >();
            Mapper.CreateMap<RestaurantDto, Restaurant>();

        }

    }
}