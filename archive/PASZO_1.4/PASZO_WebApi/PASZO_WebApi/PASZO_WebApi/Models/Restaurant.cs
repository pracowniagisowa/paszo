﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PASZO.Models
{
    public class Restaurant
    {
        [Key]
        public int Id { get; set; }

        private List<MenuItem> _menuItemsList;

        public string Name { get; set; }
        
        [NotMapped]
        public List<MenuItem> MenuItemsList
        {
            get { return _menuItemsList; }
            set { _menuItemsList = value; }
        }
        public float AddidtionalCost { get; set; }
        public string Url { get; set; }


    }
}