﻿using PASZO.Models;
using PASZO_WebApi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PASZO_Web_Api.Models
{
    public class PaszoDbContext : DbContext
    {
        public PaszoDbContext() 
            //: base("name=PaszoDbContext")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<PaszoDbContext>());
        }

        public DbSet<PaszoUser> PaszoUsers { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<MenuItem> MenuItems { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<OrderParameters> OrdersParameters { get; set; }

        public static PaszoDbContext Create()
        {
            return new PaszoDbContext();
        }
        
    }
}