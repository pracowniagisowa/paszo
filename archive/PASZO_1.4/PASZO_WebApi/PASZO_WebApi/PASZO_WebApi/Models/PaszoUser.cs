﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PASZO.Models
{
    public class PaszoUser
    {


        [Key]
        public int UserId { get; set; }
        [Required]
        [StringLength(70)]
        public string UserName { get; set; }
        public bool IsAdmin { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        //public int TelephoneNumber { get; set; }
        public DateTime? RetiredDate { get; set; }

        private int _telephoneNumber;

        public int TelephoneNumber
        {
            get { return _telephoneNumber; }
            set {
                    string input = value.ToString();
                    bool isInt = int.TryParse(input, out _telephoneNumber);
                    if (!isInt)
                    {
                        _telephoneNumber = 0;
                    }
                }
        }



        public virtual ICollection<Transaction> Transactions { get; set; }

        public PaszoUser()
        {
            Transactions = new List<Transaction>();
        }

    }
}