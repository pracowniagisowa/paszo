﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PASZO.Models
{
    public class Transaction
    {
        private MenuItem _menuItem;
        private DateTime _date;

        public int Id { get; set; }
        public float Ammount { get; set; }
        [StringLength(255)]
        public string RestourantName { get; set; }
        public string Comment { get; set; }

        public virtual PaszoUser PaszoUser { get; set; }

        public virtual MenuItem MenuItem
        {
            get { return _menuItem; }
            set
            {
                _menuItem = value;
                if (_menuItem != null)
                {
                    this.SetAmmount();
                }
            }
        }

        // public MenuItem MenuItem { get; set; }

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }        
        // konstruktor do autmatycznego wpisywania daty - jeszcze nie wiem czy to zadziała w entity
        public Transaction()
        {
            Date = DateTime.Now.Date;
        }

        public void SetAmmount()
        {
            Ammount = (MenuItem.ItemPrice + MenuItem.BoxPrice) * -1;
        }
    }
}