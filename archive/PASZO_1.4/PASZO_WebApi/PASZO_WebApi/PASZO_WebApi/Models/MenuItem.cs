﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO.Models
{
    public class MenuItem
    {

        public int Id { get; set; }

        private string _itemName;
        private float _itemPrice;

        public float BoxPrice { get; set; }

        public string ItemName
        {
            get { return _itemName; }
            set { _itemName = value; }
        }        

        public float ItemPrice  
        {
            get { return _itemPrice; }
            set { _itemPrice = value; }
        }

        

        //public string Comment { get; set; }

        //public static implicit operator MenuItem(string v)
        //{
        //    throw new NotImplementedException();
        //}
    }
}