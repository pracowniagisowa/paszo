﻿using PASZO_Web_Api.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PASZO_WebApi.Models
{
    public class OrderParameters
    {
        //private static OrderParameters instance;

        //private OrderParameters() { }

        //public static OrderParameters Instance
        //{
        //    get
        //    {
        //        if (instance == null)
        //        {
        //            instance = new OrderParameters();
        //        }
        //        return instance;
        //    }
        //}
        private string _todayRestaurant;

        public string TodayRestaurant
        {
            get { return _todayRestaurant; }
            set
            {
                _todayRestaurant = value;
                using (var context = new PaszoDbContext())
                {
                    var currentRestaurant = context.Restaurants.FirstOrDefault(u => u.Name == _todayRestaurant);
                    if (currentRestaurant != null)
                    {
                        Url = currentRestaurant.Url;
                    }
                    else
                    {
                        Url = null;
                    }
                }
            }
        }

        [Key]
        public int Id { get; set; }
        [StringLength(70)]
        public string Status { get; set; }
        public DateTime OrderTime { get; set; }
        public DateTime DeliveryTime { get; set; }
        public string Url { get; set; }
        public DateTime Date { get; set; }

        public OrderParameters()
        {
            Date = DateTime.Now.Date;
        }

        public void ClearData()
        {
            TodayRestaurant = null;
            Status = null;
            OrderTime = new DateTime(0, 0, 0, 12, 0, 0);
            DeliveryTime = new DateTime(0, 0, 0, 0, 0, 0);
        }
    }
}