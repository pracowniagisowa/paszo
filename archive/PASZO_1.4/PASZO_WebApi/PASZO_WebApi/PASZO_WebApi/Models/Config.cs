﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PASZO.Models.DataModels
{
    public static class Config
    {
        public class User
        {
            public int UserId { get; set; }
            [Required]
            [StringLength(70)]
            public string UserName { get; set; }

            public List<Transaction> Transactions { get; set; }
        }


        public class Transaction
        {
            private MenuItem _menuItem;
            private DateTime _date;

            public int Id { get; set; }
            public float Ammonut { get; set; }
            public string User { get; set; }

            public MenuItem MenuItem
            {
                get { return _menuItem; }
                set { _menuItem = value; }
            }

            public DateTime Date
            {
                get { return _date; }
                set { _date = value; }
            }
            // konstruktor do autmatycznego wpisywania daty - jeszcze nie wiem czy to zadziała w entity
            public Transaction()
            {
                Date = DateTime.Now.Date;
            }
        }

        public class MenuItem
        {
            private string _itemName;
            private float _itemPrice;
            public float BoxPrice { get; set; }
            public int Id { get; set; }

            public string ItemName
            {
                get { return _itemName; }
                set { _itemName = value; }
            }

            public float ItemPrice
            {
                get { return _itemPrice; }
                set { _itemPrice = value; }
            }
        }
    }
}