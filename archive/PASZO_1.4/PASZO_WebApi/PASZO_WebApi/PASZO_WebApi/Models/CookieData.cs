﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PASZO.Models
{
    public class CookieData
    {
        public string Login { get; set; }
        public string Token { get; set; }

    }
}