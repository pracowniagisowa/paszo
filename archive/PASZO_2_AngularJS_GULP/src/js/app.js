angular.module('PaszoApp', ['ngRoute', 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'ngCookies']) //dependencies
    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/login.html',
                // controller: 'LoginController'
            })
            .when('/order', {
                templateUrl: 'views/order.html',
                // controller: 'OrderController'
            })
            .when('/admin', {
                templateUrl: 'views/admin.html',
                // controller: 'AdminController'
            })
            .when('/userPanel', {
                templateUrl: 'views/userPanel.html',
                // controller: 'UserPanelController' a
            })

        .otherwise({
            redirectTo: '/'
        });
    }, function($httpProvider) {
        $httpProvider.defaults.withCredentials = true;

    })
    .run(function run($rootScope) {
        $rootScope.configData = configData;
        // console.log($rootScope.configData)
    });
    