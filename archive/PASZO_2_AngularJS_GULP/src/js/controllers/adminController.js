angular.module('PaszoApp')
    .controller('AdminController', AdminController);

AdminController.$inject = ['$q', '$scope', '$rootScope', '$location', '$http', 'HttpGetService', '$cookies', 'AlertService']

function AdminController($q, $scope, $rootScope, $location, $http, HttpGetService, $cookies, AlertService) {
    var $admin = this;
    $scope.ismeridian = false;
    $scope.hstep = 1;
    $scope.mstep = 5;
    $scope.isAddingUserAviable = false;
    $scope.addingUserAviableLabel = "zablokowane";
    $scope.addingUserAviableButtonLabel = "zablokowane";


    console.log("AdminController")
    var paszoToken = $cookies.get('paszoToken');

    $admin.initUserController = function () {
        $http({
                method: 'GET',
                url: $rootScope.configData.host + '/Account/GetUserByToken',
                withCredentials: true,
                headers: {
                    'Authorization': 'Bearer ' + paszoToken
                }
            })
            .then(function (data) {
                $rootScope.userFromToken = data.data;
                $admin.userFromToken = data.data;
                // console.log("GetUserByToken:");
                // console.log($admin.userFromToken);
                if (!data.data.isAdmin) {
                    $location.path('/');
                }
                $admin.refreshAll();

            }, function (error) {
                console.log(error)
                console.log("powrot do logowania");
                $location.path('/');
            });
    };


    /* #region  Adding new users */

    $admin.getIsAddingUserAviable = function () {
        $http({
            method: 'GET',
            url: $rootScope.configData.host + '/Account/IsRegisterAllowed',
        }).then(function (data) {
            // console.log(data);

            $scope.isAddingUserAviable = data.data;
            if ($scope.isAddingUserAviable) {
                $scope.addingUserAviableLabel = "aktywne";
                $scope.addingUserAviableButtonLabel = "Zablokuj";
            } else {
                $scope.addingUserAviableLabel = "zablokowane";
                $scope.addingUserAviableButtonLabel = "Odblokuj";
            }
        }, function (error) {
            console.log(error);
        });
    };

    $admin.swichIsAddingUserAviable = function () {
        $http({
            method: 'GET',
            url: $rootScope.configData.host + '/Admin/SwichIsAddingUserAviable',
            headers: {
                'Authorization': 'Bearer ' + paszoToken
            }
        }).then(function (data) {
            // console.log(data);
            $admin.getIsAddingUserAviable();
        }, function (error) {
            console.log(error);
        });
    };
    /* #endregion */

    /* #region  Admin methods */


    $admin.getAdminParameters = function () {
        $http({
                method: 'GET',
                url: $rootScope.configData.host + '/admin/GetAdminParameters',
                // withCredentials: true
                headers: {
                    'Authorization': 'Bearer ' + paszoToken
                }

            })
            .then(function (data) {
                $scope.adminParameters = data.data;
                // let timeObjectUTC = new Date();
                // let timeObject = new Date();
                // timeObject.setMinutes(timeObjectUTC.getMinutes() );
                // $scope.actualCashBoxStateTime = timeObject.toLocaleString('pl-PL');
                $scope.actualCashBoxStateTime = $scope.timeUTCToLocalString(data.data.actualCashBoxState.dateTime);
                // console.log($scope.actualCashBoxStateTime);
            }, function (error) {
                console.log(error)
            })
    }

    $admin.adminPay = function (user) {
        var transactionUrl = $rootScope.configData.host + '/Transaction/AddTransaction?userId=' + user.identityUserId;
        var payJson = {
            "ammount": user.adminPay
        };
        $http({
                method: 'POST',
                data: payJson,
                url: transactionUrl,
                // withCredentials: true
                headers: {
                    'Authorization': 'Bearer ' + paszoToken
                }
            })
            .then(function () {
                $admin.refreshAll()
            }, function (error) {
                console.log(error);
            });
    };

    $admin.swichAdmin = function (user) {
        // console.log(user);
        var swichAdminUrl = $rootScope.configData.host + '/admin/SwichAdminRole?userId=' + user.identityUserId;
        // $http.put(swichAdminUrl)
        $http({
                method: 'GET',
                url: swichAdminUrl,
                // withCredentials: true
                headers: {
                    'Authorization': 'Bearer ' + paszoToken
                }
            })
            .then(function () {
                $admin.refreshAll()
                console.log("swich admin - " + user.userName)
            }, function (error) {
                console.log(error);
            });
    };

    $admin.resetPassword = function (user) {
        var tempPass = 'paszo';
        var setPassJson = {
            "UserName": user.userName,
            "NewPassword": tempPass,
            "Password": "testtest"
        };
        // console.log(setPassJson)

        $http({
                method: "POST",
                url: $rootScope.configData.host + '/account/ResetPassword',
                data: setPassJson,
                // withCredentials: true       
                headers: {
                    'Authorization': 'Bearer ' + paszoToken
                }
            })
            .then(function () {
                var message = 'ustawiono hasło "' + tempPass + '" dla uzytkownika ' + user.userName;
                AlertService.openModal('', message);
                console.log(message)
            }, function (error) {
                console.log(error);
            });
    };

    $admin.retireUser = function (user) {
        var answer = confirm("Czy na pewno chcesz usunąć użytkownika?");
        // console.log(answer);
        if (answer) {
            $http({
                    method: "DELETE",
                    url: $rootScope.configData.host + '/account/RetireUser?userId=' + user.identityUserId,
                    // withCredentials: true
                    headers: {
                        'Authorization': 'Bearer ' + paszoToken
                    }

                })
                .then(function () {
                    // console.log("Retire - refresh");
                    $admin.refreshAll();
                    AlertService.openModal('Użytkownik z dniem dzisiajszym został usunięty z aplikacji',
                        'jednak jego transakcje pozostały w bazie danych. Aby przywrócić użytkownika należy dodać go z poziomy aplikacji oraz podpiąc relację');
                });
        }
    };
    /* #endregion */

    /* #region  Restaurans */


    $admin.getRestaurantsList = function () {
        $http({
                method: 'GET',
                url: $rootScope.configData.host + '/restaurant/GetRestaurants',
                // withCredentials: true
                headers: {
                    'Authorization': 'Bearer ' + paszoToken
                }
            })
            .then(function (data) {
                $scope.restaurantsList = data.data;
                // console.log($scope.restaurantsList);
            }, function (error) {
                console.log(error)
            });
    };


    $admin.postRestaurant = function () {
        $http({
                method: "POST",
                url: $rootScope.configData.host + '/restaurant/PostRestaurant',
                data: {
                    "Name": $scope.restaurantName,
                    "Url": $scope.restaurantUrl,
                    "TelephoneNumber": $scope.restaurantTel
                },
                // withCredentials: true
                headers: {
                    'Authorization': 'Bearer ' + paszoToken
                }

            })
            .then(function () {
                $admin.getRestaurantsList();
                $scope.restaurantName = undefined;
                $scope.restaurantUrl = undefined;
            }, function (error) {
                console.log(error);
                // AlertService.openModal('', message);
            });
    };

    $admin.delRestourant = function (currentId) {
        console.log(currentId)
        $http({
                method: 'DELETE',
                url: $rootScope.configData.host + '/restaurant/DelRestaurant?id=' + currentId,
                // withCredentials: true
                headers: {
                    'Authorization': 'Bearer ' + paszoToken
                }

            })
            .then(function () {
                $admin.getRestaurantsList();
            }, function (error) {
                console.log(error);
            })

    }

    /* #endregion */

    /* #region  Order Parameters */


    $admin.getOrderParameters = function () {
        $http({
                method: 'GET',
                url: $rootScope.configData.host + '/paszo/GetTodayOrderParameters',
                // withCredentials: true
                headers: {
                    'Authorization': 'Bearer ' + paszoToken
                }
            })
            .then(function (data) {
                // console.log(data);
                $scope.orderParameters = data.data;

                var orderDateTimeArray = data.data.orderTime.split("T");
                // console.log(orderDateTimeArray)
                var orderTimeArray = orderDateTimeArray[1].split(":");
                // console.log(orderTimeArray)
                $scope.orderTimeObject = new Date();
                $scope.orderTimeObject.setHours(orderTimeArray[0]);
                $scope.orderTimeObject.setMinutes(orderTimeArray[1]);

                var deliveryDateTimeArray = data.data.deliveryTime.split("T");
                var deliveryTimeArray = deliveryDateTimeArray[1].split(":");
                if (data.data.deliveryTime == "00:00:00") {
                    // console.log(data.data.DeliveryTime)
                    $scope.deliveryTimeVisibility = false;
                } else {
                    $scope.deliveryTimeVisibility = true
                }
                $scope.deliveryTimeObject = new Date();
                $scope.deliveryTimeObject.setHours(deliveryTimeArray[0]);
                $scope.deliveryTimeObject.setMinutes(deliveryTimeArray[1]);



            }, function (error) {
                console.log(error)
            });
    };

    $admin.postOrderParameters = function () {
        $http({
                method: "POST",
                url: $rootScope.configData.host + '/paszo/PostTodayOrderParameters',
                data: $scope.orderParameters,
                headers: {
                    'Authorization': 'Bearer ' + paszoToken
                }
            })
            .then(function () {
                console.log($scope.orderParameters);
                $admin.getOrderParameters();
            }, function (error) {
                console.log($scope.orderParameters);
                console.log(error);
                // AlertService.openModal('', message);
            });
    };



    $admin.deliveryTimeChanged = function () {
        // var deliveryTimeString = $scope.deliveryTimeObject.getHours() + ":" + $scope.deliveryTimeObject.getMinutes() + ":00";
        var deliveryTimeString = $admin.timeObjectToString($scope.deliveryTimeObject);

        $scope.orderParameters.deliveryTime = deliveryTimeString;
        console.log($scope.orderParameters.deliveryTime)
    };

    /* #endregion */

    /* #region  Drawing */
    $admin.GetMenuItemNamesThatReduceDrawingProbability = function () {
        $http({
            method: "GET",
            url: $rootScope.configData.host + '/admin/GetMenuItemNamesThatReduceDrawingProbability',
            headers: {
                'Authorization': 'Bearer ' + paszoToken
            }
        }).then(function (data) {
            $scope.menuItemNamesThatReduceDrawingProbability = data.data;
            // console.log($scope.menuItemNamesThatReduceDrawingProbability);
        }, function (error) {
            console.log(error);
            AlertService.openModal(error.statusText, error.data);
        });
    }

    $admin.GetMenuItemNamesThatIncreaseDrawingProbability = function () {
        $http({
            method: "GET",
            url: $rootScope.configData.host + '/admin/GetMenuItemNamesThatIncreaseDrawingProbability',
            headers: {
                'Authorization': 'Bearer ' + paszoToken
            }
        }).then(function (data) {
            $scope.menuItemNamesThatIncreaseDrawingProbability = data.data;
            // console.log($scope.menuItemNamesThatIncreaseDrawingProbability);
        }, function (error) {
            console.log(error);
            AlertService.openModal(error.statusText, error.data);
        });
    }

    $admin.PostMenuItemNameAffectDrawing = function (affectType) {
        // console.log(affectType);
        var nameChangeProbability;
        if (affectType == 1) {
            nameChangeProbability = $scope.nameReduceProbability;
        } else if (affectType == 2) {
            nameChangeProbability = $scope.nameIncreaseProbability;
        } else {
            nameChangeProbability = undefined;
        }
        $http({
            method: "POST",
            url: $rootScope.configData.host + '/admin/PostMenuItemNameAffectDrawing',
            data: {
                "NamePart": nameChangeProbability,
                "AffectType": affectType
            },
            headers: {
                'Authorization': 'Bearer ' + paszoToken
            }
        }).then(function () {
            $scope.nameReduceProbability = undefined;
            $scope.nameIncreaseProbability = undefined;
            $admin.GetMenuItemNamesThatReduceDrawingProbability();
            $admin.GetMenuItemNamesThatIncreaseDrawingProbability();
        }, function (error) {
            console.log(error);
            AlertService.openModal(error.statusText, error.data);
        });
    };

    $admin.DeleteMenuItemNamesThatChangeDrawingProbability = function (id) {
        $http({
            method: "DELETE",
            url: $rootScope.configData.host + '/admin/DeleteMenuItemNamesThatChangeDrawingProbability?id=' + id,
            headers: {
                'Authorization': 'Bearer ' + paszoToken
            }

        }).then(function () {
            $admin.GetMenuItemNamesThatReduceDrawingProbability();
            $admin.GetMenuItemNamesThatIncreaseDrawingProbability();
        }, function (error) {
            console.log(error);
            AlertService.openModal(error.statusText, error.data);

        });
    };

    $admin.postUserToImmunity = function () {
        $http({
            method: "POST",
            url: $rootScope.configData.host + '/admin/PostUserToImmunity?userName=' + $scope.userNameToImmunity,
            headers: {
                'Authorization': 'Bearer ' + paszoToken
            }
        }).then(function () {
            $admin.getUsersWithImmunity();
            $scope.userNameToImmunity = undefined;
        }, function (error) {
            console.log(error);
            AlertService.openModal(error.statusText, error.data);

        });
    };

    $admin.getUsersWithImmunity = function () {
        $http({
            method: "GET",
            url: $rootScope.configData.host + '/admin/GetUsersWithImmunity',
            headers: {
                'Authorization': 'Bearer ' + paszoToken
            }
        }).then(function (data) {
            $scope.usersWithImmunity = data.data;

            // console.log($scope.usersWithImmunity);
        }, function (error) {
            console.log(error);
        });
    };

    $admin.deleteUserWithImmunity = function (userName) {
        $http({
            method: "DELETE",
            url: $rootScope.configData.host + '/admin/DeleteUserWithImmunity?userName=' + userName,
            headers: {
                'Authorization': 'Bearer ' + paszoToken
            }

        }).then(function () {
            $admin.getUsersWithImmunity();
        }, function (error) {
            console.log(error);
            AlertService.openModal(error.statusText, error.data);
        });
    };

    $admin.getDrawingTimeTotalMinutesUtc = function () {
        $http({
            method: "GET",
            url: $rootScope.configData.host + '/admin/GetDrawingTimeTotalMinutesUtc',
            headers: {
                'Authorization': 'Bearer ' + paszoToken
            }

        }).then(function (data) {
            // console.log(data)
            var drawingTimeObject = new Date();
            drawingTimeObject.setHours(0);
            drawingTimeObject.setMinutes(data.data - drawingTimeObject.getTimezoneOffset());
            // console.log(drawingTimeObject);
            $scope.drawingTimeObject = drawingTimeObject;
        });
    }

    $admin.postDrawingTimeTotalMinutes = function () {
        // console.log("cyk");
        if ($scope.drawingTimeTotalMinutesUtc) {
            $http({
                method: "POST",
                url: $rootScope.configData.host + '/admin/PostDrawingTimeTotalMinutesUtc?drawingTotalMinutes=' + $scope.drawingTimeTotalMinutesUtc,
                headers: {
                    'Authorization': 'Bearer ' + paszoToken
                }
            }).then(function () {
                $admin.getDrawingTimeTotalMinutesUtc();
            }, function (error) {
                console.log(error);
            });
        }
    };

    $admin.drawingTimeChanged = function () {
        var drawingTimeTotalMinutesUtc =
            $scope.drawingTimeObject.getHours() * 60 + $scope.drawingTimeObject.getMinutes() + $scope.drawingTimeObject.getTimezoneOffset()
        $scope.drawingTimeTotalMinutesUtc = drawingTimeTotalMinutesUtc;
        // console.log($scope.drawingTimeTotalMinutesUtc);
    };
    /* #endregion */

    /* #region  CashBox and CashBox history */

    $scope.isPostCashBoxStateDisabled = true;
    $scope.isCashBoxCommentChanged = false;

    $admin.getGetCashBoxStateHistory = function () {
        $http({
            method: "GET",
            url: $rootScope.configData.host + '/paszo/GetCashBoxStateHistory',
            headers: {
                'Authorization': 'Bearer ' + paszoToken
            }
        }).then(function (data) {
            // console.log(data.data);
            $scope.cashBoxStateHistory = data.data;
        });
    }

    $admin.postExtraCash = function () {
        $http({
                method: "POST",
                url: $rootScope.configData.host + "/Admin/SetExtraCash?extraCash=" + $scope.adminParameters.extraCash,
                headers: {
                    'Authorization': 'Bearer ' + paszoToken
                }
            })
            .then(function () {
                console.log("posted: " + $scope.adminParameters.extraCash);
                $admin.getAdminParameters();
                $scope.isPostCashBoxStateDisabled = true;
            }, function (error) {
                console.log(error)
            })
    };

    $admin.postCashBoxState = function () {
        $http({
            method: "POST",
            data: {
                // "cashState": $scope.adminParameters.boxCash,
                // "cashDatabaseState": $scope.adminParameters.totalChange,
                "extraCash": $scope.adminParameters.actualCashBoxState.extraCash,
                "comment": $scope.adminParameters.actualCashBoxState.comment
            },
            url: $rootScope.configData.host + '/paszo/PostCashBoxState',
            headers: {
                'Authorization': 'Bearer ' + paszoToken
            }
        }).then(function (data) {
            //   $admin.getActualCashBoxState();
            $scope.isPostCashBoxStateDisabled = true;
            $scope.isCashBoxCommentChanged = false;
            $admin.getGetCashBoxStateHistory();
            $admin.getAdminParameters();

        }, function (error) {
            console.log(error);
        });
    };

    $scope.updateCashBoxStateOnExtraCash = function () {
        if (!$scope.isCashBoxCommentChanged) {
            $scope.adminParameters.actualCashBoxState.comment = "";
        }
        $scope.adminParameters.actualCashBoxState.cashState = $scope.adminParameters.actualCashBoxState.cashDatabaseState + $scope.adminParameters.actualCashBoxState.extraCash;
        $scope.isPostCashBoxStateDisabled = false;
        // console.log("updateCashBoxStateOnExtraCash");
    };

    $scope.updateExtraCashOnCashBoxState = function () {
        if (!$scope.isCashBoxCommentChanged) {
            $scope.adminParameters.actualCashBoxState.comment = "";
        }
        $scope.adminParameters.actualCashBoxState.extraCash = $scope.adminParameters.actualCashBoxState.cashState - $scope.adminParameters.actualCashBoxState.cashDatabaseState;
        $scope.isPostCashBoxStateDisabled = false;
        // console.log("updateCashBoxStateOnExtraCash");
    };

    $scope.commentFieldChanged = function () {
        $scope.isPostCashBoxStateDisabled = false;
        $scope.isCashBoxCommentChanged = true;
    }

    $scope.timeUTCToLocalString = function (timeString) {
        // console.log('----------');
        let timeObjectUTC = timeStringToUTCTimeObject(timeString);
        // console.log(timeObjectUTC);
        let timeStringGMT = timeObjectUTC.toLocaleString('pl-PL');
        let timeStringGMTSplited = timeStringGMT.split(',');
        // console.log(timeStringGMTSplited);
        return timeStringGMTSplited[0] + timeStringGMTSplited[1].substr(0, 6);
    }

    timeStringToUTCTimeObject = function (str) {
        var dateTimeList = str.split('T');
        var dateSplited = dateTimeList[0].split('-');
        var timeSplited = dateTimeList[1].split(':');

        var dateTimeUTC = new Date(Date.UTC(dateSplited[0], dateSplited[1] - 1, dateSplited[2], timeSplited[0], timeSplited[1], 0));
        return dateTimeUTC;
    }

    /* #endregion */

    /* #region  History */

    $scope.monthsNumber = 3;

    $admin.getTransactionsHistory = function () {
        $http({
                method: "GET",
                url: $rootScope.configData.host + "/Admin/GetTransactionsHistory?monthsNumber=" + $scope.monthsNumber,
                headers: {
                    'Authorization': 'Bearer ' + paszoToken
                }
            })
            .then(function (data) {
                $scope.transactionsHistory = data.data;
                console.log($scope.transactionsHistory);
                // console.log("transactions from " + $scope.monthsNumber);
            }, function (error) {
                console.log(error)
            })
    };

    $admin.setMonthsNumber = function (monthsNumber) {
        $scope.monthsNumber = monthsNumber;
        // console.log($scope.monthsNumber);
        $admin.getTransactionsHistory();
    };

    /* #endregion */

    $admin.goToOrder = function () {
        $location.path('/order');
    };

    $admin.refreshAll = function () {
        // $admin.getOrderParameters();
        // $admin.usersAdmin();
        $admin.getRestaurantsList();
        $admin.getAdminParameters();
        // $admin.getTotalSum();
        $admin.getIsAddingUserAviable();
        $admin.GetMenuItemNamesThatReduceDrawingProbability();
        $admin.GetMenuItemNamesThatIncreaseDrawingProbability();
        $admin.getUsersWithImmunity();
        $admin.getDrawingTimeTotalMinutesUtc();
        $admin.getGetCashBoxStateHistory();
        $admin.getTransactionsHistory();
    };

    $admin.refreshAll();

};