(function(){
  angular.module('PaszoApp')
  .controller('NewUserModalController', NewUserModalController);

  NewUserModalController.$inject = ['$scope', '$uibModalInstance', '$rootScope', '$location', '$http', 'HttpGetService', '$cookies', 'AlertService']
  function NewUserModalController($scope, $uibModalInstance, $rootScope, $location, $http, HttpGetService, $cookies, AlertService){
    var $modal = this;
    $scope.isWhiteSpace = false;

    $modal.addNewUser = function(){
        var newUserUrl = $rootScope.configData.host + '/account/Register'
        var newUserJson = {
            "UserName": $scope.newLogin.toLowerCase(),
	          "Name": $scope.newName,
            "Password": $scope.newPassword,
            "Email": $scope.newEmail,
            "TelephoneNumber": $scope.newTelephone
        };

        if ($scope.newLogin && $scope.newPassword) {
          console.log("dane nowego użytkownika wypełnione");
          if ($scope.newPassword == $scope.newPasswordRepeat) {
            console.log("hasła nowego użytkownika OK")

            $http({
              method: "POST",
              url: newUserUrl,
              data: newUserJson,
              withCredentials: true
            })
            .then(function(data){

              if (data.data.succeeded == true) {
                AlertService.openModal('Witaj ' + $scope.newName, 'twój login to: ' + $scope.newLogin.toLowerCase() + ', teraz mozesz się zalogować')
                $uibModalInstance.dismiss('cancel');
              } else {
                AlertService.openModal(data.data.errors[0].code, data.data.errors[0].description)
                $uibModalInstance.dismiss('cancel');
              }
             
              
             
            }, function(error){
              console.log(error)
              AlertService.openModal('status: ' + error.status ,error.data.Message)
            });
          } else {
            AlertService.openModal('Uwaga', 'podane hasła się nie zgadzają')
          };
          
        } else {
          AlertService.openModal('Uwaga', 'wypełnij dane!!');
        };

        
        
    };

    $modal.cancelModal = function () {
      $uibModalInstance.dismiss('cancel');
    };

    $modal.isWhiteSpace = function (str) {
      console.log( /\s/.test(str));
      $scope.isWhiteSpace = /\s/.test(str);   
    };


    
  };
})();