(function(){
'use strict'
// TODO: poprawić tekst w dotyczący GISonLine
angular.module('PaszoApp')
.service('InfoService', InfoService);

InfoService.$inject = ['$uibModal']
function InfoService($uibModal){
  var $info = this;

  $info.openModal = function () {
        var modalInstance = $uibModal.open({
          animation: $info.animationsEnabled,
          templateUrl: 'views/infoModal.html',
          controller: 'AlertModalController',
          controllerAs: '$modal',
          // backdrop: 'static',
          resolve:{
            message: function(){
              return undefined;
            },
            header: function(){
                return undefined
                 }        
             }
        });
      };
};


})();