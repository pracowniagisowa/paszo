(function(){
    angular.module('PaszoApp')
    .controller('AggregateOrdersController', AggregateOrdersController);
  
    AggregateOrdersController.$inject = ['$scope', '$uibModalInstance', '$uibModal', '$rootScope', '$cookies', '$http', 'message','header' ]
    function AggregateOrdersController($scope, $uibModalInstance, $uibModal, $rootScope, $cookies, $http){
      var $aggregate = this;

      var paszoToken = $cookies.get('paszoToken');
      $scope.aggregateOrders;
      $scope.todayDrawing;

      $aggregate.updateOrdersList = function(){
        $http({
            method: 'GET',
            url: $rootScope.configData.host + '/paszo/gettodayorder',
            headers: {'Authorization': 'Bearer '+ paszoToken}
        }).then(function(data){
            // console.log(data);
            $scope.todayOrdersByRestaurans = data.data;
            console.log($scope.todayOrdersByRestaurans);
        });
      };

      $aggregate.getTodayOrderParameters = function(){
        $http({
          method: "GET",
          url: $rootScope.configData.host + '/paszo/GetTodayOrderParameters',
          headers: {'Authorization': 'Bearer '+ paszoToken}
        }).then(
          function(data){
            $scope.todayOrderParameters = data.data;
            console.log($scope.todayOrderParameters);
          }
        );
      };
  
  
      $aggregate.ok = function () {
        $uibModalInstance.dismiss('cancel');
      };

      $aggregate.updateOrdersList();
      $aggregate.getTodayOrderParameters();
  
    };
  })();