(function(){
  angular.module('PaszoApp')
  .controller('LoginController', LoginController);

  LoginController.$inject = ['$q', '$scope', '$rootScope', '$location', '$http', 'HttpGetService', '$cookies', '$uibModal', 'AlertService', 'InfoService']
  function LoginController($q, $scope, $rootScope, $location, $http, HttpGetService, $cookies, $uibModal, AlertService, InfoService){
    var $login = this;
    console.log('login controller');


    var config;
    $scope.loading = false;  
    var token = $cookies.get('paszoToken');  
    var isAddingUserAviable = false;
    var adminsList = [];
    console.log('Cookie token: ' + token) 
    
    $login.getIsAddingUserAviable = function(){
      $http({
        method: 'GET',
        url: $rootScope.configData.host + '/Account/IsRegisterAllowed',
      }).then(function(data){
        console.log(data);
        isAddingUserAviable = data.data;
      });
    };
    
    $login.getUserDataAndRedirect = function(token){
      console.log($rootScope.configData.host + '/Account/GetUserByToken');
      $http({
        method: 'GET',
        url: $rootScope.configData.host + '/Account/GetUserByToken',
        // withCredentials: true,
        headers: {'Authorization': 'Bearer '+ token}
      })
      .then(function(data){
        console.log(data);

        $rootScope.userFromToken = data.data;
        // $rootScope.isAdmin = data.data.IsAdmin;
        $rootScope.name = data.data.Name;

        if ($rootScope.name == null) {
          $rootScope.displayName = $rootScope.login;
        }
        else{
          $rootScope.displayName = $rootScope.name;
        };
        console.log('Login from $rootscope: ' + $rootScope.login);
        $location.path('/order');

      }, function(error){
          console.log(error)
        });
    };

    if (token != undefined) {
      $login.getUserDataAndRedirect(token);
    };


    $login.loginButton = function(){  

      $scope.loading = true;

      var loginJson = {
          "UserName": $scope.username.toLowerCase(),
          "Password": $scope.password
              }
      $http({
        method: 'POST',
        url: $rootScope.configData.host + '/Account/login',
        data: loginJson,
        withCredentials: true
      })
      .then(function(data){
        console.log("data:");
        console.log(data);

        var checkLogin = data.data.Success;
        var uName = $scope.username;
        // console.log(uName);
        $scope.loading = false; 

        if (data.data.succeeded == false) {
          $scope.loading = false; 
          AlertService.openModal('Błędny login lub hasło', 
          'Jeśli jesteś nowym użytkownikiem skorzystaj z przycisku w prawym górnym rogu aplikacji')
        };
        if (data.data.succeeded == true) {          
          console.log('logowanie przeszlo...');
          $rootScope.login = uName;
          $cookies.put('paszoToken', data.data.token); //, {'path': '/'}
          $login.getUserDataAndRedirect(data.data.token);
          // $location.path('/order');
        };

      }, function(error){
        $scope.loading = false; 
        AlertService.openModal('status: ' + error.status ,error.statusText)
                 
        });      
      };

      $login.animationsEnabled = true;

      $login.newUser = function (){
        if (isAddingUserAviable) {
          var modalInstance = $uibModal.open({
            animation: $login.animationsEnabled,
            templateUrl: 'views/newUserModal.html',
            controller: 'NewUserModalController',
            controllerAs: '$modal',
            size: 'sm',
            backdrop: 'static' 
          });
        } else {
          console.log("nie można!!!!!!!!!!!!!")
          AlertService.openModal("Dodawanie nowych użytkowników jest wyłączone", 
          "Aby mieć dostęp do dodawania nowych użytkoników skontaktuj się z administratorem. Lista administratorów to: " 
          + adminsList);
        }

        
      };

      $login.infoPanel = function () {
        InfoService.openModal();
      };

      $login.passwordForgotten = function(){
        $http({
          method: 'GET',
          url: $rootScope.configData.host + '/account/getlistofadmins'
        })
        .then(function(data){
          AlertService.openModal('Twoje hasło może zrestartować administrator aplikacji', 'Lista administratorów to: ' + data.data);
        })
      };

      $login.getAdminList = function(){
        $http({
          method: 'GET',
          url: $rootScope.configData.host + '/account/getlistofadmins'
        })
        .then(function(data){
          console.log(data);
          adminsList = data.data;
        })
      };

      $login.getAdminList();
      $login.getIsAddingUserAviable();
    };

})();
