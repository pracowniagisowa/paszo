(function(){
'use strict'

angular.module('PaszoApp')
.service('AlertService', AlertService);

AlertService.$inject = ['$uibModal']
function AlertService($uibModal){
  var $alert = this;

  $alert.openModal = function (header, message) {
    var modalInstance = $uibModal.open({
      animation: $alert.animationsEnabled,
      templateUrl: 'views/alertModal.html',
      controller: 'AlertModalController',
      controllerAs: '$modal',
      backdrop: 'static',
      resolve:{
        message: function(){
          return message;
        },
        header: function(){
            return header
        }        
      }
    });
  };
};


})();