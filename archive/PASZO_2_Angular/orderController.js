(function (){
'use strict'

angular.module('PaszoApp')
.controller('OrderController', OrderController);

OrderController.$inject = ['$q','$scope', '$uibModal', 'HttpGetService', '$rootScope', '$location', '$cookies', '$http', 'InfoService', 'AlertService']
function OrderController($q, $scope, $uibModal, HttpGetService, $rootScope, $location, $cookies, $http, InfoService, AlertService){
  var $order = this;

  var getOrderParametersLoaded = false;
  var getChangeLoaded = false;
  var updateOrdersListLoaded = false; 
  var getTodaySumLoaded = false;
  var getRestaurantsListLoaded = false;

  var printBools = function(){
    // console.log("getOrderParametersLoaded " + getOrderParametersLoaded);
    // console.log("getChangeLoaded " + getChangeLoaded);
    // console.log("updateOrdersListLoaded " + updateOrdersListLoaded);
    // console.log("getTodaySumLoaded " + getTodaySumLoaded);
    // console.log("getRestaurantsListLoaded " + getRestaurantsListLoaded);
    // console.log("total: " + $scope.orderPageLoaded);
    // console.log("--------------");
  };

  // var orderPageLoaded = false;

  var orderPageLoadedToScope = function(){
    $scope.orderPageLoaded = getOrderParametersLoaded || updateOrdersListLoaded
      getTodaySumLoaded;// || getRestaurantsListLoaded; // getChangeLoaded
    // console.log($scope.orderPageLoaded);
  };

  // console.log(" start " + $scope.orderPageLoaded);

  var paszoToken;
  $scope.ismeridian = false;
  $scope.hstep = 1;
  $scope.mstep = 5;
  
  $scope.visiblePrices = true;
  $scope.showHidePricesLabel = "Pokaż ceny";
  var paszoToken = $cookies.get('paszoToken');
  // console.log("token: " + paszoToken);


  $order.initOrderController = function(){
    $http({
      method: 'GET',
      url: $rootScope.configData.host + '/Account/GetUserByToken',
      withCredentials: true,
      headers: {'Authorization': 'Bearer '+ paszoToken}
    })
    .then(function(data){
      // console.log(data);
      $rootScope.userFromToken = data.data;
      $rootScope.login = data.data.userName;
      $rootScope.isAdmin = data.data.isAdmin;
      $rootScope.name = data.data.name;

      if ($rootScope.name == undefined) {
        $rootScope.displayName = $rootScope.login;
      }
      else{
        $rootScope.displayName = $rootScope.name;
      };
      console.log($rootScope.displayName);
      $order.getOrderParameters();//
      $order.getChange();//
      $order.updateOrdersList();//
      $order.getTodaySum();//
      $order.getActualCashBoxState();
      $order.getRestaurantsList();
      $order.getActualCashBoxState();
    }, function(error){
      console.log(error)      
      console.log("powrot do logowania");
      $location.path('/');
    });
  };

  $order.updateOrdersList = function(){
    $http({
      method: 'GET',
      url: $rootScope.configData.host + '/paszo/gettodayorder',
      headers: {'Authorization': 'Bearer '+ paszoToken}
    })
    .then(function(data){
      // console.log(data);
      $scope.todayOrdersByRestaurans = data.data;
      $scope.todayOrdersList = data.data.todayOrders;
      $scope.todayDrawaing = data.data.todayDrawing;
      if ($scope.todayDrawaing != null) {
        $order.openAggregatedOrders();
        $scope.cashBoxAccordionClass = "panel-collapse collapse in ";
        console.log("if");

      } else {
        console.log("else");
        $scope.cashBoxAccordionClass = "panel-collapse collapse";
      }
      // console.log("updateOrdersList")
      // console.log($scope.todayOrdersList);
      updateOrdersListLoaded = true;
      orderPageLoadedToScope();
      printBools();

    }, function(error){
      console.log(error)
    });
  };

  $order.getTodaySum = function(){
    // console.log("getTodaySum")
    $http({
      method: 'GET',
      url: $rootScope.configData.host + '/paszo/getTodatSum',
      headers: {'Authorization': 'Bearer '+ paszoToken}
    })
    .then(function(data){
      $scope.todaySum = data.data;
      getTodaySumLoaded = true;
      orderPageLoadedToScope();
      printBools();

    }, function(error){
      console.log(error)
    });
  }; 

  $order.getChange = function(){
      $http({
        method: "GET",
        url: $rootScope.configData.host + '/account/GetUserChange?userId=' + $rootScope.userFromToken.userId,
        withCredentials: true,
        headers: {'Authorization': 'Bearer '+ paszoToken}
      })
      .then(function(data){
        $scope.saldo = data.data;
        // console.log($scope.saldo);
        
        getChangeLoaded = true;
        orderPageLoadedToScope();
        printBools();

        }, function(error){
        console.log(error)
        });
  };

  $order.infoPanel = function(){
    InfoService.openModal();
  };

  $order.openAggregatedOrders = function(){
    var modalInstance = $uibModal.open({
      animation: $order.animationsEnabled,
      templateUrl: 'aggregateOrdersModal.html',
      controller: 'AggregateOrdersController',
      controllerAs: '$aggregate',
      //backdrop: 'static',
      resolve:{
        message: function(){
          return undefined;
        },
        header: function(){
            return undefined
             }        
         }
    });
  };

  $order.openOrder = function (){
    console.log($scope.saldo);
    if ($scope.saldo > -20) {
      var modalInstance = $uibModal.open({
        animation: $order.animationsEnabled,
        templateUrl: 'orderModal.html',
        controller: 'ModalController',
        controllerAs: '$modal',
        backdrop: 'static',
        resolve:{
          editObject: function(){
            return undefined;
          },
          updateOrders: function () {
            return function(){$order.refreshAll()}; 
          }
        }
      });
    } else {
      AlertService.openModal('Jesteś spłukany!!', 
          'Twoje saldo wynosi: ' + $scope.saldo + 'zł')
    }

    
  };

  $order.openPay = function (){
    var modalInstance = $uibModal.open({
      animation: $order.animationsEnabled,
      templateUrl: 'payModal.html',
      controller: 'ModalController',
      controllerAs: '$modal',
      backdrop: 'static',
      resolve:{
        editObject: function(){
          return undefined;
        },
        updateOrders: function () {
          return function(){$order.refreshAll()}; 
        }
      }
    });
  };

  $order.editOrder = function (currentOrder){
    console.log(currentOrder)
    var answer = true;
    if (currentOrder.menuItem.isOrdered) {
      answer = confirm("Zamówienie zostało już przekazane do restauracji. Czy jesteś pewien że możesz je edytować?");
    }  
    if (answer) {
      var modalInstance = $uibModal.open({
        animation: $order.animationsEnabled,
        templateUrl: 'editModal.html',
        controller: 'ModalController',
        controllerAs: '$modal',
        backdrop: 'static',
        resolve:{
          editObject: function(){
            return currentOrder;
          },
          updateOrders: function () {
            return function(){$order.refreshAll()}; 
          }
        }
      });
    }
    
  };

  $order.delOrder = function (currentOrder){
    // console.log(currentOrder);
    var answer = true;
    if (currentOrder.menuItem.isOrdered) {
      answer = confirm("Zamówienie zostało już przekazane do restauracji. Czy jesteś pewien że możesz je usunąc?");
    }    
    if (answer) {
      $http({
        method: "DELETE",
        url: $rootScope.configData.host + '/transaction/deltransaction?id=' + currentOrder.id,
        // withCredentials: true
        headers: {'Authorization': 'Bearer '+ paszoToken}
  
      })
      .then(function(){
          // $order.refreshAll();
          $order.updateOrdersList();
          $order.getTodaySum();
          $order.getChange();
        }, function(error){console.log(error);} 
      );
    }
    
  };

  $order.goToAdmin = function () {
    // console.log("go to admin");
      // console.log($rootScope.login);
    $location.path('/admin');
  };

  $order.goToUserPanel = function(){
    $location.path('/userPanel');
  };

  $order.refreshAll = function(){
    $order.updateOrdersList();
    $order.getTodaySum();
    $order.getChange();
    $order.getOrderParameters();
    $order.getRestaurantsList();
    $order.getActualCashBoxState();

    // console.log($scope.orderPageLoaded);
  };

  $order.logout = function(){
    $cookies.put('login', undefined, {'path': '/'});
    $cookies.put('paszoToken', undefined, {'path': '/'});    
    $location.path('/');

  };

  $order.swichVisiblePrices = function(){
    $scope.visiblePrices = !$scope.visiblePrices;
    if ($scope.visiblePrices) {
      $scope.showHidePricesLabel = 'Ukryj ceny'
    }
    else{
       $scope.showHidePricesLabel = 'Pokaż ceny'
    }
  };

  $order.getOrderParameters = function(){
    $http({
        method: 'GET',
        url: $rootScope.configData.host + '/paszo/GetTodayOrderParameters',
        withCredentials: true,
        headers: {'Authorization': 'Bearer '+ paszoToken}

      })
      .then(function(data){
        $scope.orderParameters = data.data;
        // console.log($scope.orderParameters)
        if (data.data.status == 'Zamówione' || data.data.status == 'Doręczone') {
          $scope.orderTimeVisible = false
        } else {$scope.orderTimeVisible = true}


        // console.log(orderTimeArray)
        $scope.orderTimeObject = new Date();
        $scope.orderTimeObject.setHours(0);
        $scope.orderTimeObject.setMinutes(data.data.orderTime);
        
        if (data.data.deliveryTime == 0) {
          // console.log(data.data.DeliveryTime)
          $scope.deliveryTimeVisibility = false;
        } else {$scope.deliveryTimeVisibility = true}
        $scope.deliveryTimeObject = new Date();
        $scope.deliveryTimeObject.setHours(0);
        $scope.deliveryTimeObject.setMinutes(data.data.deliveryTime);

        getOrderParametersLoaded = true;
        orderPageLoadedToScope();
        printBools();

        
      }, function(error){
      console.log(error)
    });
  };
  $order.postOrderParameters = function () {
    $http({
      method: "POST",
      url: $rootScope.configData.host + '/paszo/PostTodayOrderParameters',
      data: $scope.orderParameters,
      headers: {'Authorization': 'Bearer '+ paszoToken}
    })
    .then (function(){
      // console.log($scope.orderParameters);
      $order.getOrderParameters();
      $order.updateOrdersList();      
    }, function(error){
      // console.log($scope.orderParameters);
      console.log(error);
      // AlertService.openModal('', message);
    } );
  };

  $order.setRestourantToScope = function(value){
    $scope.orderParameters.restaurant = value;
    console.log(value);
  };

  $order.setStatusToScope = function(value){
    $scope.orderParameters.status = value;
    // console.log(value);
    // console.log($scope.orderParameters);
  };

  $order.timeObjectToInt = function(timeObject){
    return timeObject.getHours() * 60 + timeObject.getMinutes();
  };

  $order.orderTimeChanged = function(){
    var orderTimeString = $order.timeObjectToInt($scope.orderTimeObject);      
    $scope.orderParameters.orderTime = orderTimeString;
    // console.log($scope.orderParameters.OrderTime)
  };

  $order.deliveryTimeChanged = function(){
    // var deliveryTimeString = $scope.deliveryTimeObject.getHours() + ":" + $scope.deliveryTimeObject.getMinutes() + ":00";
    var deliveryTimeString = $order.timeObjectToInt($scope.deliveryTimeObject);
    
    $scope.orderParameters.deliveryTime = deliveryTimeString;
    // console.log($scope.orderParameters.deliveryTime)
  };

  $order.getRestaurantsList = function(){
    $http({
        method: 'GET',
        url: $rootScope.configData.host + '/restaurant/GetRestaurants',
        // withCredentials: true
        headers: {'Authorization': 'Bearer '+ paszoToken}
      })
      .then(function(data){
        $scope.restaurantsList = data.data;
        getRestaurantsListLoaded = true;
        orderPageLoadedToScope();
        printBools();
      }, function(error){
      console.log(error)
    });
  };

  $order.getActualCashBoxState = function(){
    $http({
      method: "GET",
      url: $rootScope.configData.host + '/paszo/GetActualCashBoxState',
      headers: {'Authorization': 'Bearer '+ paszoToken}
    }).then(function(data){
      $scope.actualCashBoxState = data.data;
      $order.actualCashBoxStateCashState = data.data.cashState;
      $order.actualCashBoxStateComment= data.data.comment;
      $order.actualCashBoxStateTime = data.data.time;
      $order.actualCashBoxStateUserName = data.data.userName;

      console.log($scope.actualCashBoxState);
    }, function(error){
      console.log(error)
    });
  };

  $order.postCashBoxState = function(){
    $http({
      method: "POST",
      data: {
        "cashState": $order.actualCashBoxStateCashState,
        "comment": $order.actualCashBoxStateComment
      },
      url: $rootScope.configData.host + '/paszo/PostCashBoxState',
      headers: {'Authorization': 'Bearer '+ paszoToken}
    }).then(function(data){
      $order.getActualCashBoxState();
    }, function(error){
      console.log(error);
    });
  };

  $order.goToLink = function(){

  };

  $order.initOrderController();

  $order.animationsEnabled = true;

};


})();
