(function(){
  angular.module('PaszoApp')
  .controller('AlertModalController', AlertModalController);

  AlertModalController.$inject = ['$scope', '$uibModalInstance', '$uibModal', '$rootScope', 'message','header' ]
  function AlertModalController($scope, $uibModalInstance, $uibModal, $rootScope, message, header){
    var $modal = this;

    $scope.message = message;
    $scope.header = header;



    $modal.ok = function () {
      $uibModalInstance.dismiss('cancel');
    };

  };
})();
