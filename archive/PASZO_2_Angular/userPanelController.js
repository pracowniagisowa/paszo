(function (){

angular.module('PaszoApp')
.controller('UserPanelController', UserPanelController);

UserPanelController.$inject = ['$scope', '$rootScope', '$http', '$cookies', 'AlertService', '$location']
function UserPanelController($scope, $rootScope, $http, $cookies, AlertService, $location){
    var $userPanel = this;
    console.log("UserPanelController")
    var paszoToken = $cookies.get('paszoToken');

    $userPanel.userFromToken = $rootScope.userFromToken;

    $scope.telephoneNumber;
    $scope.email;
    $scope.name;

    $userPanel.initUserController = function(){
        $http({
          method: 'GET',
          url: $rootScope.configData.host + '/Account/GetUserByToken',
          withCredentials: true,
          headers: {'Authorization': 'Bearer '+ paszoToken}
        })
        .then(function(data){
            console.log(data.status);
            if (data.status == 200) {
                $rootScope.userFromToken = data.data;
                $userPanel.userFromToken = data.data;
                console.log("GetUserByToken:");
                console.log($rootScope.userFromToken);
                $scope.telephoneNumber = data.data.telephoneNumber;
                $scope.name = data.data.name;
                $scope.email = data.data.email;

                $userPanel.getChange();
                $userPanel.getUserHistory();
            } else {
                console.log("else:");
                console.log(data);

            };
        
        }, function(error){
            console.log(error)
            console.log("powrot do logowania");
            $location.path('/');
        });
      };

    $userPanel.goToOrder = function(){
        $location.path('/order');
    };

    $userPanel.getChange = function(){
        $http({
            method: "GET",
            url: $rootScope.configData.host + '/Account/GetUserChange?userId=' + $rootScope.userFromToken.userId,
            // withCredentials: true
            headers: {'Authorization': 'Bearer '+ paszoToken}

        })
        .then(function(data){
        $scope.saldo = data.data;
        $scope.saldoAbsoute = Math.abs(data.data);

        // console.log($scope.credit);
        }, function(error){
        console.log(error)
        });
    };

    $userPanel.passwordChange = function(){
        if ($scope.newPassword != $scope.newPasswordRepeat) {
            AlertService.openModal('Uwaga','Nowo podane hasła się nie zgadzają')
            return;
        }
        console.log("zmieniamy dalej")
        var loginJson = {
          "UserName": $rootScope.login,
          "Password": $scope.oldPassword,
          "NewPassword": $scope.newPassword
              };
        $http({
            method: 'POST',
            url: $rootScope.configData.host + '/Account/SetPassword?userId=' + $rootScope.userFromToken.userId,
            data: loginJson,
            headers: {'Authorization': 'Bearer '+ paszoToken}
                })
                .then(function(data){
                    console.log(data)
                    AlertService.openModal('', data.data)

                }), function (error) {
                    console.log(error);
                };
    };

    $userPanel.getUserHistory = function(){
        $http({
            method: "GET",
            url: $rootScope.configData.host + '/Account/GetUserHistory?userId=' + $rootScope.userFromToken.userId,
            // withCredentials: true
            headers: {'Authorization': 'Bearer '+ paszoToken}

          })
          .then(function(data){
            console.log("Historia:")
            $scope.userHistory = data.data;
            console.log($scope.userHistory);

            }, function(error){
            console.log(error)
            });
      };
    
    $userPanel.getUserData = function(){
        $http({
            method: "GET",
            url: $rootScope.configData.host + '/Account/getuserbylogin?login=' + $rootScope.userFromToken.userName,
            // withCredentials: true
            headers: {'Authorization': 'Bearer '+ paszoToken}
        })
        .then(function(data){
            console.log("getuserbylogin:");
            console.log(data.data);
            var userData = data.data;
            $scope.name = userData.Name;
            $scope.email = userData.Email;
            if (userData.TelephoneNumber != 0) {
                $scope.telephoneNumber = userData.TelephoneNumber;
            }

        }, function(error){
            console.log(error)
        });
      };

    $userPanel.editUserData = function(){
        var userData = {
            "UserName": $rootScope.login,
            "Name": $scope.name,
            "Email": $scope.email,
            "TelephoneNumber": $scope.telephoneNumber
        };
        console.log(userData);
        $http({
            method: "POST",
            url: $rootScope.configData.host + '/Account/EditUserData?userId=' + $rootScope.userFromToken.userId,//?user=' + userData,
            data: userData,
            // withCredentials: true 
            headers: {'Authorization': 'Bearer '+ paszoToken}
        }).then(function(data){
            AlertService.openModal('Dane użytkownika zostały zmienione')
        });
      };


    $userPanel.initUserController();
    // $userPanel.getChange();
    // $userPanel.getUserHistory();
    // $userPanel.getUserData();
    console.log("User from rootscope:");
    console.log($rootScope.userFromToken);


};
})();