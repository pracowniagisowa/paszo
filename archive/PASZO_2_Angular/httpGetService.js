(function(){
'use strict'

angular.module('PaszoApp')
.service('HttpGetService', HttpGetService);

HttpGetService.$inject = ['$q', '$http']
function HttpGetService($q, $http){
  var todayOrder = this

  todayOrder.getData = function (url) {
    var deferred = $q.defer();
    console.log('HttpGetService');

    $http.get(url)
    .then(function(response){
      deferred.resolve(response.data);
    }, function(error){
      console.log(error)
      deferred.reject(error);
    });
      return deferred.promise;
  };
};

})();
