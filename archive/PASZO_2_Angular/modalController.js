(function() {
    angular.module('PaszoApp')
        .controller('ModalController', ModalController);

    ModalController.$inject = ['$scope', '$uibModalInstance', '$rootScope', '$location', '$http', '$cookies', 'HttpGetService', 'editObject', 'updateOrders']

    function ModalController($scope, $uibModalInstance, $rootScope, $location, $http, $cookies, HttpGetService, editObject, updateOrders) {
        var $modal = this;
        var isOrdered = false;
        $scope.currentRestaurant;

            // console.log('zmienna z modala: ' + updateOrders);
        $scope.boxPrice = 0;
        var paszoToken = $cookies.get('paszoToken');

        

        $modal.initModalController = function(){
            $http({
                method: 'GET',
                url: $rootScope.configData.host + '/restaurant/GetRestaurants',
                // withCredentials: true
                headers: { 'Authorization': 'Bearer ' + paszoToken }
            })
            .then(function(data) {
                $scope.restaurantsList = data.data;
                getRestaurantsListLoaded = true;
                // console.log($scope.restaurantsList);
                // orderPageLoadedToScope();
            }, function(error) {
                console.log(error)
            });

            $http({
                method: "GET",
                url: $rootScope.configData.host + '/account/GetUserChange?userId=' + $rootScope.userFromToken.userId,
                withCredentials: true,
                headers: { 'Authorization': 'Bearer ' + paszoToken }
            })
            .then(function(data) {
                // console.log(data);
                $scope.saldo = data.data;
                $scope.debt = data.data * -1;

            }, function(error) {
                console.log(error)
            });

            if (editObject == undefined) {

                $http({
                    method: 'GET',
                    url: $rootScope.configData.host + '/paszo/GetTodayOrderParameters',
                    withCredentials: true,
                    headers: { 'Authorization': 'Bearer ' + paszoToken }
    
                }).then(function(data) {
                    $scope.currentRestaurant = data.data.restaurant;
                    console.log($scope.currentRestaurant);
                    $modal.getMenuItemsForRestaurants();
                });
            } else {
                $http({
                        method: "GET",
                        url: $rootScope.configData.host + '/Transaction/GetTransactionById?id=' + editObject.id,
                        headers: { 'Authorization': 'Bearer ' + paszoToken }
                    })
                    .then(function(data) {
                        // $scope.currentOrder = data;
                        console.log(data);
                        $scope.currentRestaurant = editObject.restaurantDto;
                        console.log($scope.currentRestaurant);
                        
                        $scope.itemNameEdit = data.data.menuItem.itemName;
                        $scope.itemPriceEdit = data.data.menuItem.itemPrice;
                        $scope.boxPriceEdit = data.data.menuItem.boxPrice;
                        $scope.commentEdit = data.data.comment;
                        isOrdered = data.data.menuItem.isOrdered;
                        // console.log($scope.itemName +"; "+ $scope.itemPrice +"; "+ $scope.boxPrice )

                    }, function(error) {
                        console.log(error)
                    });
            };
        };

        $modal.getMenuItemsForRestaurants = function() {
            console.log('getMenuItemsForRestaurants');
            console.log($scope.currentRestaurant);
            $http({
                method: "GET",
                url: $rootScope.configData.host + '/restaurant/GetMenuItemsForRestaurant?restaurantName=' + $scope.currentRestaurant.name,
                headers: { 'Authorization': 'Bearer ' + paszoToken }
            }).then(function(data) {
                // console.log(data);
                $scope.menuItemsForRestaurant = data.data;

            })
        };

        $modal.setRestourantToScope = function(value) {
            console.log("value to scope:");
            console.log(value);
            $scope.currentRestaurant = value;
            $modal.getMenuItemsForRestaurants();
        };


        $scope.countAll = function() {
            // console.log("Count All function")
            $scope.totalPrice = $scope.itemPrice + $scope.boxPrice;
            var toPayValue = $scope.totalPrice - $scope.saldo;
            if (toPayValue < 0) {
                $scope.toPay = 0;
            } else {
                $scope.toPay = toPayValue;
            }
        };

        $scope.countAllEdit = function() {
            // console.log("Count All Edit function")
            $scope.totalPrice = $scope.itemPriceEdit + $scope.boxPriceEdit;
            var toPayValue = $scope.totalPrice - $scope.saldo;
            if (toPayValue < 0) {
                $scope.toPay = 0;
            } else {
                $scope.toPay = toPayValue;
            }
        };



        $modal.PassSelectedMenuItemToScope = function(menuItem) {
            console.log(menuItem)
            $scope.itemName = menuItem.itemName;
            $scope.itemPrice = menuItem.itemPrice;
            $scope.boxPrice = menuItem.boxPrice;
            $scope.countAll();
        };

        $modal.PassSelectedMenuItemToEditScope = function(menuItem) {
            console.log(menuItem)
            $scope.itemNameEdit = menuItem.itemName;
            $scope.itemPriceEdit = menuItem.itemPrice;
            $scope.boxPriceEdit = menuItem.boxPrice;
            $scope.countAll();
        };

        $modal.orderOk = function() {
            console.log($scope.currentRestaurant);
            var orderJson = {
                "MenuItem": {
                    "ItemName": $scope.itemName,
                    "ItemPrice": $scope.itemPrice,
                    "BoxPrice": $scope.boxPrice
                },
                "Comment": $scope.comment,
                "Restaurant": {
                    "Id": $scope.currentRestaurant.id
                }
            };
            console.log(orderJson);

            $http({
                    method: "POST",
                    data: orderJson,
                    url: $rootScope.configData.host + '/Transaction/AddTransaction?userId=' + $rootScope.userFromToken.userId,
                    // withCredentials: true
                    headers: { 'Authorization': 'Bearer ' + paszoToken }

                })
                .then(function() {
                    updateOrders()
                }, function(error) {
                    console.log(error);

                });


            $uibModalInstance.dismiss('cancel');
        };

        $modal.payOk = function() {
            // console.log($scope.income);

            var payJson = {
                "Ammount": $scope.income
            };
            // $http.put(transactionUrl, payJson)
            $http({
                    method: "PUT",
                    data: payJson,
                    url: $rootScope.configData.host + '/Transaction/AddTransaction?userName=' + $rootScope.login,
                    // withCredentials: true
                    headers: { 'Authorization': 'Bearer ' + paszoToken }
                })
                .then(function() {
                    updateOrders()
                }, function(error) { console.log(error); });

            $uibModalInstance.dismiss('cancel');
        };

        $modal.cancelModal = function() {
            $uibModalInstance.dismiss('cancel');
        };

        $modal.saveEdit = function() {
            console.log("edytuje...")

            var editUrl = $rootScope.configData.host + '/Transaction/EditTransaction?id=' + editObject.id;

            var editJson = {
                    "MenuItem": {
                        "ItemName": $scope.itemNameEdit,
                        "ItemPrice": $scope.itemPriceEdit,
                        "BoxPrice": $scope.boxPriceEdit,
                        "IsOrdered": isOrdered
                    },
                    "Comment": $scope.commentEdit,
                }
                // console.log(editJson)

            // $http.put(editUrl, editJson)
            $http({
                    method: "POST",
                    data: editJson,
                    url: $rootScope.configData.host + '/Transaction/EditTransaction?id=' + editObject.id,
                    // withCredentials: true
                    headers: { 'Authorization': 'Bearer ' + paszoToken }
                })
                .then(function() {
                    updateOrders()
                }, function(error) { console.log(error); });

            $uibModalInstance.dismiss('cancel');
        };

        $modal.initModalController();

        // $modal.getMenuItemsForRestaurants();


    };
})();