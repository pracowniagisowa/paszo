(function(){
    angular.module('PaszoApp')
    .controller('AggregateOrdersController', AggregateOrdersController);
  
    AggregateOrdersController.$inject = ['$scope', '$uibModalInstance', '$uibModal', '$rootScope', '$cookies', '$http', 'message','header' ]
    function AggregateOrdersController($scope, $uibModalInstance, $uibModal, $rootScope, $cookies, $http){
      var $aggregate = this;

      var paszoToken = $cookies.get('paszoToken');
      $scope.aggregateOrders;
      $scope.todayDrawing;

      $aggregate.getAggregateOrders = function(){
        $http({
            method: "GET",
            url: $rootScope.configData.host + '/paszo/GetAggregatedOrder',
            headers: {'Authorization': 'Bearer '+ paszoToken}
        }).then(function(data){
            // console.log(data);
            $scope.aggregateOrders = data.data.aggregatedOrders;
            $scope.todayDrawing = data.data.todayDrawing;
            console.log($scope.aggregatedOrders);
        });
      };

      $aggregate.getTodayOrderParameters = function(){
        $http({
          method: "GET",
          url: $rootScope.configData.host + '/paszo/GetTodayOrderParameters',
          headers: {'Authorization': 'Bearer '+ paszoToken}
        }).then(
          function(data){
            $scope.todayOrderParameters = data.data;
            console.log($scope.todayOrderParameters);
          }
        );
      };
  
  
      $aggregate.ok = function () {
        $uibModalInstance.dismiss('cancel');
      };

      $aggregate.getAggregateOrders();
      $aggregate.getTodayOrderParameters();
  
    };
  })();